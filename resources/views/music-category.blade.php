@extends('layouts.admin-master')

@section('title')
Music Category
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('css/customnotification.css')}}">

<style>
[class*="sidebar-dark"]  .user-panel{
    border-bottom: 1px solid #C83448 !important;
  }
.dot-edit-delete{
    font-size: 15px;
    width: 32px;
    height: 32px;
    padding-left:8.5px !important;
    padding-top: 6px !important; 
}
#albumFilterSearch{
   width: 100%; 
}
.music-category-header{
    margin-top: 103px;
}
.music-card-header{
    height: 233px;
}
@media screen and (max-width: 300px) {
    .music-category-header{
    margin-top: 80px !important;
}
}
@media screen and (max-width: 768px) {
  .music-card-header{
    height: 268px;
}

}
</style>
@endsection

@section('content')
<div class="row  pt-1 pb-3">
    <div class="col-lg-12">
        <span class="withdrawal-text">Music Category</span> <button type="button" data-toggle="modal" data-target="#addMusicCategory" class="btn ml-2 red-button btn-xs border-radius8 add-noti"><i class="bi bi-plus-lg"></i> Add New</button>
    </div>  
</div>   
<div class="row pt-1" >
    <div class="col-lg-3 col-md-12 ">
            <div class="card border-radius8 music-category-card">
            <div class="card-body border-radius8 pt-3 pl-4 pr-4 pb-3">
                <div class="row">
                    <small class="pl-0 text-dark font-weight500" >Categories List</small>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-4 pl-0">
                        <div class="background" style="background-image: url('../image/blog/blo.png');  ">
                            <div class="text-center transbox">
                               <p>This is </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-4 pl-0">
                        <div class="background" style="background-image: url('../image/blog/blo2.png');  ">
                            <div class="text-center transbox">
                               <p>This is </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-4 pl-0">
                        <div class="background" style="background-image: url('../image/blog/blo3.png');  ">
                            <div class="text-center transbox">
                               <p>This is </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-4 pl-0">
                        <div class="background" style="background-image: url('../image/blog/blo.png');  ">
                            <div class="text-center transbox">
                               <p>This is </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-4 pl-0">
                        <div class="background" style="background-image: url('../image/blog/blo.png');  ">
                            <div class="text-center transbox">
                               <p>This is </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-4 pl-0">
                        <div class="background" style="background-image: url('../image/blog/blo2.png');  ">
                            <div class="text-center transbox">
                               <p>This is </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-4 pl-0">
                        <div class="background" style="background-image: url('../image/blog/blo.png');  ">
                            <div class="text-center transbox">
                               <p>This is </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-4 pl-0">
                        <div class="background" style="background-image: url('../image/blog/blo2.png');  ">
                            <div class="text-center transbox">
                               <p>This is </p>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        
        </div>
    {{-- <img src="{{ asset('image/blog/blo.png')}}" alt="Girl in a jacket" width="100%" height="69"> --}}
    
    <div class="col-lg-9 col-md-12" >
        <div class="card border-radius8 music-category-card p-0">
            <div class="card-header music-card-header p-0">
                <div class="row m-0 p-0" style="background-color: #E9B3C2;height: 193px;">
                    <div class="col-lg-6 col-md-6 col-sm-6 pl-4">
                        <div class="row music-category-header">
                            <small class="text-white h4 border-radius8 p-2 pl-3 "  style="background-color:#5A1C52; width:180px">Hiphop & Rap</small>
                            <span class="dot-edit-delete" data-toggle="modal" data-target="#editMusicCategory"><i class="bi bi-pencil-fill"></i></span>
                        <span class="dot-edit-delete" data-toggle="modal" data-target="#deleteMusicCategory"><i class="bi bi-trash-fill"></i></span>
                        </div>
                        <div class="row">
                            <small class="text-white border-radius8 pr-1 pl-3 "  style="background-color:#5A1C52; width:135px;padding-top:1px;padding-bottom:1px">24 Songs| 5 Albums</small>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 p-3">
                        <div class="back float-end" style="background-image: url('../image/blog/bl.jpg');  ">
                            <div class="text-center trans">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row m-0">
                            <div class="col-lg-9 col-md-9">
                            <ul class="nav nav-pills ml-auto " style="margin-left: -8px !important;margin-top: 4px !important;font-size: 11px;">
                                <li class="nav-item text-center" style="width: 120px;"><span class="nav-link active" href="#tab_1" data-toggle="tab" style="">All Songs</span></li>
                                <li class="nav-item text-center" style="width: 120px;"><span class="nav-link" href="#tab_2" data-toggle="tab">All Albums</span></li>
                            </ul>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <input type= "search" class="form-control  m-1 ml-0  float-right font-size12" id="albumFilterSearch"  placeholder="Search">
                            </div>    
                        </div>
                    </div>
            <div class="card-body p-0">
                <div class="tab-content">
                    <div class="tab-pane active table-responsive" id="tab_1">
                         {{-- Song Table --}}
                                <table id="songTable" class="table display" >
                                    <thead id="song-thead">
                                        <tr>
                                            <th style="width: 240px"></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr style="background-color: white !important;">
                                            <td  class="p-0 pl-2" style="width: 240px">
                                                <ul class="products-list product-list-in-card pl-2 pr-1">
                                                    <li class="item">
                                                    <div class="product-img pt-1">
                                                        <img src="{{asset('image/blog/blog3.jpg') }}" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
                                                    </div>
                                                    <div class="product-info ml-5">
                                                        <small  class="product-title font-szie10">Life on Mars <span class="font-szie10 light-grey-text">(Paid $2)</span></small>
                                                        <small class="product-description font-szie10 light-grey-text">3:56</small>
                                                    </div>
                                                    </li>
                                                </ul> 
                                            </td>
                                            <td style="padding-left:494px;"></td>
                                            <td class=" pr-2"> 
                                                <span class="p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Audio</span>
                                                <span class="p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Video</span>
                                            </td>
                                        </tr>
                                        <tr style="background-color: white !important;">
                                            <td  class="p-0 pl-2" style="width: 240px">
                                                <ul class="products-list product-list-in-card pl-2 pr-1">
                                                    <li class="item">
                                                    <div class="product-img pt-1">
                                                        <img src="{{asset('image/blog/blog3.jpg') }}" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
                                                    </div>
                                                    <div class="product-info ml-5">
                                                        <small  class="product-title font-szie10">Life on Mars <span class="font-szie10 light-grey-text">(Paid $2)</span></small>
                                                        <small class="product-description font-szie10 light-grey-text">3:56</small>
                                                    </div>
                                                    </li>
                                                </ul> 
                                            </td>
                                            <td ></td>
                                            <td class=" pr-2 "> 
                                                <span class=" p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Audio</span>
                                                <span class=" p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Video</span>
                                            </td>
                                        </tr>                                       
                                    </tbody>
                                </table>

                    </div>
                    <div class="tab-pane " id="tab_2">
                        <div class="row m-0 mt-4" >
                            <div class=" box ml-2" style="width: 88px">
                                <center class=" float-left">
                                    <a href="#" data-toggle="modal" data-target="#albumDetail"><img src="{{asset('image/blog/blog1.jpg') }}"  class="border-radius5" width="80px" height="80px" alt="">
                                    <small  class="name" style="font-size: 10px;color:black">Welcome to </small></a>
                                </center>
                            </div>
                            <div class=" box ml-2" style="width: 88px">
                                <center class=" float-left">
                                    <img src="{{asset('image/blog/blog3.jpg') }}" class="border-radius5" width="80px" height="80px" alt="">
                                    <small class="name" style="font-size: 10px">70s Rock </small>
                                </center>
                            </div>
                            <div class=" box ml-2" style="width: 88px">
                                <center class=" float-left">
                                    <img src="{{asset('image/blog/blog1.jpg') }}"  class="border-radius5" width="80px" height="80px" alt="">
                                    <small  class="name" style="font-size: 10px">World </small>
                                </center>
                            </div>
                            <div class=" box ml-2" style="width: 88px">
                                <center class=" float-left">
                                    <img src="{{asset('image/blog/blog3.jpg') }}" class="border-radius5" width="80px" height="80px" alt="">
                                    <small class="name" style="font-size: 10px">80s Rock </small>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


{{-- Add Music Category Modal --}}
<div class="modal fade" id="addMusicCategory">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle" >
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-3 pb-2 pr-2" >
                <p class="modal-title add-bank-text" ><b>Add Category</b></p>
                <button type="button" class="close"  data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="editProfileForm" class="needs-validation" novalidate>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="mb-2 mt-1 input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark " style="font-size: 13.5px;font-weight:500;color:black">Title</span></label>
                            <input type="text" class="form-control input-sm border-radius8" placeholder="Type category name" style="font-size: 12.5px" id="title" name="title" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-1 mt-1 input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark " style="font-size: 13.5px;font-weight:500"><b>Image</b> <span class="add-song-span-text ">( Max 1MB )</span></span></label>
                            <input type="file" class="form-control input-sm border-radius8 "   placeholder="Choose Image (Max 1MB)" style="font-size: 12.5px" id="categoryImage" name="categoryImage" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer p-2 justify-content-between " style="margin-left: 4px">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">Close</button>
                <input type="submit" class="btn red-button btn-xs mr-2 pt-1 pb-1 pl-3 pr-3 border-radius8 account-Create-button" value="Add">
            </div>
            </form>
        </div>
    </div>
</div>

{{-- Edit Music Category Modal --}}
<div class="modal fade" id="editMusicCategory">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle" >
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-3 pb-2 pr-2" >
                <p class="modal-title add-bank-text" ><b>Edit Category</b></p>
                <button type="button" class="close"  data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="editProfileForm" class="needs-validation" novalidate>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="mb-2 mt-1 input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark " style="font-size: 13.5px;font-weight:500;color:black">Title</span></label>
                            <input type="text" class="form-control input-sm border-radius8" vale="Hip & Hop" placeholder="Type category name" style="font-size: 12.5px" id="editTitle" name="editTitle" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-1 mt-1 input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark " style="font-size: 13.5px;font-weight:500"><b>Image</b> <span class="add-song-span-text ">( Max 1MB )</span></span></label>
                            <input type="file" class="form-control input-sm border-radius8 "   placeholder="Choose Image (Max 1MB)" style="font-size: 12.5px" id="editCategoryImage" name="editCategoryImage" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer p-2 justify-content-between " style="margin-left: 4px">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">Close</button>
                <input type="submit" class="btn red-button btn-xs mr-2 pt-1 pb-1 pl-3 pr-3 border-radius8 account-Create-button" value="Update">
            </div>
            </form>
        </div>
    </div>
</div>



{{-- Delete Music Category Modal --}}
<div class="modal fade" id="deleteMusicCategory">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle">
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-3 pb-2" >
                <p class="modal-title notification-detail-text"><b>Confirmation</b></p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col notification-detail-textt">
                        <small>Are You Sure You Want To Delete this Category</small>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between ">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">No</button>
                <form>
                    <input type="submit" class="btn red-button btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8" value="Yes">
                </form>
            </div>
        </div>
    </div>
</div>

{{-- Album detail Modal --}}
<div class="modal fade" id="albumDetail">
    <div class="modal-dialog modal-dialog-centered modal-lg card-detail">
        <div class="modal-content border-radius12">
            <div class="modal-header border-radius-top pl-3 pt-4 pb-3 pr-2" style="background-color: #BA011A;color:white">
                <div class="col-1">
                    <div>
                        <img src="{{asset('image/blog/blog3.jpg') }}" style="" class="border-radius5 song-image" alt="thumbnail" width="45px" height="45px">
                    </div>
                </div>
                <div class="col-8 pl-3">
                    <div class="p-1 pl-0" id="song-title-text">
                        <small class="d-flex align-items-start" style="font-weight: 500">Life on Mars</small>
                        <small class="d-flex align-items-end " style="font-size:9px;">05 Songs</small>
                    </div>
                </div>
                <div class="col-3  ">
                    <div class="float-right">
                        <button type="button" class="close pt-0" style="color: #fff"  data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="modal-body p-0 ">
                <div class="row">
                    <div class="col-12 table-wrapper-scroll-y my-custom-scrollbar">
                        <table class="table responsive ">
                            <tbody>
                                <tr>
                                    <td  class="p-0 pl-2">
                                        <ul class="products-list product-list-in-card pl-2 pr-1">
                                            <li class="item">
                                                <div class="product-img pt-1">
                                                    <img src="{{asset('image/blog/blog3.jpg') }}" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
                                                </div>
                                                <div class="product-info ml-5">
                                                    <small  class="product-title font-szie10">Life on Mars </small>
                                                    <small class="product-description font-szie10 light-grey-text">3:56</small>
                                                </div>
                                            </li>
                                        </ul>
                                    </td>
                                    <td class="pl-0 pr-0"> 
                                        <span class="ml-5 p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Audio</span>
                                        <span class="p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Video</span>
                                    </td>
                                    <td></td>
                                    <td class="pb-2" > 
                                            <small class="product-description font-szie10">Free</small>
                                    </td>
                                </tr>
                                <tr>
                                    <td  class="p-0 pl-2">
                                        <ul class="products-list product-list-in-card pl-2 pr-1">
                                            <li class="item">
                                                <div class="product-img pt-1">
                                                    <img src="{{asset('image/blog/blog3.jpg') }}" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
                                                </div>
                                                <div class="product-info ml-5">
                                                    <small  class="product-title font-szie10">Life on Mars </small>
                                                    <small class="product-description font-szie10 light-grey-text">3:56</small>
                                                </div>
                                            </li>
                                        </ul>
                                    </td>
                                    <td class="pl-0 pr-0"> 
                                        <span class="ml-5 p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Audio</span>
                                        <span class="p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Video</span>
                                    </td>
                                    <td></td>
                                    <td class="pb-2" > 
                                            <small class="product-description font-szie10">Free</small>
                                    </td>
                                </tr>
                                <tr>
                                    <td  class="p-0 pl-2">
                                        <ul class="products-list product-list-in-card pl-2 pr-1">
                                            <li class="item">
                                                <div class="product-img pt-1">
                                                    <img src="{{asset('image/blog/blog3.jpg') }}" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
                                                </div>
                                                <div class="product-info ml-5">
                                                    <small  class="product-title font-szie10">Life on Mars </small>
                                                    <small class="product-description font-szie10 light-grey-text">3:56</small>
                                                </div>
                                            </li>
                                        </ul>
                                    </td>
                                    <td class="pl-0 pr-0"> 
                                        <span class="ml-5 p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Audio</span>
                                    </td>
                                    <td></td>
                                    <td class="pb-2" > 
                                            <small class="product-description font-szie10">$2</small>
                                    </td>
                                </tr>
                                <tr>
                                    <td  class="p-0 pl-2">
                                        <ul class="products-list product-list-in-card pl-2 pr-1">
                                            <li class="item">
                                                <div class="product-img pt-1">
                                                    <img src="{{asset('image/blog/blog3.jpg') }}" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
                                                </div>
                                                <div class="product-info ml-5">
                                                    <small  class="product-title font-szie10">Life on Mars </small>
                                                    <small class="product-description font-szie10 light-grey-text">3:56</small>
                                                </div>
                                            </li>
                                        </ul>
                                    </td>
                                    <td class="pl-0 pr-0"> 
                                        <span class="ml-5 p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Audio</span>
                                    </td>
                                    <td></td>
                                    <td class="pb-2" > 
                                            <small class="product-description font-szie10">$2</small>
                                    </td>
                                </tr>
                                <tr>
                                    <td  class="p-0 pl-2">
                                        <ul class="products-list product-list-in-card pl-2 pr-1">
                                            <li class="item">
                                                <div class="product-img pt-1">
                                                    <img src="{{asset('image/blog/blog3.jpg') }}" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
                                                </div>
                                                <div class="product-info ml-5">
                                                    <small  class="product-title font-szie10">Life on Mars </small>
                                                    <small class="product-description font-szie10 light-grey-text">3:56</small>
                                                </div>
                                            </li>
                                        </ul>
                                    </td>
                                    <td class="pl-0 pr-0"> 
                                        <span class="ml-5 p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Audio</span>
                                    </td>
                                    <td></td>
                                    <td class="pb-2" > 
                                            <small class="product-description font-szie10">$2</small>
                                    </td>
                                </tr>
                                <tr>
                                    <td  class="p-0 pl-2">
                                        <ul class="products-list product-list-in-card pl-2 pr-1">
                                            <li class="item">
                                                <div class="product-img pt-1">
                                                    <img src="{{asset('image/blog/blog3.jpg') }}" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
                                                </div>
                                                <div class="product-info ml-5">
                                                    <small  class="product-title font-szie10">Life on Mars </small>
                                                    <small class="product-description font-szie10 light-grey-text">3:56</small>
                                                </div>
                                            </li>
                                        </ul>
                                    </td>
                                    <td class="pl-0 pr-0"> 
                                        <span class="ml-5 p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Audio</span>
                                    </td>
                                    <td></td>
                                    <td class="pb-2" > 
                                            <small class="product-description font-szie10">$2</small>
                                    </td>
                                </tr>
                            </tbody>
                        </table>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection


@section('script')
 <script>
    // Disable form submissions if there are invalid fields
    (function() {
      'use strict';
      window.addEventListener('load', function() {
        // Get the forms we want to add validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
          form.addEventListener('submit', function(event) {
            if (form.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
            }
            form.classList.add('was-validated');
          }, false);
        });
      }, false);
    })();
    </script>
    <script>$(document).ready(function() {
      $("#title").on('click', function(event) {
        event.preventDefault();
        $('#title').addClass( "border-danger" );        
      });
      $("#title" ).mouseleave(function() {
        $('#title').removeClass( "border-danger" );
      });      
      $("#categoryImage").on('click', function(event) {
        event.preventDefault();
        $('#categoryImage').addClass( "border-danger" );
      });
      $("#categoryImage" ).mouseleave(function() {
        $('#categoryImage').removeClass( "border-danger" );
      });   
  });
</script>

 <script>
     $(document).ready(function(){
    
    var $search = $("#albumFilterSearch").on('input',function(){
        var matcher = new RegExp($(this).val(), 'gi');
        $('.box').show().not(function(){
            return matcher.test($(this).find('.name').text())
        }).hide();
    });
});
 </script>
@endsection


