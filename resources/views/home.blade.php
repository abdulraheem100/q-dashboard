@extends('layouts.master')

@section('title')
Q Music
@endsection

@section('style')
<style>
    .content-wrapper{
        background-color: white;
    }
    .white-color{
        color: white !important;
    }
    .main-sidebar{
        width: 200px;

    }
    .img-22{
        width: 26px !important;
        height: 26px !important;
        margin-right: 3px;
    }
    #header-nav{
        margin-left: 185px;
        height: 45px !important;
    }
    .width180{
        width: 180px !important;
    }
    @media screen and (max-width: 995px) {
    #header-nav{
        margin-left: 0px;
    }
    }
    @media screen and (max-width: 767px) {
    .img-22{
        margin-top: -2px;
    }
    }
</style>
@endsection

@section('content')
<h1>Pakistan</h1>
@endsection


@section('script')
 
@endsection

