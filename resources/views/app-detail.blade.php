@extends('layouts.admin-master')

@section('title')
User Detail
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('css/customnotification.css')}}">
<style>
[class*="sidebar-dark"]  .user-panel{
    border-bottom: 1px solid #C83448 !important;
  }
</style>
@endsection

@section('content')
<div class="row  pt-1" style="padding-bottom: 13px !important">
    <div class="col">
        <span class="withdrawal-text">User Detail</span>
    </div>
</div>

<div class="row  pt-1 pb-3">
    <div class="col-lg-3 col-md-12">
        <div class="card border-radius8">
            <div class="card-body border-radius8 pt-3 pl-4 pr-4 pb-3">
                <div class="row">
                    <div class="d-flex justify-content-center mb-2">
                        <div id="image-show" class ="roundedImage rounded-circle mt-2">
                        </div>
                    </div>
                    <small class="text-center" style="font-weight:400;color:#212529">Anna Nestrom</small>
                    <small class="text-center light-grey-text ">sami.ullah@techswivel.com</small>
                    <div  class="card m-0 mt-3 pl-0 pb-1 mb-2 border-radius8 off-white shadow-none" style="width: 100%;background-color:#F8E5E8">
                        <div class="card-body m-2 ml-0 p-0 border-radius8">
                            <div class="row pl-1" style="line-height: 1.2;">
                                <div class="col-lg-12 pl-2">
                                    <small class=" pt-1" style="font-weight: 500;font-size:12px;">Current plan</small>
                                    <div class="d-flex align-items-start">
                                        <small class="float-left ml-0 pt-1"  style="font-weight:500;font-size:16px;">Standard Plan</small>
                                    </div>
                                    <div class="d-flex align-items-end">
                                        <small class="pb-0 mb-0" style="font-weight: 500;font-size:12px;">$5:00 / Month</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pt-1 pl-0 pr-0">
                        <i class="bi bi-calendar4-event icon-blue"></i><small class="light-grey-text pl-2">Join Date</small><small class="float-right font-weight500" style="font-size:10px;padding-top:8px">03-01-2021</small>
                    </div>     
                    <div class="dropdown-divider"></div>
                    <div class="pl-0 pr-0">
                        <i class="bi bi-tag icon-blue"></i><small class="light-grey-text pl-2">User ID</small><small class="float-right font-weight500" style="font-weight:bold;font-size:10px;padding-top:6px">00001</small>    
                    </div>
                    <div class="dropdown-divider"></div>
                    <div class="pl-0 pr-0">
                        <i class="bi bi-telephone icon-blue"></i><small class="light-grey-text pl-2">Phone No.</small><small class="float-right font-weight500" style="font-weight:bold;font-size:10px;padding-top:6px">+923314162941</small>    
                    </div>
                    <div class="pt-2 pl-0 pr-0">
                    <div class="col-12 pl-0">
                        <small class=" pt-1" style="font-weight: 500;font-size:14px;">Interests</small>
                    </div>
                    <div class="col-12 mt-1 pl-0">
                        <span class="mt-1 p-1 pl-2 pr-2 font-size12" style="border: 0.5px solid #E1E5F1;border-radius:6px;">Classical</span>
                        <span class="mt-1 p-1 pl-2 pr-2 font-size12" style="border: 0.5px solid #E1E5F1;border-radius:6px;">Rock</span>
                        <span class="mt-1 p-1 pl-2 pr-2 font-size12" style="border: 0.5px solid #E1E5F1;border-radius:6px;">Hip-Hop</span>
                    </div>
                    <div class="row mt-4 ">
                        <div class="col-6 ">
                                <a href="#" id="editProfile"  class="btn btn-block rounded-lg red-button font-weight-lighter text-white border-radius8 edit-profile" data-toggle="modal" data-target="#editUserDetail" >Edit Detail</a>
                        </div>
                        <div class="col-6 ">
                            <a href="#" id="editProfile"  class="btn btn-block rounded-lg float-end orange-button font-weight-lighter text-white border-radius8 edit-profile" data-toggle="modal" data-target="#deleteUserModel" >Delete</a>
                        </div>
                    </div>
                    </div>
                    
                </div>
            </div>
        </div>
        
    </div>
    <div class="col-lg-9 col-md-12" >
                <div class="card" style="height: 580px;overflow: auto">
                    <div class="card-header pb-0" >
                        <div class="row ">
                            <ul class="nav nav-pills ml-auto " style="margin-left: -12px !important;margin-bottom: -1px !important;font-size: 11px;">
                                <li class="nav-item"><span class="nav-link active" href="#tab_1" data-toggle="tab" style="">Playlists</span></li>
                                <li class="nav-item"><span class="nav-link" href="#tab_2" data-toggle="tab">Favorite Songs</span></li>
                                <li class="nav-item"><span class="nav-link" href="#tab_3" data-toggle="tab">Purchased Albums</span></li>
                                <li class="nav-item"><span class="nav-link" href="#tab_4" data-toggle="tab">Purchased Songs</span></li>
                                <li class="nav-item"><span class="nav-link" href="#tab_5" data-toggle="tab">Following Artists</span></li>
                                <li class="nav-item"><span class="nav-link" href="#tab_4" data-toggle="tab">Downloaded</span></li>
                                <li class="nav-item"><span class="nav-link" href="#tab_5" data-toggle="tab">Buying History</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body pt-2 pl-0 pr-0">
                        <div class="tab-content">
                            <div class="tab-pane active table-responsive" id="tab_1">
                                @include('userDetail.user-playlist')
                            </div>
                            <div class="tab-pane " id="tab_2">
                                @include('userDetail.user-favorite-songs')
                            </div>
                            <div class="tab-pane table-responsive" id="tab_3">
                            </div>
                            <div class="tab-pane mt-1 ml-1" id="tab_4">
                            </div>
                            <div class="tab-pane table-responsive" id="tab_5">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>  


{{-- Delete App User Modal --}}
<div class="modal fade" id="deleteUserModel">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle">
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-3 pb-2" >
                <p class="modal-title notification-detail-text"><b>Confirmation</b></p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col notification-detail-textt">
                        <small>Are You Sure You Want To Delete this User?</small>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between ">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">No</button>
                <form>
                    <input type="submit" class="btn red-button btn-xs pt-1 pb-1 pl-4 pr-4 border-radius8" value="Yes">
                </form>
            </div>
        </div>
    </div>
</div>

{{-- Edit User detail Modal --}}
<div class="modal fade" id="editUserDetail">
    <div class="modal-dialog modal-dialog-centered ">
        <div class="modal-content border-radius12">
            <div class="modal-header p-3 pb-2 " >
                <p class="modal-title">Edit User Detail</p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="notificationForm" class="needs-validation" novalidate>
            <div class="modal-body">
                <div class="row m-2">
                    <div class="col-12">
                        <label class="m-0"><small class="text-dark mb-0 font-wegiht600">Name</small></label>
                        <div class="mb-1 mt-1 input-group-sm input_field " >
                            <input type="text" class="form-control input-sm border-radius8"  placeholder="Enter title" id="userName" name="userName" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                            <div class="valid-feedback">Valid!</div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Image <span class="add-song-span-text ">( Max 1MB )</span></span></label>
                            <input type="file" class="form-control input-sm border-radius8 "   placeholder="Choose Image (Max 1MB)" style="font-size: 12.5px" id="userImage" name="userImage" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <small class="text-dark mb-0 font-wegiht600" >Date of birth</small></label>
                            <input type="date" class="form-control input-sm border-radius8 "   placeholder="Choose the date" style="font-size: 12.5px" id="userDateOfBirth" name="userDateOfBirth" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <small class="text-dark mb-0 font-wegiht600" >Gender</small></label>
                            <select  class="form-select input-sm border-radius8 " style="font-size: 12.5px" id="userGender" name="userGender" required>
                                <option value="male" selected>Male</option> 
                                <option value="female" >female</option>
                                <option value="other">other</option>
                            </select>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <label class="m-0"><small class="text-dark add-song-label-text">User Address</small></label>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <small class="text-dark mb-0 font-wegiht600" >Address</small></label>
                            <input type="text" class="form-control input-sm border-radius8 "   placeholder="User Address" style="font-size: 12.5px" id="userAddress" name="userAddress" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                    <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                                    <label class="m-0 mb-1"> <small class="text-dark mb-0 font-wegiht600" >City</small></label>
                                    <input type="text" class="form-control input-sm border-radius8 "   placeholder="User Address" style="font-size: 12.5px" id="userCity" name="userCity" required>
                                <div class="invalid-feedback">Please fill out this field.</div>
                            </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                                    <label class="m-0 mb-1"> <small class="text-dark mb-0 font-wegiht600" >State</small></label>
                                    <input type="text" class="form-control input-sm border-radius8 "   placeholder="User Address" style="font-size: 12.5px" id="userState" name="userState" required>
                                    <div class="invalid-feedback">Please fill out this field.</div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                    <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                                    <label class="m-0 mb-1"> <small class="text-dark mb-0 font-wegiht600" >Country</small></label>
                                    <input type="text" class="form-control input-sm border-radius8 "   placeholder="User Address" style="font-size: 12.5px" id="userCountry" name="userCountry" required>
                                <div class="invalid-feedback">Please fill out this field.</div>
                            </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                                    <label class="m-0 mb-1"> <small class="text-dark mb-0 font-wegiht600" >Zip Code</small></label>
                                    <input type="text" class="form-control input-sm border-radius8 "   placeholder="User Address" style="font-size: 12.5px" id="userZipCode" name="userZipCode" required>
                                    <div class="invalid-feedback">Please fill out this field.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between ">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">Canel</button>
                <input type="submit" class="btn red-button btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8" value="Update">
            </div>
            </form>
        </div>
    </div>
</div>

@endsection



@section('script')
<script>
  // Disable form submissions if there are invalid fields
  (function() {'use strict';
    window.addEventListener('load', function() {
      // Get the forms we want to add validation styles to
      var forms = document.getElementsByClassName('needs-validation');
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
          if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
          }
          form.classList.add('was-validated');
        }, false);
      });
    }, false);
  })();
</script>

@endsection

