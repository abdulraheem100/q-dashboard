@extends('layouts.master')

@section('title')
Notification
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('css/customnotification.css')}}">
<style>
.dataTables_paginate {
    margin-right: 7px !important;
    margin-bottom: 7px !important;
}
</style>
@endsection

@section('content')
<div class="row  pt-1 pb-3">
    <div class="col">
        <span class="withdrawal-text">Send Notification</span>  <button type="button" data-toggle="modal" data-target="#addNotification" class="btn ml-2 red-button btn-xs border-radius8 add-noti"><i class="fas fa-plus"></i> Add new</button>
        <input type= "search" class="form-control ml-3 mr-2 float-right" id="filterSearch"  placeholder="Search">
         <select class="float-right p-1 month-select" id="status" name="status" onchange="getStatusValue();"></select>
       
    </div>
</div>   

{{-- Notification Table --}}
<table id="example" class=" display " >
    <thead>
        <tr>
            <th>Date </th>
            <th>Status</th>
            <th>Notification title</th>
            <th>Notification</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr >
            <td class="p-3">23/01/2011</td>
            <td class="p-3 "><span class="pt-1 pb-1 pl-2 pr-2 text-Rejected border-radius8">Rejected</span></td>
            {{-- <td class="p-3 "><span class="pt-1 pb-1 pl-2 pr-2 text-Approved border-radius8">Approved</span></td> --}}
            {{-- <td class="p-3 "><span class="pt-1 pb-1 pl-2 pr-2 text-Pending border-radius8">Pending</span></td> --}}
            <td class="p-3">New Song Preorder release Date</td>
            <td class="p-3">A good place to start is to tell your audience about your specific area of expertise</td>
            <td class="p-3 float-right d-flex justify-content-start" >
                 <button class="btn text-left btn-xs red-button border-radius8 s-button" style="margin-right: 7px"  data-toggle="modal" data-target="#notificationDetail"><span class="fas fa-eye fa-xs"  id="eye-icon"></span></button>
                <button class="btn text-left btn-xs orange-button border-radius8 s-button" data-toggle="modal" data-target="#deleteNotification"><span class="fas fa-trash fa-xs" id="trash-icon"></span></button>
            </td>
        </tr>
        <tr >
            <td class="p-3">23/01/2011</td>
            <td class="p-3 "><span class="pt-1 pb-1 pl-2 pr-2 text-Approved border-radius8">Approved</span></td>
            <td class="p-3">New Song Preorder release Date</td>
            <td class="p-3">A good place to start is to tell your audience about your specific area of expertise</td>
            <td class="p-3 float-right d-flex justify-content-start" >
                 <button class="btn text-left btn-xs red-button border-radius8 s-button" style="margin-right: 7px"  data-toggle="modal" data-target="#notificationDetail"><span class="fas fa-eye fa-xs"  id="eye-icon"></span></button>
                <button class="btn text-left btn-xs orange-button border-radius8 s-button" data-toggle="modal" data-target="#deleteNotification"><span class="fas fa-trash fa-xs" id="trash-icon"></span></button>
            </td>
        </tr>
        <tr >
            <td class="p-3">23/01/2011</td>
            <td class="p-3 "><span class="pt-1 pb-1 pl-2 pr-2 text-Approved border-radius8">Approved</span></td>
            <td class="p-3">New Song Preorder release Date</td>
            <td class="p-3">A good place to start is to tell your audience about your specific area of expertise</td>
            <td class="p-3 float-right d-flex justify-content-start" >
                 <button class="btn text-left btn-xs red-button border-radius8 s-button" style="margin-right: 7px"  data-toggle="modal" data-target="#notificationDetail"><span class="fas fa-eye fa-xs"  id="eye-icon"></span></button>
                <button class="btn text-left btn-xs orange-button border-radius8 s-button" data-toggle="modal" data-target="#deleteNotification"><span class="fas fa-trash fa-xs" id="trash-icon"></span></button>
            </td>
        </tr>
        <tr >
            <td class="p-3">23/01/2011</td>
            <td class="p-3 "><span class="pt-1 pb-1 pl-2 pr-2 text-Approved border-radius8">Approved</span></td>
            <td class="p-3">New Song Preorder release Date</td>
            <td class="p-3">A good place to start is to tell your audience about your specific area of expertise</td>
            <td class="p-3 float-right d-flex justify-content-start" >
                 <button class="btn text-left btn-xs red-button border-radius8 s-button" style="margin-right: 7px"  data-toggle="modal" data-target="#notificationDetail"><span class="fas fa-eye fa-xs"  id="eye-icon"></span></button>
                <button class="btn text-left btn-xs orange-button border-radius8 s-button" data-toggle="modal" data-target="#deleteNotification"><span class="fas fa-trash fa-xs" id="trash-icon"></span></button>
            </td>
        </tr>
        <tr >
            <td class="p-3">23/01/2011</td>
            <td class="p-3 "><span class="pt-1 pb-1 pl-2 pr-2 text-Approved border-radius8">Approved</span></td>
            <td class="p-3">New Song Preorder release Date</td>
            <td class="p-3">A good place to start is to tell your audience about your specific area of expertise</td>
            <td class="p-3 float-right d-flex justify-content-start" >
                 <button class="btn text-left btn-xs red-button border-radius8 s-button" style="margin-right: 7px"  data-toggle="modal" data-target="#notificationDetail"><span class="fas fa-eye fa-xs"  id="eye-icon"></span></button>
                <button class="btn text-left btn-xs orange-button border-radius8 s-button" data-toggle="modal" data-target="#deleteNotification"><span class="fas fa-trash fa-xs" id="trash-icon"></span></button>
            </td>
        </tr>
        <tr >
            <td class="p-3">23/01/2011</td>
            <td class="p-3 "><span class="pt-1 pb-1 pl-2 pr-2 text-Approved border-radius8">Approved</span></td>
            <td class="p-3">New Song Preorder release Date</td>
            <td class="p-3">A good place to start is to tell your audience about your specific area of expertise</td>
            <td class="p-3 float-right d-flex justify-content-start" >
                 <button class="btn text-left btn-xs red-button border-radius8 s-button" style="margin-right: 7px"  data-toggle="modal" data-target="#notificationDetail"><span class="fas fa-eye fa-xs"  id="eye-icon"></span></button>
                <button class="btn text-left btn-xs orange-button border-radius8 s-button" data-toggle="modal" data-target="#deleteNotification"><span class="fas fa-trash fa-xs" id="trash-icon"></span></button>
            </td>
        </tr>
        <tr >
            <td class="p-3">23/01/2011</td>
            <td class="p-3 "><span class="pt-1 pb-1 pl-2 pr-2 text-Rejected border-radius8">Rejected</span></td>
            <td class="p-3">New Song Preorder release Date</td>
            <td class="p-3">A good place to start is to tell your audience about your specific area of expertise</td>
            <td class="p-3 float-right d-flex justify-content-start" >
                 <button class="btn text-left btn-xs red-button border-radius8 s-button" style="margin-right: 7px"  data-toggle="modal" data-target="#notificationDetail"><span class="fas fa-eye fa-xs"  id="eye-icon"></span></button>
                <button class="btn text-left btn-xs orange-button border-radius8 s-button" data-toggle="modal" data-target="#deleteNotification"><span class="fas fa-trash fa-xs" id="trash-icon"></span></button>
            </td>
        </tr>
        <tr >
            <td class="p-3">23/01/2011</td>
            <td class="p-3 "><span class="pt-1 pb-1 pl-2 pr-2 text-Rejected border-radius8">Rejected</span></td>
            <td class="p-3">New Song Preorder release Date</td>
            <td class="p-3">A good place to start is to tell your audience about your specific area of expertise</td>
            <td class="p-3 float-right d-flex justify-content-start" >
                 <button class="btn text-left btn-xs red-button border-radius8 s-button" style="margin-right: 7px"  data-toggle="modal" data-target="#notificationDetail"><span class="fas fa-eye fa-xs"  id="eye-icon"></span></button>
                <button class="btn text-left btn-xs orange-button border-radius8 s-button" data-toggle="modal" data-target="#deleteNotification"><span class="fas fa-trash fa-xs" id="trash-icon"></span></button>
            </td>
        </tr>
        <tr >
            <td class="p-3">23/01/2011</td>
            <td class="p-3 "><span class="pt-1 pb-1 pl-2 pr-2 text-Rejected border-radius8">Rejected</span></td>
            <td class="p-3">New Song Preorder release Date</td>
            <td class="p-3">A good place to start is to tell your audience about your specific area of expertise</td>
            <td class="p-3 float-right d-flex justify-content-start" >
                 <button class="btn text-left btn-xs red-button border-radius8 s-button" style="margin-right: 7px"  data-toggle="modal" data-target="#notificationDetail"><span class="fas fa-eye fa-xs"  id="eye-icon"></span></button>
                <button class="btn text-left btn-xs orange-button border-radius8 s-button" data-toggle="modal" data-target="#deleteNotification"><span class="fas fa-trash fa-xs" id="trash-icon"></span></button>
            </td>
        </tr>
        <tr >
            <td class="p-3">23/01/2011</td>
            <td class="p-3 "><span class="pt-1 pb-1 pl-2 pr-2 text-Rejected border-radius8">Rejected</span></td>
            <td class="p-3">New Song Preorder release Date</td>
            <td class="p-3">A good place to start is to tell your audience about your specific area of expertise</td>
            <td class="p-3 float-right d-flex justify-content-start" >
                 <button class="btn text-left btn-xs red-button border-radius8 s-button" style="margin-right: 7px"  data-toggle="modal" data-target="#notificationDetail"><span class="fas fa-eye fa-xs"  id="eye-icon"></span></button>
                <button class="btn text-left btn-xs orange-button border-radius8 s-button" data-toggle="modal" data-target="#deleteNotification"><span class="fas fa-trash fa-xs" id="trash-icon"></span></button>
            </td>
        </tr>
        <tr >
            <td class="p-3">23/01/2011</td>
            <td class="p-3 "><span class="pt-1 pb-1 pl-2 pr-2 text-Rejected border-radius8">Rejected</span></td>
            <td class="p-3">New Song Preorder release Date</td>
            <td class="p-3">A good place to start is to tell your audience about your specific area of expertise</td>
            <td class="p-3 float-right d-flex justify-content-start" >
                 <button class="btn text-left btn-xs red-button border-radius8 s-button" style="margin-right: 7px"  data-toggle="modal" data-target="#notificationDetail"><span class="fas fa-eye fa-xs"  id="eye-icon"></span></button>
                <button class="btn text-left btn-xs orange-button border-radius8 s-button" data-toggle="modal" data-target="#deleteNotification"><span class="fas fa-trash fa-xs" id="trash-icon"></span></button>
            </td>
        </tr>
        <tr >
            <td class="p-3">23/01/2011</td>
            <td class="p-3 "><span class="pt-1 pb-1 pl-2 pr-2 text-Rejected border-radius8">Rejected</span></td>
            <td class="p-3">New Song Preorder release Date</td>
            <td class="p-3">A good place to start is to tell your audience about your specific area of expertise</td>
            <td class="p-3 float-right d-flex justify-content-start" >
                 <button class="btn text-left btn-xs red-button border-radius8 s-button" style="margin-right: 7px"  data-toggle="modal" data-target="#notificationDetail"><span class="fas fa-eye fa-xs"  id="eye-icon"></span></button>
                <button class="btn text-left btn-xs orange-button border-radius8 s-button" data-toggle="modal" data-target="#deleteNotification"><span class="fas fa-trash fa-xs" id="trash-icon"></span></button>
            </td>
        </tr>
        <tr >
            <td class="p-3">23/01/2011</td>
            <td class="p-3 "><span class="pt-1 pb-1 pl-2 pr-2 text-Rejected border-radius8">Rejected</span></td>
            <td class="p-3">New Song Preorder release Date</td>
            <td class="p-3">A good place to start is to tell your audience about your specific area of expertise</td>
            <td class="p-3 float-right d-flex justify-content-start" >
                 <button class="btn text-left btn-xs red-button border-radius8 s-button" style="margin-right: 7px"  data-toggle="modal" data-target="#notificationDetail"><span class="fas fa-eye fa-xs"  id="eye-icon"></span></button>
                <button class="btn text-left btn-xs orange-button border-radius8 s-button" data-toggle="modal" data-target="#deleteNotification"><span class="fas fa-trash fa-xs" id="trash-icon"></span></button>
            </td>
        </tr>
        <tr >
            <td class="p-3">23/01/2011</td>
            <td class="p-3 "><span class="pt-1 pb-1 pl-2 pr-2 text-Pending border-radius8">Pending</span></td>
            <td class="p-3">New Song Preorder release Date</td>
            <td class="p-3">A good place to start is to tell your audience about your specific area of expertise</td>
            <td class="p-3 float-right d-flex justify-content-start" >
                 <button class="btn text-left btn-xs red-button border-radius8 s-button" style="margin-right: 7px"  data-toggle="modal" data-target="#notificationDetail"><span class="fas fa-eye fa-xs"  id="eye-icon"></span></button>
                <button class="btn text-left btn-xs orange-button border-radius8 s-button" data-toggle="modal" data-target="#deleteNotification"><span class="fas fa-trash fa-xs" id="trash-icon"></span></button>
            </td>
        </tr>
        <tr >
            <td class="p-3">23/01/2011</td>
            <td class="p-3 "><span class="pt-1 pb-1 pl-2 pr-2 text-Pending border-radius8">Pending</span></td>
            <td class="p-3">New Song Preorder release Date</td>
            <td class="p-3">A good place to start is to tell your audience about your specific area of expertise</td>
            <td class="p-3 float-right d-flex justify-content-start" >
                 <button class="btn text-left btn-xs red-button border-radius8 s-button" style="margin-right: 7px"  data-toggle="modal" data-target="#notificationDetail"><span class="fas fa-eye fa-xs"  id="eye-icon"></span></button>
                <button class="btn text-left btn-xs orange-button border-radius8 s-button" data-toggle="modal" data-target="#deleteNotification"><span class="fas fa-trash fa-xs" id="trash-icon"></span></button>
            </td>
        </tr>
        <tr >
            <td class="p-3">23/01/2011</td>
            <td class="p-3 "><span class="pt-1 pb-1 pl-2 pr-2 text-Pending border-radius8">Pending</span></td>
            <td class="p-3">New Song Preorder release Date</td>
            <td class="p-3">A good place to start is to tell your audience about your specific area of expertise</td>
            <td class="p-3 float-right d-flex justify-content-start" >
                 <button class="btn text-left btn-xs red-button border-radius8 s-button" style="margin-right: 7px"  data-toggle="modal" data-target="#notificationDetail"><span class="fas fa-eye fa-xs"  id="eye-icon"></span></button>
                <button class="btn text-left btn-xs orange-button border-radius8 s-button" data-toggle="modal" data-target="#deleteNotification"><span class="fas fa-trash fa-xs" id="trash-icon"></span></button>
            </td>
        </tr>
        <tr >
            <td class="p-3">23/01/2011</td>
            <td class="p-3 "><span class="pt-1 pb-1 pl-2 pr-2 text-Pending border-radius8">Pending</span></td>
            <td class="p-3">New Song Preorder release Date</td>
            <td class="p-3">A good place to start is to tell your audience about your specific area of expertise</td>
            <td class="p-3 float-right d-flex justify-content-start" >
                 <button class="btn text-left btn-xs red-button border-radius8 s-button" style="margin-right: 7px"  data-toggle="modal" data-target="#notificationDetail"><span class="fas fa-eye fa-xs"  id="eye-icon"></span></button>
                <button class="btn text-left btn-xs orange-button border-radius8 s-button" data-toggle="modal" data-target="#deleteNotification"><span class="fas fa-trash fa-xs" id="trash-icon"></span></button>
            </td>
        </tr>
        <tr >
            <td class="p-3">23/01/2011</td>
            <td class="p-3 "><span class="pt-1 pb-1 pl-2 pr-2 text-Pending border-radius8">Pending</span></td>
            <td class="p-3">New Song Preorder release Date</td>
            <td class="p-3">A good place to start is to tell your audience about your specific area of expertise</td>
            <td class="p-3 float-right d-flex justify-content-start" >
                 <button class="btn text-left btn-xs red-button border-radius8 s-button" style="margin-right: 7px"  data-toggle="modal" data-target="#notificationDetail"><span class="fas fa-eye fa-xs"  id="eye-icon"></span></button>
                <button class="btn text-left btn-xs orange-button border-radius8 s-button" data-toggle="modal" data-target="#deleteNotification"><span class="fas fa-trash fa-xs" id="trash-icon"></span></button>
            </td>
        </tr>
        <tr >
            <td class="p-3">23/01/2011</td>
            <td class="p-3 "><span class="pt-1 pb-1 pl-2 pr-2 text-Pending border-radius8">Pending</span></td>
            <td class="p-3">New Song Preorder release Date</td>
            <td class="p-3">A good place to start is to tell your audience about your specific area of expertise</td>
            <td class="p-3 float-right d-flex justify-content-start" >
                 <button class="btn text-left btn-xs red-button border-radius8 s-button" style="margin-right: 7px"  data-toggle="modal" data-target="#notificationDetail"><span class="fas fa-eye fa-xs"  id="eye-icon"></span></button>
                <button class="btn text-left btn-xs orange-button border-radius8 s-button" data-toggle="modal" data-target="#deleteNotification"><span class="fas fa-trash fa-xs" id="trash-icon"></span></button>
            </td>
        </tr>
    </tbody>
</table>

{{-- Add Notification Modal --}}
<div class="modal fade" id="addNotification">
    <div class="modal-dialog modal-dialog-centered ">
        <div class="modal-content border-radius12">
            <div class="modal-header p-3 pb-2 " >
                <small class="modal-title"><b>New Notification (5/10 left)</b></small>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="notificationForm" class="needs-validation" novalidate>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <label class="m-0"><small class="text-dark mb-0 font-wegiht600"><b>Title</b></small></label>
                        <div class="mb-1 mt-1 input-group-sm input_field " >
                            <input type="text" class="form-control input-sm border-radius8"  placeholder="Enter title" id="title" name="title" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                            <div class="valid-feedback">Valid!</div>
                        </div>
                        <label class="m-0 mt-1"><small class="text-dark mb-0 font-wegiht600"><b>Message</b></small></label>
                        <div class="mb-1 mt-1 input-group-sm input_field " >
                            <textarea rows="7" cols="50" name="message"  id="message"  class="form-control input-xs rounded border-radius8"  placeholder="Enter Notification Message" form="notificationForm" required></textarea>
                            <div class="invalid-feedback">Please fill out this field.</div>
                            <div class="valid-feedback">Valid!</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between ">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">Canel</button>
                <input type="submit" class="btn red-button btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8" value="Send For Approval">
            </div>
            </form>
        </div>
    </div>
</div>


{{-- Notification detail Modal --}}
<div class="modal fade" id="notificationDetail">
    <div class="modal-dialog modal-dialog-centered modal-sm ">
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-3 pb-2 pr-2" >
                <p class="modal-title notification-detail-text" ><b>Notification Detail </b></p>
                <button type="button" class="close"  data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <p class="notification-title-text"><b>New Song Preorder Release Date</b></p>
                    </div>
                    <div class="col-12 mb-3 notification-de-text" >
                        <small>A Good Place To Start is Tell Your Audience About Your specific Area Of Expertise.Do You Have A Formal Education In Arts Or Arts History</small>
                    </div>
                    <div class="dropdown-divider"></div>
                    <div class="pl-2 pr-2">
                        <small class="float-left">Date</small>
                        <small class="float-right">23/01/2021</small>
                    </div>
                </div>
            </div>
            <div class="modal-footer p-2 float-right" style="margin-left: 4px">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>



{{-- Delete Notification Modal --}}
<div class="modal fade" id="deleteNotification">
    <div class="modal-dialog modal-dialog-centered modal-sm ">
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-3 pb-2" >
                <p class="modal-title notification-detail-text"><b>Delete Notification</b></p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col notification-detail-textt">
                        <small>Are You Sure You Want To Delete</small>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between ">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">No</button>
                <form>
                    <input type="submit" class="btn red-button btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8" value="Confirm">
                </form>
            </div>
        </div>
    </div>
</div>

@endsection


@section('script')
 <script>
    // Disable form submissions if there are invalid fields
    (function() {
      'use strict';
      window.addEventListener('load', function() {
        // Get the forms we want to add validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
          form.addEventListener('submit', function(event) {
            if (form.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
            }
            form.classList.add('was-validated');
          }, false);
        });
      }, false);
    })();
    </script>
    <script>
    $(document).ready(function() {
        table = $('#example').DataTable({
            dom:"<'row'<'col'<'card m-2 border-radius8lr'<'.card-header p-2 pl-3'<'allnoti'>il><'card-body p-0 table-responsive'<rt>>>p>>",
            "initComplete": function(settings, json) {
                $('.dataTables_info').addClass('dataTables_info');
                $('.dataTables_info').addClass('dataTables_info');
        
       }
        });
         $("div.allnoti").html('<span class="pr-2 all-noti-text"style="">All Notifications</span>');
     $('#filterSearch').keyup(function(){
       table.search(this.value).draw();
       });
         
    });
    function getStatusValue(){
        var conceptName = $('#status').find(":selected").text();
        if(conceptName == 'All')
        {
            table.search('').draw();
             
        }else{
            table.search(conceptName).draw();
        }
        
    } 
    </script>
    <script>
     

    </script>
    <script>
        (function () {
    let months = ["Pending", "Rejected", "Approved"];
    var month_selected = 'All';     //(new Date).getMonth(); // current month
    var option = '';
    option = '<option value="'+00+'">All</option>'; // first option

    for (let i = 0; i < months.length; i++) {
        let month_number = (i + 1);

        // value month number with 0. [01 02 03 04..]
        let month = (month_number <= 9) ? '0' + month_number : month_number;


        let selected = (i === month_selected ? ' selected' : '');
        option += '<option value="' + month + '"' + selected + '>' + months[i] + '</option>';
    }
    document.getElementById("status").innerHTML = option;
})();

    </script>
@endsection
