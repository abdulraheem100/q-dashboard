@extends('layouts.master')

@section('title')
Dashboard
@endsection

@section('style')
<style>
.sidenav {
  height: 100%;
  width: 0;
  position: fixed;
  z-index: 1;
  top: 0;
  right: 0;
  background-color: #fff;
  overflow-x: hidden;
  transition: 0.5s;
  padding-top: 60px;
  
}
#mySideClose{
  border-radius: 32%;
  height: 37px;
  width: 0;
  position: absolute;
  z-index: 1;
  top: 123px;
  right:260px;
  background-color: #fff;
  overflow-x: hidden;
  transition: 0.5s;
  padding-top: 0px;
margin-top: -46px;

}
#mySideClose a {
    
  /* padding: 8px 8px 8px 32px; */
  text-decoration: none;
  font-size: 25px;
  color: #ba011a;
  display: block;
  transition: 0.3s;
}
.sidenav a {
  padding: 8px 8px 8px 32px;
  text-decoration: none;
  font-size: 25px;
  color: #818181;
  display: block;
  transition: 0.3s;
}
#mySideClose a:hover{
  color: #f1f1f1;  
}
.sidenav a:hover {
  color: #f1f1f1;
}
#mySideClose .closebtn{
  position: absolute;
  top: 0;
  right: 25px;
  font-size: 36px;
  margin-left: 50px;  
}
.sidenav .closebtn {
  position: absolute;
  top: 0;
  right: 25px;
  font-size: 36px;
  margin-left: 50px;
}

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}
@media screen and (max-width: 290px) {
  #mySideClose{
    right: 251px !important;    
  }
}
.newBuy::-webkit-scrollbar {
  width: 5px;
}

.newBuy::-webkit-scrollbar-thumb {
  background: #888; 
}

.newBuy::-webkit-scrollbar-thumb:hover {
  background: #555; 
}
</style>
@endsection
@section('content')
<div class="row pt-0 pl-1">
    <p class="withdrawal-text ">Dashboard</p>
</div>
<div class="row pt-0 pl-1">
    <div class="col-lg-6">
         <div class="card">
              <div class="card-header border-0 pl-2 pt-3">
                <div class="d-flex justify-content-between">
                    <small class="float-left ml-2 pt-1"  style="font-weight:500">Sales Report</small>
                     <select class="  month-select" id="saleReport" name="saleReport" ></select>
                </div>
              </div>
              <div class="card-body pl-2 pr-2 pt-0 pb-3">
                <div class="d-flex">
                  <p class="d-flex flex-column">
                    <small class="float-left ml-2 pt-1"  style="font-weight:500">Total Sales</small>
                    <small class="float-left ml-2 pt-1"  style="font-weight:800">$4030.00
                         <span class="text-success" style="font-weight:500"><i class="fas fa-arrow-up"></i> 33.1% <span class="text-muted" style="font-weight:400">Since last month</span></span>
                    </small>
                  </p>
                </div>
                <!-- /.d-flex -->

                <div class="position-relative mb-1">
                  <canvas id="sales-chart" height="170"></canvas>
                </div>

                <div class="d-flex flex-row justify-content-end">
                  <span class="mr-2">
                    <i class="fas fa-square black-red-text"></i> <small class=" ml-0 pt-1"  style="font-weight:500">This Year</small>
                  </span>
                </div>
              </div>
            </div>
    </div>
    <div class="col-lg-6">
         <div class="card" style="height: 328px">
              <div class="card-header border-0">
               <div class="d-flex justify-content-between">
                    <small class="  pt-1"  style="font-weight:500">New Buying</small>
                    <a href="/artist"><small class="  pt-1"  style="font-weight:500">View Details</small></a>
               </div> 
              </div>
              <div class="card-body table-responsive newBuy p-0">
                <table class="table table-striped table-valign-middle" style="font-size: 12px">
                  <thead>
                  <tr>
                    <th>#No</th>
                    <th>User</th>
                    <th>Purchased at</th>
                    <th>Song/Album</th>
                    <th></th>
                  </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="border-none p-2 ">00001</td>
                      <td class="border-none p-2">
                        <div class="d-flex flex-row">
                          <div id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0"></div>
                          <div class="ml-2 padding-top5">Anna Nestron</div>
                        </div>
                      </td>
                      <td class="border-none p-2 ">25/01/2021.5:00AM</td>
                      <td class="border-none p-2 ">Song</td>
                      <td class="border-none p-2 "><button type="button" style="cursor:pointer" onclick="openNav()" id="viewEarn" data-toggle="modal" data-target="#" class="btn ml-2 p-1 pl-2 pr-2 red-button btn-xs border-radius8 font-size10"><i class="bi bi-folder-fill "></i> View</button></td>
                    </tr>
                    <tr>
                      <td class="border-none p-2 ">00001</td>
                      <td class="border-none p-2">
                        <div class="d-flex flex-row">
                          <div id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0"></div>
                          <div class="ml-2 padding-top5">Anna Nestron</div>
                        </div>
                      </td>
                      <td class="border-none p-2 ">25/01/2021.5:00AM</td>
                      <td class="border-none p-2 ">Song</td>
                      <td class="border-none p-2 "><button type="button" style="cursor:pointer" onclick="openNav()" id="viewEarn" data-toggle="modal" data-target="#" class="btn ml-2 p-1 pl-2 pr-2 red-button btn-xs border-radius8 font-size10"><i class="bi bi-folder-fill "></i> View</button></td>
                    </tr>
                    <tr>
                      <td class="border-none p-2 ">00001</td>
                      <td class="border-none p-2">
                        <div class="d-flex flex-row">
                          <div id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0"></div>
                          <div class="ml-2 padding-top5">Anna Nestron</div>
                        </div>
                      </td>
                      <td class="border-none p-2 ">25/01/2021.5:00AM</td>
                      <td class="border-none p-2 ">Song</td>
                      <td class="border-none p-2 "><button type="button" style="cursor:pointer" onclick="openNav()" id="viewEarn" data-toggle="modal" data-target="#" class="btn ml-2 p-1 pl-2 pr-2 red-button btn-xs border-radius8 font-size10"><i class="bi bi-folder-fill "></i> View</button></td>
                    </tr>
                    <tr>
                      <td class="border-none p-2 ">00001</td>
                      <td class="border-none p-2">
                        <div class="d-flex flex-row">
                          <div id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0"></div>
                          <div class="ml-2 padding-top5">Anna Nestron</div>
                        </div>
                      </td>
                      <td class="border-none p-2 ">25/01/2021.5:00AM</td>
                      <td class="border-none p-2 ">Song</td>
                      <td class="border-none p-2 "><button type="button" style="cursor:pointer" onclick="openNav()" id="viewEarn" data-toggle="modal" data-target="#" class="btn ml-2 p-1 pl-2 pr-2 red-button btn-xs border-radius8 font-size10"><i class="bi bi-folder-fill "></i> View</button></td>
                    </tr>
                    <tr>
                      <td class="border-none p-2 ">00001</td>
                      <td class="border-none p-2">
                        <div class="d-flex flex-row">
                          <div id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0"></div>
                          <div class="ml-2 padding-top5">Anna Nestron</div>
                        </div>
                      </td>
                      <td class="border-none p-2 ">25/01/2021.5:00AM</td>
                      <td class="border-none p-2 ">Song</td>
                      <td class="border-none p-2 "><button type="button" style="cursor:pointer" onclick="openNav()" id="viewEarn" data-toggle="modal" data-target="#" class="btn ml-2 p-1 pl-2 pr-2 red-button btn-xs border-radius8 font-size10"><i class="bi bi-folder-fill "></i> View</button></td>
                    </tr>
                    <tr>
                      <td class="border-none p-2 ">00001</td>
                      <td class="border-none p-2">
                        <div class="d-flex flex-row">
                          <div id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0"></div>
                          <div class="ml-2 padding-top5">Anna Nestron</div>
                        </div>
                      </td>
                      <td class="border-none p-2 ">25/01/2021.5:00AM</td>
                      <td class="border-none p-2 ">Song</td>
                      <td class="border-none p-2 "><button type="button" style="cursor:pointer" onclick="openNav()" id="viewEarn" data-toggle="modal" data-target="#" class="btn ml-2 p-1 pl-2 pr-2 red-button btn-xs border-radius8 font-size10"><i class="bi bi-folder-fill "></i> View</button></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
    </div>
</div>

<div id="mySidenav" class="sidenav shadow">
    
  <div class="row m-0 mb-1 mt-1">
    <div class="col ">
        <small class="float-left ml-0 pt-1"  style="font-weight:500">User Detail</small> 
  
    </div>
    <ul class="products-list product-list-in-card pl-2 pr-1">
        <li class="item">
            <div class="product-img pt-1">
                <img src="{{asset('image/blog/blog3.jpg') }}" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
            </div>
            <div class="product-info ml-5">
                <small  class="product-title font-szie10">Khawar Hussain</small>
                <small class="product-description font-szie10 light-grey-text">khawar.hussain@gmail.com</small>
            </div>
        </li>
    </ul>
    <small class="float-left ml-0 pt-1"  style="font-weight:500">Purchased items [Preorder]</small> 
    <ul class="products-list product-list-in-card pl-2 pr-1">
        <li class="item" style="border-bottom: none">
            <div class="product-img pt-1">
                <img src="{{asset('image/blog/blog3.jpg') }}" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
            </div>
            <div class="product-info ml-5">
                <small  class="product-title font-szie10 ">Life on Mars<span class="float-right pr-3">$2</span></small>
                <small class="product-description font-szie10 light-grey-text">3:56</small>
            </div>
        </li>
        <li class="item" style="border-bottom: none">
            <div class="product-img pt-1">
                <img src="{{asset('image/blog/blog3.jpg') }}" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
            </div>
            <div class="product-info ml-5">
                <small  class="product-title font-szie10 ">Life on Mars<span class="float-right pr-3">$5</span></small>
                <small class="product-description font-szie10 light-grey-text">3:56</small>
            </div>
        </li>
        <li class="item" style="border-bottom: none">
            <div class="product-img pt-1">
                <img src="{{asset('image/blog/blog3.jpg') }}" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
            </div>
            <div class="product-info ml-5">
                <small  class="product-title font-szie10 ">Life on Mars<span class="float-right pr-3">$10</span></small>
                <small class="product-description font-szie10 light-grey-text">3:56</small>
            </div>
        </li>
        <li class="item" style="border-bottom: none">
            <div class="product-img pt-1">
                <img src="{{asset('image/blog/blog3.jpg') }}" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
            </div>
            <div class="product-info ml-5">
                <small  class="product-title font-szie10 ">Life on Mars<span class="float-right pr-3">$10</span></small>
                <small class="product-description font-szie10 light-grey-text">3:56</small>
            </div>
        </li>
        <li class="item" style="border-bottom: none">
            <div class="product-img pt-1">
                <img src="{{asset('image/blog/blog3.jpg') }}" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
            </div>
            <div class="product-info ml-5">
                <small  class="product-title font-szie10 ">Life on Mars<span class="float-right pr-3">$2</span></small>
                <small class="product-description font-szie10 light-grey-text">3:56</small>
            </div>
        </li>
        <li class="item" style="border-bottom: none">
            <div class="product-img pt-1">
                <img src="{{asset('image/blog/blog3.jpg') }}" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
            </div>
            <div class="product-info ml-5">
                <small  class="product-title font-szie10 ">Life on Mars<span class="float-right pr-3">$5</span></small>
                <small class="product-description font-szie10 light-grey-text">3:56</small>
            </div>
        </li>
    </ul>
    <div  class="card m-2 pl-0 mr-5 border-radius8 off-white " style="width: 94%">
        <div class="card-body m-2 ml-0 p-0 border-radius8">
            <div class="row">
                <div class="col-lg-12">
                    <div class="d-flex align-items-start">
                        <small class="float-left ml-0 pt-1"  style="font-weight:500">Address</small>
                    </div>
                    <div class="d-flex align-items-end">
                         <p class="pb-2  userAccountNumber">885B Faisal Town Lahore,Pakistan</p>
                    </div>
               </div>
            </div>
        </div>
      </div>
  </div>
</div>    

    

<div id="mySideClose" class="shadow">
  <div class="row m-0 ">
    <div class="col " style="padding-left:7px">
         <a href="javascript:void(0)"  onclick="closeNav()"><i class="bi bi-x"></i></a>
    </div>
 </div>
</div> 
@endsection


@section('script')
<script>
   /* global Chart:false */

$(function () {
  'use strict'

  var ticksStyle = {
    fontColor: '#242424',
    fontStyle: 'lighter',
    fontSize:'9',
  }

  var mode = 'index'
  var intersect = true

  var $salesChart = $('#sales-chart')
  // eslint-disable-next-line no-unused-vars
  var salesChart = new Chart($salesChart, {
    type: 'bar',
    data: {
      labels: ['Jan','Feb','Mar','Apr','Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
      datasets: [
        {
          backgroundColor: '#242424',
          borderColor: '#242424',
          data: [700, 1100, 1100, 1000, 800, 500, 1000,300, 1700, 2700, 2000, 1800, 1500, 2000]
        }
      ]
    },
    options: {  
      maintainAspectRatio: false,
      tooltips: {
        mode: mode,
        intersect: intersect
      },
      hover: {
        mode: mode,
        intersect: intersect
      },
      legend: {
        display: false
      },
      scales: {
        yAxes: [{
          // display: false,
          gridLines: {
            display: true,
            lineWidth: '6px',
            color: 'rgba(0, 0, 0, .2)',
            zeroLineColor: 'transparent'
          },
          ticks:{
              display: false,
          }
          
        }],
        xAxes: [{
            barPercentage: 0.6,
          display: true,
          gridLines: {
            display: false
          },
          ticks: ticksStyle
        }]
      }
    }
  })

})

</script>
<script>
        (function () {
    let months = ["Year 2021", "Year 2020", "Year 2019"];
    var month_selected = 'All';     //(new Date).getMonth(); // current month
    var option = '';
    option = '<option value="'+00+'">Year </option>'; // first option

    for (let i = 0; i < months.length; i++) {
        let month_number = (i + 1);

        // value month number with 0. [01 02 03 04..]
        let month = (month_number <= 9) ? '0' + month_number : month_number;


        let selected = (i === month_selected ? ' selected' : '');
        option += '<option value="' + month + '"' + selected + '>' + months[i] + '</option>';
    }
    document.getElementById("saleReport").innerHTML = option;
})();

    </script>
<script>
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
  document.getElementById("mySideClose").style.width = "37px";
}


function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("mySideClose").style.width = "0";
}
</script>
@endsection