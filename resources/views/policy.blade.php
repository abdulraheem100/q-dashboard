@extends('layouts.master')

@section('title')
Privacy Policy
@endsection


@section('style')

@endsection
@section('content')

<div class="row">
    <div class="col-lg-6 offset-lg-3" style="margin-top: 35px">
        <h5 class="mb-3"><b>Privacy Policy</b></h5>
        <p class=" grey-text">By accessing the Confessional Prayer Bible (CPB) you agree to this privacy policy and its terms and consent to having your data tranferred to and processes in the United States. Your use of CPB is alos governed by our Terms of Use .Please read those terms caefully as well.</p>
        <p class=" grey-text">Here's summary of what you can expect to find in our Privary Policy, which covers all CPB-branded products and services.</p>
        <h5 class="mb-3">How we use Your data to make your QMusic experience  more personal.</h5>
        <p class=" grey-text">This Privary Policy outlines the types of data we collect from your interaction with CPB, as well as how we process that information to enhance your CPB experience. When you create a CPB account or use any one of our applications or sites, the information we collect is for the purpose of offering a more personalized experience</p>
        <h5 class="mb-3">Your privacy protected.</h5>
        <p class=" grey-text">We take the privacy of the information you provide and that we collect seriously and we implement security safeguards designed to protect your dataas discussed below. We do not share your personally identifiable data with any third-party advertisers ad networks for third-party advertising purposes.</p>
        <h5 class="mb-3">It's your experience</h5>
        <p class=" grey-text">You have choices about how your personal information is accessed, collected, shared, and stored by Rabboni Books, which are discussed below. You will make choices about our use and processsing of your information when you first engage CPB and when you enage certain CPB functionality and may also make certain choices in the settings menu of your CPB Member account.</p>
        <h5 class="mb-3">We welcome your questions and comments.</h5>
        <p class=" grey-text">We welcome any questions or comments you may have about this Privacy Policy and our privacy practices. If you have any questions or comments, you may contact us by mail at Rabboni Books, Attn. CPB Support, 1700 Broadway Ave. N, Suite #150, Rochester MN 55906, or by email at tell@acts1038.com </p>
        <p class=" grey-text">Where CPB has provided you with a translation other than the English language version of the Privacy Policy, then you agree that the translation is provided for your convenience only and that the English language version of the Privacy Policy will govern your relationship with CPB. If there is any contradiction between.</p>
    </div>
</div>
@endsection


@section('script')

@endsection