<aside class="main-sidebar sidebar-dark-primary elevation-4 text-xs red-background" >
    <!-- Brand Logo -->
<div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-1  pb-0 mb-0 d-flex" style="margin-top: 4px !important;">
      <div class="text-center">
        <div class="row d-flex align-items-center">
          <p class="" style="color: white;font-size: 15px;margin-top: 1px !important;">Q The Music</p>
        </div>
        </div>
    </div>
    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item width180">
          <a href="/admin/dashboard" class="nav-link white-color width180">
            <i class="nav-icon fas fa-tachometer-alt"></i> 
            <p>
              Dashboard 
            </p>
          </a>
        </li>
        <li class="nav-item width180">
          <a href="/admin/app-users" class="nav-link white-color width180">
            <i class="nav-icon bi bi-person"></i>
            <p>
                App Users 
            </p>
          </a>
        </li>
        <li class="nav-item width180">
          <a href="#" class="nav-link white-color width180">
            <i class="nav-icon far fa-bell"></i>
            <p>
                Notifications
            </p>
          </a>
        </li>
        <li class="nav-item width180">
          <a href="/admin/music-category" class="nav-link white-color width180">
            <i class="nav-icon bi bi-music-note-list"></i>
            <p>
                Music Categories 
            </p>
          </a>
        </li>
        <li class="nav-item width180">
             <a href="#" class="nav-link white-color width180">
            <i class="nav-icon fas fa-guitar"></i>
            <p>
                Artists
                <i class="fas fa-angle-left right"></i>
            </p>
          </a>
            <ul class="nav nav-treeview">
              <li class="nav-item width180">
                <a href="#" class="nav-link white-color width180">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Active Artists</p>
                </a>
              </li>
              <li class="nav-item width180">
                <a href="#" class="nav-link white-color width180">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Blocked Artists</p>
                </a>
              </li>
              <li class="nav-item width180">
                <a href="#" class="nav-link white-color width180">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Notifications</p>
                </a>
              </li>
            </ul>
          </li>

        <li class="nav-item width180">
            <a href="#" class="nav-link white-color width180">
                <i class="nav-icon bi bi-box-seam"></i>
              <p>
                Packages 
              </p>
            </a>
        </li>
        <li class="nav-item width180">
            <a href="#" class="nav-link white-color width180">
            <i class="nav-icon fas fa-money-check-alt"></i>
            <p>
                Earnings
                <i class="fas fa-angle-left right"></i>
            </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item width180">
                <a href="#" class="nav-link white-color width180">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Purchased History</p>
                </a>
              </li>
              <li class="nav-item width180">
                <a href="#" class="nav-link white-color width180">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Artist Earnings</p>
                </a>
              </li>
            </ul>
        </li>
        <li class="nav-item width180">
            <a href="#" class="nav-link white-color width180">
            <i class="nav-icon bi bi-badge-ad"></i>
            <p>
                Ads Management
                <i class="fas fa-angle-left right"></i>
            </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item width180">
                <a href="#" class="nav-link white-color width180">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Ads</p>
                </a>
              </li>
            </ul>
        </li>

        <li class="nav-item width180">
            <a href="#" class="nav-link white-color width180">
                <i class="nav-icon far fa-handshake"></i>
              <p>
                Terms and conditions 
              </p>
            </a>
          </li>
          <li class="nav-item width180">
            <a href="#" class="nav-link white-color width180">
                <i class="nav-icon fas fa-business-time"></i>
              <p>
                Privacy policy 
              </p>
            </a>
          </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>