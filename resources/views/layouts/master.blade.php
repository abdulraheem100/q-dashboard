<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>@yield('title')</title>
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('css/app.css')}}">
  <link rel="stylesheet" href="{{ asset('dist/css/adminlte.css')}}">
  <link rel="stylesheet" href="{{ asset('css/custom.css')}}">
   <!-- daterange picker -->
  <link rel="stylesheet" href={{ asset("plugins/daterangepicker/daterangepicker.css")}}>
<!--style sheet -->
  @yield('style')
  
  </head>
<body class="hold-transition sidebar-mini layout-fixed ">
<div class="wrapper">
  <!-- Navbar -->
  @include('layouts.header')
       
  <!-- Sidebar -->
  @include('layouts.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper pt-2">
    <!-- Main content -->
      <div class="container-fluid ">
        @yield('content')
      </div>
  </div>
  <!-- Footer -->
  @include('layouts.footer')
</div>


<script src="{{ asset('js/jquery.min.js')}}"></script>


<!-- AdminLTE App -->
<script src="{{ asset('js/app.js')}}"></script>
<script src="{{ asset('js/jquery.dataTables.min.js')}}"></script>
 
@yield('script')

</body>
</html>