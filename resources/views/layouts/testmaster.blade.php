<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>AdminLTE 3 | @yield('title')</title>

  {{-- <link rel="stylesheet" href="{{ asset('css/app.css')}}"> --}}
  
    <!-- Theme style -->
  {{-- <link rel="stylesheet" href="{{ asset('dist/css/adminlte.css')}}"> --}}
  <!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

  
  <!--style sheet -->
  @yield('style')
  
  </head>
<body>
        @yield('content')

<!-- jQuery -->
<script src="{{ asset('js/jquery.min.js')}}"></script>

<!-- AdminLTE App -->
<script src="{{ asset('js/app.js')}}"></script>

@yield('script')

</body>
</html>