<!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light" id="header-nav">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Navbar Search -->
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
            <div class="user-panel  pb-3  d-flex">
                {{-- <div class="image"> --}}
                  <img src="{{ asset('dist/img/user2-160x160.jpg')}}"  class="img-size-32 img-circle img-22"  alt="User Image" >
                {{-- </div> --}}
              </div>
        </a>
        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right">
          <div class="dropdown-item disabled">
            <!-- Message Start -->
            <div class="media">
              <img src="{{ asset('dist/img/user2-160x160.jpg')}}" alt="User Avatar"  class="img-size-50 mr-3 img-circle">
              <div class="media-body">
                <p class="dropdown-item-title">
                  Sami Ullah
                </p>
                <small class="text-sm">sami.ullah@techswivel.com</small>
                
              </div>
            </div></div>
          <div class="dropdown-divider"></div>
          <div class="dropdown-item" >
            <div class="container">
              <button type="button" class="btn btn-block btn-danger red-button btn-xs rounded">Log out</button>
            </div>
            </div>
        </div>
      </li>
       </ul>
  </nav>
  <!-- /.navbar -->

 