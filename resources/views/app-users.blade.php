@extends('layouts.admin-master')

@section('title')
App Users
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('css/customnotification.css')}}">
<style>
[class*="sidebar-dark"]  .user-panel{
    border-bottom: 1px solid #C83448 !important;
  }
.pink-button{
    background-color: #F4DADA;
    color:#FF0101;
}
#viewEarn{
    margin-top: 12px;
    height: 25px;
}
.text-line {
            display: flex;
            flex-direction: row;
}
.text-line:after {
            content: "";
            flex: 1 1;
            border-bottom: 1px solid #8A8C8E;
            margin: auto;
}
.sidenav a{
    font-size:14px;
    padding-top:0px; 
}
</style>
<style>
.dataTables_paginate {
    margin-right: 7px !important;
    margin-bottom: 7px !important;
}
</style>
@endsection

@section('content')
<div class="row  pt-1 pb-3">
    <div class="col-lg-6">
        <span class="withdrawal-text">App Users</span>
    </div>  
    <div class="col-lg-6">
        <input type= "search" class="form-control ml-3 mr-2 float-right" id="filterSearch"  placeholder="Search">
    </div>
</div>   

{{-- App User Table --}}
<table id="example" class=" display " >
    <thead>
        <tr>
            <th># No.</th>
            <th>User Name</th>
            <th>Email</th>
            <th>Phone #</th>
            <th>Social Login</th>
            <th>Join Date</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr >
            <td class="p-2">00001</td>
            <td class="p-2">
              <div class="d-flex flex-row">
                  <div id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0"></div>
                  <div class="ml-2 padding-top5">Anna Nestron</div>
              </div>
            </td>
            <td class="p-2">samiiiolle@gmail.com</td>
            <td class="p-2 ">+923314162949</td>
            <td class="p-2">Apple</td>
            <td class="p-2">15 Sep 2021</td>
            <td class="p-2 float-right d-flex justify-content-start" >
                <button class="btn text-left btn-xs pink-button border-radius8 s-button" style="margin-right: 7px"  data-toggle="modal" data-target="#blockUser"><i class="fas fa-ban" style="margin-left: 1px;margin-top:1px"></i></button>
                <button type="button" style="cursor:pointer" onclick="openNav()" id="viewEarn" data-toggle="modal" data-target="#" class="btn ml-0 p-1 pl-2 pr-2 red-button btn-xs border-radius8 font-size10"><i class="bi bi-folder-fill "></i> View</button>
            </td>
        </tr>
        </tbody>
</table>


{{-- Block User Modal --}}
<div class="modal fade" id="blockUser">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle">
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-3 pb-2" >
                <p class="modal-title notification-detail-text"><b>Confirmation</b></p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col notification-detail-textt">
                        <small>Are You Sure You Want To Block this User?</small>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between ">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">No</button>
                <form>
                    <input type="submit" class="btn red-button btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8" value="Yes">
                </form>
            </div>
        </div>
    </div>
</div>


<div id="mySidenav" class="sidenav shadow">
  <div class="row m-0 mb-1 mt-1">
    <div class="col ">
        <small class="float-left ml-0 pt-1"  style="font-weight:500">User Detail</small> 
    </div>
    <ul class="products-list product-list-in-card pl-2 pr-1">
        <li class="item">
            <div class="product-img pt-1">
                <img src="{{asset('image/blog/blog3.jpg') }}" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
            </div>
            <div class="product-info ml-5">
                <small  class="product-title font-szie10">Khawar Hussain</small>
                <small class="product-description font-szie10 light-grey-text">khawar.hussain@gmail.com</small>
            </div>
        </li>
    </ul>
    <div class="row ">
        <ul class="nav nav-pills ml-auto " style="margin-left: -12px !important;margin-bottom: -1px !important;font-size: 11px;">
            <li class="nav-item"><span class="nav-link active" href="#tab_1" data-toggle="tab" style="">General Info</span></li>
            <li class="nav-item"><span class="nav-link" href="#tab_2" data-toggle="tab">Buying History</span></li>
        </ul>
    </div>
    <div class="tab-content">
        <div class="tab-pane active p-2 pt-3" id="tab_1">
            <div class="row">
                <div class="col">
                    <small class="font-szie10 light-grey-text"><i class="bi bi-calendar-event-fill"></i> Date of Birth</small>
                </div>
                <div class="col">
                    <small class="font-szie10 float-right pt-1" style="font-weight: 500">03-01-1995</small>
                </div>    
            </div>
            <div class="row pt-1">
                <div class="col">
                    <small class="font-szie10 light-grey-text"><i class="bi bi-telephone-fill"></i> Phone No.</small>
                </div>
                <div class="col">
                    <small class="font-szie10 float-right pt-1" style="font-weight: 500">0341985311</small>
                </div>    
            </div>
            <div class="row pt-1">
                <div class="col">
                    <small class=" light-grey-text"><i class="fas fa-mars"></i> Gender</small>
                </div>
                <div class="col">
                    <small class="font-szie10 float-right pt-1" style="font-weight: 500">Male</small>
                </div>    
            </div>   
            <small class="font-szie10 text-line pt-1" style="font-weight: 500">Address</small>    
            <small class="font-szie10 light-grey-text"> 888 B Faisal Town Lahore, Punjab Pakistan </small>
            <small class="font-szie10 text-line pt-3" style="font-weight: 500">User Type</small>    
            <div  class="card m-0 mt-2 pl-0 border-radius8 off-white " style="width: 100%">
                <div class="card-body m-2 ml-0 p-0 border-radius8">
                    <div class="row">
                        <div class="col-lg-12">
                            <small class="font-szie10 pt-1 light-grey-text" style="font-weight: 500">Current plan</small>
                            <div class="d-flex align-items-start">
                                <small class="float-left ml-0 pt-1"  style="font-weight:500">Standard Plan</small>
                            </div>
                            <div class="d-flex align-items-end">
                                <p class="pb-0 mb-0 font-szie10 light-grey-text">$5:00/Month</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="m-0 mt-3">
                <button type="button" class="btn btn-block  red-button btn-sm border-radius8"><small>View Full Detail</small></button>
            </div>
        </div>
        <div class="tab-pane mt-2" id="tab_2">
            <div class="row">
                <div class="col">
                    <small class="font-szie10 light-grey-text">Last 30 Days</small>
                </div>
                <div class="col">
                    <a href="{{ route('admin.detail')}}"><small class="font-szie10 float-right pt-1" style="color:#ba011a">View All</small></a>
                </div>    
            </div>
            <ul class="products-list product-list-in-card ">
                <li class="item" style="border-bottom: none">
                    <div class="product-img pt-1">
                        <img src="{{asset('image/blog/blog3.jpg') }}" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
                    </div>
                    <div class="product-info ml-5">
                        <small  class="product-title font-szie10 ">Life on Mars<span class="float-right pt-2 pr-1">$2</span></small>
                        <small class="product-description font-szie10 light-grey-text">3:56</small>
                    </div>
                </li>
                <li class="item" style="border-bottom: none">
                    <div class="product-img pt-1">
                        <img src="{{asset('image/blog/blog3.jpg') }}" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
                    </div>
                    <div class="product-info ml-5">
                        <small  class="product-title font-szie10 ">Life on Mars<span class="float-right pt-2 pr-1">$2</span></small>
                        <small class="product-description font-szie10 light-grey-text">3:56</small>
                    </div>
                </li>
                <li class="item" style="border-bottom: none">
                    <div class="product-img pt-1">
                        <img src="{{asset('image/blog/blog3.jpg') }}" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
                    </div>
                    <div class="product-info ml-5">
                        <small  class="product-title font-szie10 ">Life on Mars<span class="float-right pt-2 pr-1">$5</span></small>
                        <small class="product-description font-szie10 light-grey-text">3:56</small>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    
  </div>
</div>    

<div id="mySideClose" class="shadow">
  <div class="row m-0 ">
    <div class="col " style="padding-left:7px">
         <a href="javascript:void(0)"  onclick="closeNav()"><i class="bi bi-x"></i></a>
    </div>
 </div>
</div> 


@endsection


@section('script')
 <script>
    // Disable form submissions if there are invalid fields
    (function() {
      'use strict';
      window.addEventListener('load', function() {
        // Get the forms we want to add validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
          form.addEventListener('submit', function(event) {
            if (form.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
            }
            form.classList.add('was-validated');
          }, false);
        });
      }, false);
    })();
    </script>
    <script>
    $(document).ready(function() {
        table = $('#example').DataTable({
            dom:"<'row'<'col'<'card m-2 border-radius8lr'<'.card-header p-2 pl-3'<'allnoti'>il><'card-body p-0 table-responsive'<rt>>>p>>",
            "initComplete": function(settings, json) {
                $('.dataTables_info').addClass('dataTables_info');
                $('.dataTables_info').addClass('dataTables_info');
        
       }
        });
         $("div.allnoti").html('<span class="pr-2 all-noti-text"style="">All Notifications</span>');
     $('#filterSearch').keyup(function(){
       table.search(this.value).draw();
       });
         
    });
    function getStatusValue(){
        var conceptName = $('#status').find(":selected").text();
        if(conceptName == 'All')
        {
            table.search('').draw();
             
        }else{
            table.search(conceptName).draw();
        }
        
    } 
    </script>
    <script>
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
  document.getElementById("mySideClose").style.width = "37px";
}


function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("mySideClose").style.width = "0";
}
</script>

@endsection
