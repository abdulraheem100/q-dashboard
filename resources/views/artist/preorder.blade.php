<div class="ml-3 mr-3">
    <ul class="nav nav-pills-preorder ml-auto ">
        <li class="nav-item"><button type="button"  href="#tab_11" data-toggle="tab"  class="nav-link-preorder active btn ml-2 p-1 pl-4 pr-4 red-button btn-xs border-radius8" >Songs</button></li>
        <li class="nav-item"><button type="button"  href="#tab_22"  data-toggle="tab"  class="nav-link-preorder btn ml-2 p-1 pl-4 pr-4 red-button btn-xs border-radius8"> Albums</button></li>
    </ul>

    <div class="tab-content mt-4 ml-2 mr-3">
        <div class="tab-pane active" id="tab_11">
            <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card border-radius8" style="background-color: #F9F9F9">
                        <div class="card-header border-radius-top p-2 pl-2 " >
                            <div class="row pb-1">
                                <div class="col-3">
                                <div>
                                    <img src="{{asset('image/blog/blog3.jpg') }}" style="" class="border-radius5 song-image" alt="thumbnail" width="45px" height="45px">
                                </div>
                                </div>
                                <div class="col-6 ">
                                    <div class="p-1 pl-0 " >
                                        <small class="d-flex align-items-start" style="font-weight: 500">Life on Mars</small>
                                        <small class="d-flex align-items-end " style="font-weight: 500">25 Sep 2021</small>
                                    </div>
                                </div>
                                <div class="col-3">
                                <div class="float-right">
                                    <button type="button"  class="btn grey-button  btn-xs " id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border-radius: 5px" ><i class="bi bi-three-dots"></i></button>
                                    <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                    <div class="dropdown-item m-0" >
                                        <div class="container">
                                            <a href="#" class="text-dark" data-toggle="modal" data-target="#editSongPreorder"><small>Edit Pre Order</small></a>
                                        </div>
                                    </div>
                                    <div class="dropdown-divider m-0"></div>
                                    <div class="dropdown-item" >
                                        <div class="container">
                                            <a href="#" class="text-dark" data-toggle="modal" data-target="#deletePreorder"><small>Delete Pre Order</small></a>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                </div>
                            </div>
                            <div class="row mt-4">
                                <div class="col-6">
                                    <small style="font-weight: 500">Preorders: 34/100</small>
                                </div>
                                <div class="col-6">
                                    <button type="button" data-toggle="modal" data-target="#preorderSongDetail"  class="float-right btn ml-2 p-1 pl-4 pr-4 red-button btn-xs border-radius8" ><small>View Detail</small></button>
                                </div>
                            </div>
                                
                        </div>
                        <div class="card-body p-2 border-radius-bootom" style="text-align:center;background-color:#212529">
                            <small style="font-size:14px;font-weight: 500;color:white">02 : 12 : 56 : 48</small>
                        </div> 
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card border-radius8" style="background-color: #F9F9F9">
                        <div class="card-header border-radius-top p-2 pl-2 " >
                            <div class="row pb-1">
                                <div class="col-3">
                                <div>
                                    <img src="{{asset('image/blog/blog3.jpg') }}" style="" class="border-radius5 song-image" alt="thumbnail" width="45px" height="45px">
                                </div>
                                </div>
                                <div class="col-6  ">
                                    <div class="p-1 pl-0 " >
                                        <small class="d-flex align-items-start" style="font-weight: 500">Life on Mars</small>
                                        <small class="d-flex align-items-end " style="font-weight: 500">25 Sep 2021</small>
                                    </div>
                                </div>
                                <div class="col-3">
                                <div class="float-right">
                                    <button type="button"  class="btn grey-button  btn-xs " id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border-radius: 5px" ><i class="bi bi-three-dots"></i></button>
                                    <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                    <div class="dropdown-item m-0" >
                                        <div class="container">
                                            <a href="#" class="text-dark" data-toggle="modal" data-target="#editSongPreorder"><small>Edit Pre Order</small></a>
                                        </div>
                                    </div>
                                    <div class="dropdown-divider m-0"></div>
                                    <div class="dropdown-item" >
                                        <div class="container">
                                            <a href="#" class="text-dark" data-toggle="modal" data-target="#deletePreorder"><small>Delete Pre Order</small></a>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                </div>
                            </div>
                            <div class="row mt-4">
                                <div class="col-6">
                                    <small style="font-weight: 500">Preorders: 34/100</small>
                                </div>
                                <div class="col-6">
                                    <button type="button" data-toggle="modal" data-target="#preorderSongDetail"  class="float-right btn ml-2 p-1 pl-4 pr-4 red-button btn-xs border-radius8" ><small>View Detail</small></button>
                                </div>
                            </div>
                                
                        </div>
                        <div class="card-body p-2 border-radius-bootom" style="text-align:center;background-color:#212529">
                            <small style="font-size:14px;font-weight: 500;color:white">02 : 12 : 56 : 48</small>
                        </div> 
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card border-radius8" style="background-color: #F9F9F9">
                        <div class="card-header border-radius-top p-2 pl-2 " >
                            <div class="row pb-1">
                                <div class="col-3" >
                                <div>
                                    <img src="{{asset('image/blog/blog3.jpg') }}" style="" class="border-radius5 song-image" alt="thumbnail" width="45px" height="45px">
                                </div>
                                </div>
                                <div class="col-6 ">
                                    <div class="p-1 pl-0 " >
                                        <small class="d-flex align-items-start" style="font-weight: 500">Life on Mars</small>
                                        <small class="d-flex align-items-end " style="font-weight: 500">25 Sep 2021</small>
                                    </div>
                                </div>
                                <div class="col-3">
                                <div class="float-right">
                                    <button type="button"  class="btn grey-button  btn-xs " id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border-radius: 5px" ><i class="bi bi-three-dots"></i></button>
                                    <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                    <div class="dropdown-item m-0" >
                                        <div class="container">
                                            <a href="#" class="text-dark" data-toggle="modal" data-target="#editSongPreorder"><small>Edit Pre Order</small></a>
                                        </div>
                                    </div>
                                    <div class="dropdown-divider m-0"></div>
                                    <div class="dropdown-item" >
                                        <div class="container">
                                        <a href="#" class="text-dark" data-toggle="modal" data-target="#deletePreorder"><small>Delete Pre Order</small></a>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                </div>
                            </div>
                            <div class="row mt-4">
                                <div class="col-6">
                                    <small style="font-weight: 500">Preorders: 34/100</small>
                                </div>
                                <div class="col-6">
                                    <button type="button" data-toggle="modal" data-target="#preorderSongDetail"  class="float-right btn ml-2 p-1 pl-4 pr-4 red-button btn-xs border-radius8" ><small>View Detail</small></button>
                                </div>
                            </div>
                                
                        </div>
                        <div class="card-body p-2 border-radius-bootom" style="text-align:center;background-color:#212529">
                            <small style="font-size:14px;font-weight: 500;color:white">02 : 12 : 56 : 48</small>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="row mb-1" style="margin-top: 168px">
                <div class="d-flex justify-content-center">
                    <button type="button" data-toggle="modal" data-target="#addSongPreorder" class="shadow btn ml-2 red-button btn-xs border-radius8 pt-2 pb-2 pl-4 pr-4"><i class="fas fa-plus"></i> Pre Order</button>
                </div>   
            </div>
            
        </div>
        <div class="tab-pane " id="tab_22">
            <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card border-radius8" style="background-color: #F9F9F9">
                        <div class="card-header border-radius-top p-2 pl-2 " >
                            <div class="row pb-1">
                                <div class="col-3">
                                <div>
                                    <img src="{{asset('image/blog/blog3.jpg') }}" style="" class="border-radius5 song-image" alt="thumbnail" width="45px" height="45px">
                                </div>
                                </div>
                                <div class="col-7 ml-0">
                                    <div class="p-1 pl-0 " >
                                        <small class="d-flex align-items-start" style="font-weight: 500">Life on Mars (4 Songs)</small>
                                        <small class="d-flex align-items-end " style="font-weight: 500">25 Sep 2021</small>
                                    </div>
                                </div>
                                <div class="col-2 ">
                                <div class="float-right">
                                    <button type="button"  class="btn grey-button  btn-xs " id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border-radius: 5px" ><i class="bi bi-three-dots"></i></button>
                                    <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                    <div class="dropdown-item m-0" >
                                        <div class="container">
                                            <a href="#" class="text-dark" data-toggle="modal" data-target="#editAlbumPreorder"><small>Edit Pre Order</small></a>
                                        </div>
                                    </div>
                                    <div class="dropdown-divider m-0"></div>
                                    <div class="dropdown-item" >
                                        <div class="container">
                                            <a href="#" class="text-dark" data-toggle="modal" data-target="#deletePreorder"><small>Delete Pre Order</small></a>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                </div>
                            </div>
                            <div class="row mt-4">
                                <div class="col-6">
                                    <small style="font-weight: 500">Preorders: 34/100</small>
                                </div>
                                <div class="col-6">
                                    <button type="button" data-toggle="modal" data-target="#preorderAlbumDetail"  class="float-right btn ml-2 p-1 pl-4 pr-4 red-button btn-xs border-radius8" ><small>View Detail</small></button>
                                </div>
                            </div>
                                
                        </div>
                        <div class="card-body p-2 border-radius-bootom" style="text-align:center;background-color:#212529">
                            <small style="font-size:14px;font-weight: 500;color:white">02 : 12 : 56 : 48</small>
                        </div> 
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card border-radius8" style="background-color: #F9F9F9">
                        <div class="card-header border-radius-top p-2 pl-2 " >
                            <div class="row pb-1">
                                <div class="col-3">
                                <div>
                                    <img src="{{asset('image/blog/blog3.jpg') }}" style="" class="border-radius5 song-image" alt="thumbnail" width="45px" height="45px">
                                </div>
                                </div>
                                <div class="col-7 ml-0">
                                    <div class="p-1 pl-0 " >
                                        <small class="d-flex align-items-start" style="font-weight: 500">Life on Mars (4 Songs)</small>
                                        <small class="d-flex align-items-end " style="font-weight: 500">25 Sep 2021</small>
                                    </div>
                                </div>
                                <div class="col-2">
                                <div class="float-right">
                                    <button type="button"  class="btn grey-button  btn-xs " id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border-radius: 5px" ><i class="bi bi-three-dots"></i></button>
                                    <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                    <div class="dropdown-item m-0" >
                                        <div class="container">
                                            <a href="#" class="text-dark" data-toggle="modal" data-target="#editAlbumPreorder"><small>Edit Pre Order</small></a>
                                        </div>
                                    </div>
                                    <div class="dropdown-divider m-0"></div>
                                    <div class="dropdown-item" >
                                        <div class="container">
                                            <a href="#" class="text-dark" data-toggle="modal" data-target="#deletePreorder"><small>Delete Pre Order</small></a>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                </div>
                            </div>
                            <div class="row mt-4">
                                <div class="col-6">
                                    <small style="font-weight: 500">Preorders: 34/100</small>
                                </div>
                                <div class="col-6">
                                    <button type="button" data-toggle="modal" data-target="#preorderAlbumDetail"  class="float-right btn ml-2 p-1 pl-4 pr-4 red-button btn-xs border-radius8" ><small>View Detail</small></button>
                                </div>
                            </div>
                                
                        </div>
                        <div class="card-body p-2 border-radius-bootom" style="text-align:center;background-color:#212529">
                            <small style="font-size:14px;font-weight: 500;color:white">02 : 12 : 56 : 48</small>
                        </div> 
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card border-radius8" style="background-color: #F9F9F9">
                        <div class="card-header border-radius-top p-2 pl-2 " >
                            <div class="row pb-1">
                                <div class="col-3" >
                                <div>
                                    <img src="{{asset('image/blog/blog3.jpg') }}" style="" class="border-radius5 song-image" alt="thumbnail" width="45px" height="45px">
                                </div>
                                </div>
                                <div class="col-7 ml-0">
                                    <div class="p-1 pl-0 " >
                                        <small class="d-flex align-items-start" style="font-weight: 500">Life on Mars (4 Songs)</small>
                                        <small class="d-flex align-items-end " style="font-weight: 500">25 Sep 2021</small>
                                    </div>
                                </div>
                                <div class="col-2">
                                <div class="float-right">
                                    <button type="button"  class="btn grey-button  btn-xs " id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border-radius: 5px" ><i class="bi bi-three-dots"></i></button>
                                    <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                    <div class="dropdown-item m-0" >
                                        <div class="container">
                                            <a href="#" class="text-dark" data-toggle="modal" data-target="#editAlbumPreorder"><small>Edit Pre Order</small></a>
                                        </div>
                                    </div>
                                    <div class="dropdown-divider m-0"></div>
                                    <div class="dropdown-item" >
                                        <div class="container">
                                            <a href="#" class="text-dark" data-toggle="modal" data-target="#deletePreorder"><small>Delete Pre Order</small></a>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                </div>
                            </div>
                            <div class="row mt-4">
                                <div class="col-6">
                                    <small style="font-weight: 500">Preorders: 34/100</small>
                                </div>
                                <div class="col-6">
                                    <button type="button" data-toggle="modal" data-target="#preorderAlbumDetail"  class="float-right btn ml-2 p-1 pl-4 pr-4 red-button btn-xs border-radius8" ><small>View Detail</small></button>
                                </div>
                            </div>
                                
                        </div>
                        <div class="card-body p-2 border-radius-bootom" style="text-align:center;background-color:#212529">
                            <small style="font-size:14px;font-weight: 500;color:white">02 : 12 : 56 : 48</small>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="row mb-1" style="margin-top: 168px">
                <div class="d-flex justify-content-center">
                    <button type="button" data-toggle="modal" data-target="#addAlbumPreorder" class="shadow btn ml-2 red-button btn-xs border-radius8 pt-2 pb-2 pl-4 pr-4"><i class="fa fa-plus"></i> Pre Order</button>
                </div>   
            </div>
            
        </div>
    </div>
</div>


{{-- Pre order song detail Modal --}}
<div class="modal fade" id="preorderSongDetail">
    <div class="modal-dialog modal-dialog-centered modal-lg card-detail">
        <div class="modal-content border-radius12">
            <div class="modal-header border-radius-top pl-3 pt-4 pb-3 pr-2" style="background-color: #242424;color:white">
                <div class="col-1">
                    <div>
                        <img src="{{asset('image/blog/blog3.jpg') }}" style="" class="border-radius5 song-image" alt="thumbnail" width="45px" height="45px">
                    </div>
                </div>
                <div class="col-8 pl-3">
                    <div class="p-1 pl-0" id="song-title-text">
                        <small class="d-flex align-items-start" style="font-weight: 500">Life on Mars <small class="ml-2 mt-1" style="font-size:9px"> (Price $2)</small></small>
                        <small class="d-flex align-items-end " style="font-size:9px;">25 Sep 2021</small>
                    </div>
                </div>
                <div class="col-3">
                    <div class="float-right">
                        <button type="button" class="close" style="color: #fff"  data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="modal-body p-0 ">
                <div class="row">
                    <div class="col-12 table-wrapper-scroll-y my-custom-scrollbar">
                        <table class="table responsive table-striped ">
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="d-flex flex-row">
                                            <div id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">
                                            </div>
                                            <div class="ml-2 padding-top5"><small>Anna Nestron</small></div>
                                        </div>
                                    </td>
                                    <td><small>samiiolle@gmail.com</small></td>
                                    <td><small>26/2/2021.5:00AM</small></td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="d-flex flex-row">
                                            <div id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">
                                            </div>
                                            <div class="ml-2 padding-top5"><small>Anna Nestron</small></div>
                                        </div>
                                    </td>
                                    <td><small>samiiolle@gmail.com</small></td>
                                    <td><small>26/2/2021.5:00AM</small></td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="d-flex flex-row">
                                            <div id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">
                                            </div>
                                            <div class="ml-2 padding-top5"><small>Anna Nestron</small></div>
                                        </div>
                                    </td>
                                    <td><small>samiiolle@gmail.com</small></td>
                                    <td><small>26/2/2021.5:00AM</small></td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="d-flex flex-row">
                                            <div id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">
                                            </div>
                                            <div class="ml-2 padding-top5"><small>Anna Nestron</small></div>
                                        </div>
                                    </td>
                                    <td><small>samiiolle@gmail.com</small></td>
                                    <td><small>26/2/2021.5:00AM</small></td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="d-flex flex-row">
                                            <div id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">
                                            </div>
                                            <div class="ml-2 padding-top5"><small>Anna Nestron</small></div>
                                        </div>
                                    </td>
                                    <td><small>samiiolle@gmail.com</small></td>
                                    <td><small>26/2/2021.5:00AM</small></td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="d-flex flex-row">
                                            <div id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">
                                            </div>
                                            <div class="ml-2 padding-top5"><small>Anna Nestron</small></div>
                                        </div>
                                    </td>
                                    <td><small>samiiolle@gmail.com</small></td>
                                    <td><small>26/2/2021.5:00AM</small></td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="d-flex flex-row">
                                            <div id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">
                                            </div>
                                            <div class="ml-2 padding-top5"><small>Anna Nestron</small></div>
                                        </div>
                                    </td>
                                    <td><small>samiiolle@gmail.com</small></td>
                                    <td><small>26/2/2021.5:00AM</small></td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="d-flex flex-row">
                                            <div id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">
                                            </div>
                                            <div class="ml-2 padding-top5"><small>Anna Nestron</small></div>
                                        </div>
                                    </td>
                                    <td><small>samiiolle@gmail.com</small></td>
                                    <td><small>26/2/2021.5:00AM</small></td>
                                </tr>
                            </tbody>
                        </table>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



{{-- Pre order Album detail Modal --}}
<div class="modal fade" id="preorderAlbumDetail">
    <div class="modal-dialog modal-dialog-centered modal-lg card-detail">
        <div class="modal-content border-radius12">
            <div class="modal-header border-radius-top pl-3 pt-4 pb-3 pr-2" style="background-color: #242424;color:white">
                <div class="col-1">
                    <div>
                        <img src="{{asset('image/blog/blog3.jpg') }}" style="" class="border-radius5 song-image" alt="thumbnail" width="45px" height="45px">
                    </div>
                </div>
                <div class="col-8 pl-3">
                    <div class="p-1 pl-0" id="song-title-text">
                        <small class="d-flex align-items-start" style="font-weight: 500">Life on Mars <small class="ml-2 mt-1" style="font-size:9px"> (Price $2)</small></small>
                        <small class="d-flex align-items-end " style="font-size:9px;">25 Sep 2021</small>
                    </div>
                </div>
                <div class="col-3">
                    <div class="float-right">
                        <button type="button" class="close" style="color: #fff"  data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="modal-body p-0 ">
                <div class="row m-0">
                    <ul class="nav nav-pills ml-auto " style="margin-bottom: 0px !important;font-size: 11px;">
                        <li class="nav-item"><span class="nav-link active" href="#tab_21" data-toggle="tab" style="">All Songs</span></li>
                        <li class="nav-item"><span class="nav-link " href="#tab_24" data-toggle="tab" style="">Orders</span></li>
                    </ul>
                </div>
                <div class="card-body pt-2 pl-0 pr-0">
                    <div class="tab-content">
                        <div class="tab-pane  active" id="tab_21">
                            <table class="table responsive ">
                            <tbody>
                                <tr>
                                    <td class="p-0 pl-2">
                                        <ul class="products-list product-list-in-card pl-2 pr-2">
                                            <li class="item">
                                                <div class="product-img pt-1">
                                                    <img src="{{asset('image/blog/blog3.jpg') }}" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
                                                </div>
                                                <div class="product-info ml-5">
                                                    <small  class="product-title font-szie10">Life on Mars <span class="light-grey-text"> (3:56)</span></small>
                                                <small class="product-description font-szie10 light-grey-text">Free song</small>
                                                </div>
                                            </li>
                                        </ul>
                                    </td>
                                    <td>
                                        <span class="p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Audio</span>
                                        <span class="p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Video</span>
                                    </td>
                                    <td></td>
                                    <td class="pb-2" > 
                                       <button class="btn text-left btn-xs orange-button border-radius8 s-button" data-toggle="modal" data-target="#deleteSongAlbum"><span class="bi bi-trash3"  id="trash-icon"></span></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="p-0 pl-2">
                                        <ul class="products-list product-list-in-card pl-2 pr-2">
                                            <li class="item">
                                                <div class="product-img pt-1">
                                                    <img src="{{asset('image/blog/blog3.jpg') }}" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
                                                </div>
                                                <div class="product-info ml-5">
                                                    <small  class="product-title font-szie10">Life on Mars <span class="light-grey-text"> (3:56)</span></small>
                                                <small class="product-description font-szie10 light-grey-text">Free song</small>
                                                </div>
                                            </li>
                                        </ul>
                                    </td>
                                    <td>
                                        <span class="p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Audio</span>
                                        <span class="p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Video</span>
                                    </td>
                                    <td></td>
                                    <td class="pb-2" > 
                                       <button class="btn text-left btn-xs orange-button border-radius8 s-button" data-toggle="modal" data-target="#deleteSongAlbum"><span class="bi bi-trash3"  id="trash-icon"></span></button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                        <div class="tab-pane " id="tab_24">
                            <div class="row">
                    <div class="col-12 table-wrapper-scroll-y my-custom-scrollbar">
                        <table class="table responsive table-striped ">
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="d-flex flex-row">
                                            <div id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">
                                            </div>
                                            <div class="ml-2 padding-top5"><small>Anna Nestron</small></div>
                                        </div>
                                    </td>
                                    <td><small>samiiolle@gmail.com</small></td>
                                    <td><small>26/2/2021.5:00AM</small></td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="d-flex flex-row">
                                            <div id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">
                                            </div>
                                            <div class="ml-2 padding-top5"><small>Anna Nestron</small></div>
                                        </div>
                                    </td>
                                    <td><small>samiiolle@gmail.com</small></td>
                                    <td><small>26/2/2021.5:00AM</small></td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="d-flex flex-row">
                                            <div id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">
                                            </div>
                                            <div class="ml-2 padding-top5"><small>Anna Nestron</small></div>
                                        </div>
                                    </td>
                                    <td><small>samiiolle@gmail.com</small></td>
                                    <td><small>26/2/2021.5:00AM</small></td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="d-flex flex-row">
                                            <div id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">
                                            </div>
                                            <div class="ml-2 padding-top5"><small>Anna Nestron</small></div>
                                        </div>
                                    </td>
                                    <td><small>samiiolle@gmail.com</small></td>
                                    <td><small>26/2/2021.5:00AM</small></td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="d-flex flex-row">
                                            <div id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">
                                            </div>
                                            <div class="ml-2 padding-top5"><small>Anna Nestron</small></div>
                                        </div>
                                    </td>
                                    <td><small>samiiolle@gmail.com</small></td>
                                    <td><small>26/2/2021.5:00AM</small></td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="d-flex flex-row">
                                            <div id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">
                                            </div>
                                            <div class="ml-2 padding-top5"><small>Anna Nestron</small></div>
                                        </div>
                                    </td>
                                    <td><small>samiiolle@gmail.com</small></td>
                                    <td><small>26/2/2021.5:00AM</small></td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="d-flex flex-row">
                                            <div id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">
                                            </div>
                                            <div class="ml-2 padding-top5"><small>Anna Nestron</small></div>
                                        </div>
                                    </td>
                                    <td><small>samiiolle@gmail.com</small></td>
                                    <td><small>26/2/2021.5:00AM</small></td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="d-flex flex-row">
                                            <div id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">
                                            </div>
                                            <div class="ml-2 padding-top5"><small>Anna Nestron</small></div>
                                        </div>
                                    </td>
                                    <td><small>samiiolle@gmail.com</small></td>
                                    <td><small>26/2/2021.5:00AM</small></td>
                                </tr>
                            </tbody>
                        </table>  
                    </div>
                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



{{-- Add Song pre Order Modal --}}
<div class="modal fade" id="addSongPreorder">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle" >
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-3 pb-2 pr-2" >
                <p class="modal-title add-bank-text" ><b class="font-size12">Create Pre Order</b></p>
                <button type="button" class="close"  data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="addSongPreorderForm" class="needs-validation" novalidate>
            <div class="modal-body">
                <div class="row">
                   
                    <div class="col-12">
                        <div class="mb-2 mt-0 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Order Type ?</span></label>
                            <select id="orderTypeSong" name="orderTypeSong" class="form-control border-radius8 add-song-input-text"  onchange="setStatusValue();">
                                <option class="add-song-input-text">Song</option>
                                <option class="add-song-input-text">Album</option>
                            </select>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-0 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Title</span></label>
                            <input type="text" class="form-control input-sm border-radius8 add-song-input-text"   placeholder="Enter song title" style="font-size: 12.5px" id="orderSongName" name="orderSongName" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div id="price-div" class="mb-2 mt-1 input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Price</span></label>
                            <input type="text" class="form-control input-sm border-radius8 add-song-input-text"   placeholder="Enter price" style="font-size: 12.5px" id="orderSongPrice" name="orderSongPrice" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Category</span></label>
                            <select class="form-control font-size12 border-radius8 add-song-input-text" id="orderSongCategory" name="orderSongCategory">
                                <option>Select Category</option>
                            </select>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Language</span></label>
                            <select class="form-control font-size12 border-radius8 add-song-input-text" id="orderSongLanguage" name="orderSongLanguage">
                                <option>Select Language</option>
                            </select>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Tumbnail <span class="add-song-span-text ">( Max 1MB )</span></span></label>
                            <input type="file" class="form-control input-sm border-radius8 "   placeholder="Choose Tumbnail (Max 1MB)" style="font-size: 12.5px" id="orderSongThumbnail" name="orderSongThumbnail" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Audio File <span class="add-song-span-text "> ( Max 5MB )</span></span></label>
                            <input type="file" class="form-control input-sm border-radius8"   placeholder="Choose file" style="font-size: 12.5px" id="orderAudioFile" name="orderAudioFile" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Video File <span class="add-song-span-text "> ( Max 5MB Only Mp4 ) ( Optional )</span></span></label>
                            <input type="file" class="form-control input-sm border-radius8"   placeholder="Choose file" style="font-size: 12.5px" id="orderVideoFile" name="orderVideoFile" >
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-3 mt-2 mb-0 input-group-sm input_field " >
                            <label class="m-0  mb-1"> <span class="text-dark add-song-label-text"  ><b>Lyrics <span class="add-song-span-text "> ( Optional )</span></b></span></label>
                            <textarea rows="3" cols="50" name="OrderSongLyrics"  id="orderSongLyrics"   class="form-control add-song-input-text input-xs rounded border-radius8"  placeholder="Enter Lyrics.."  required></textarea>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-3 mt-0 mb-0 input-group-sm input_field " >
                            <label class="m-0  mb-1"> <span class="text-dark add-song-label-text"  ><b>Date:</b></span></label>
                            <div class="input-group date" id="reservationdate" data-target-input="nearest">
                                <input type="text" class="form-control datetimepicker-input" placeholder="Select publish date" style="font-size: 12.5px;border-right: none;height:31px; " data-target="#reservationdate" id="orderSongDate" name="orderSongDate" required/>
                                <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                    <div class="input-group-text red-text" style="background-color: white;border-left:none;height:31px; "><i class="bi bi-calendar3"></i></div>
                                </div>
                            </div>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>

                        <div class="mb-3 mt-0 mb-0 input-group-sm input_field " >
                            <label class="m-0  mb-1"> <span class="text-dark add-song-label-text"  ><b>Time:</b></span></label>
                            <div class="input-group date" id="timepicker" data-target-input="nearest">
                                <input type="text" class="form-control datetimepicker-input" placeholder="Select publish time" style="font-size: 12.5px;border-right: none;height:31px; " data-target="#timepicker" id="orderSongTime" name="orderSongTime" required/>
                                <div class="input-group-append" data-target="#timepicker" data-toggle="datetimepicker">
                                    <div class="input-group-text red-text" style="background-color: white;border-left:none;height:31px; "><i class="far fa-clock"></i></div>
                                </div>
                            </div>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer p-2 justify-content-between " style="margin-left: 4px">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">Canel</button>
                <input type="submit" class="btn red-button btn-xs mr-2 pt-1 pb-1 pl-3 pr-3 border-radius8 account-Create-button" value="Create">
            </div>
            </form>
        </div>
    </div>
</div>


{{-- Edit Song pre Order Modal --}}
<div class="modal fade" id="editSongPreorder">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle" >
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-3 pb-2 pr-2" >
                <p class="modal-title add-bank-text" ><b class="font-size12">Edit Pre Order</b></p>
                <button type="button" class="close"  data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="editSongPreorderForm" class="needs-validation" novalidate>
            <div class="modal-body">
                <div class="row">
                   
                    <div class="col-12">
                        <div class="mb-2 mt-0 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Order Type ?</span></label>
                            <select id="editOrderTypeSong" name="edtiOrderTypeSong" class="form-control border-radius8 add-song-input-text"  onchange="setStatusValue();">
                                <option class="add-song-input-text" selected>Song</option>
                                <option class="add-song-input-text">Album</option>
                            </select>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-0 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Title</span></label>
                            <input type="text" class="form-control input-sm border-radius8 add-song-input-text"   placeholder="Enter song title" style="font-size: 12.5px" id="editOrderSongName" name="editOrderSongName" value="Life is song" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div id="price-div" class="mb-2 mt-1 input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Price</span></label>
                            <input type="text" class="form-control input-sm border-radius8 add-song-input-text"   placeholder="Enter price" style="font-size: 12.5px" id="editOrderSongPrice" name="editOrderSongPrice" value="$4.0" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Category</span></label>
                            <select class="form-control font-size12 border-radius8 add-song-input-text" id="editOrderSongCategory" name="editOrderSongCategory">
                                <option>Select Category</option>
                            </select>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Language</span></label>
                            <select class="form-control font-size12 border-radius8 add-song-input-text" id="editOrderSongLanguage" name="editOrderSongLanguage">
                                <option>Select Language</option>
                            </select>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Tumbnail <span class="add-song-span-text ">( Max 1MB )</span></span></label>
                            <input type="file" class="form-control input-sm border-radius8 "   placeholder="Choose Tumbnail (Max 1MB)" style="font-size: 12.5px" id="editOrderSongThumbnail" name="editOrderSongThumbnail" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Audio File <span class="add-song-span-text "> ( Max 5MB )</span></span></label>
                            <input type="file" class="form-control input-sm border-radius8"   placeholder="Choose file" style="font-size: 12.5px" id="editOrderAudioFile" name="editOrderAudioFile" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Video File <span class="add-song-span-text "> ( Max 5MB Only Mp4 ) ( Optional )</span></span></label>
                            <input type="file" class="form-control input-sm border-radius8"   placeholder="Choose file" style="font-size: 12.5px" id="editOrderVideoFile" name="editOrderVideoFile" >
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-3 mt-2 mb-0 input-group-sm input_field " >
                            <label class="m-0  mb-1"> <span class="text-dark add-song-label-text"  ><b>Lyrics <span class="add-song-span-text "> ( Optional )</span></b></span></label>
                            <textarea rows="3" cols="50" name="OrderSongLyrics"  id="editOrderSongLyrics"   class="form-control add-song-input-text input-xs rounded border-radius8"  placeholder="Enter Lyrics.."  required></textarea>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-3 mt-0 mb-0 input-group-sm input_field " >
                            <label class="m-0  mb-1"> <span class="text-dark add-song-label-text"  ><b>Date:</b></span></label>
                            <div class="input-group date" id="reservationdate" data-target-input="nearest">
                                <input type="text" class="form-control datetimepicker-input " placeholder="Select publish date" style="font-size: 12.5px;border-right: none;height:31px; " data-target="#reservationdate" id="eidtOrderSongDate" name="eidtOrderSongDate" required/>
                                <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                    <div class="input-group-text red-text" style="background-color: white;border-left:none;height:31px;"><i class="bi bi-calendar3"></i></div>
                                </div>
                            </div>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>

                        <div class="mb-3 mt-0 mb-0 input-group-sm input_field " >
                            <label class="m-0  mb-1"> <span class="text-dark add-song-label-text"  ><b>Time:</b></span></label>
                            <div class="input-group date" id="timepicker" data-target-input="nearest">
                                <input type="text" class="form-control datetimepicker-input rounded" placeholder="Select publish time" style="font-size: 12.5px;border-right: none;height:31px; " data-target="#timepicker" id="eidtOrderSongTime" name="eidtOrderSongTime" required/>
                                <div class="input-group-append" data-target="#timepicker" data-toggle="datetimepicker">
                                    <div class="input-group-text red-text" style="background-color: white;border-left:none;height:31px;"><i class="far fa-clock"></i></div>
                                </div>
                            </div>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer p-2 justify-content-between " style="margin-left: 4px">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">Canel</button>
                <input type="submit" class="btn red-button btn-xs mr-2 pt-1 pb-1 pl-3 pr-3 border-radius8 account-Create-button" value="Update">
            </div>
            </form>
        </div>
    </div>
</div>






{{-- Add Album pre Order Modal --}}
<div class="modal fade" id="addAlbumPreorder">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle" >
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-2 pb-2 pr-2" >
                <p class="modal-title add-bank-text" ><b class="font-size12">Create Pre Order</b></p>
                <button type="button" class="close"  data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="addSongPreorderForm" class="needs-validation" novalidate>
            <div class="modal-body">
                <div class="row">
                   
                    <div class="col-12">
                        <div class="mb-2 mt-0 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Order Type ?</span></label>
                            <select id="orderTypeAlbum" name="orderTypeAlbum" class="form-control border-radius8 add-song-input-text"  onchange="setStatusValue();">
                                <option class="add-song-input-text">Song</option>
                                <option class="add-song-input-text" selected>Album</option>
                            </select>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-0 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Title</span></label>
                            <input type="text" class="form-control input-sm border-radius8 add-song-input-text"   placeholder="Enter Album title" style="font-size: 12.5px" id="orderAlbumName" name="orderAlbumName" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-0"> <span class="text-dark add-song-label-text" >Tumbnail </span></label><br>
                            <input type="file" style="font-size: 12.5px;margin: .4rem 0;color:gray"      id="orderAlbumThumbnail" name="orderAlbumThumbnail" required><br>
                            <span class="add-song-span-text "> Max. size 1MB </span>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-1 mt-0 mb-0 input-group-sm input_field " >
                            <label class="m-0  mb-1"> <span class="text-dark add-song-label-text"  ><b>Date:</b></span></label>
                            <div class="input-group date" id="reservationdate" data-target-input="nearest">
                                <input type="text" class="form-control input-sm border-left datetimepicker-input" placeholder="Select publish date" style="font-size: 12.5px;border-right: none;height:31px; " data-target="#reservationdate" id="orderAlbumDate" name="orderAlbumDate" required/>
                                <div class="input-group-append border-right" data-target="#reservationdate" data-toggle="datetimepicker">
                                    <div class="input-group-text red-text border-right" style="background-color: white;border-left:none; height:31px;"><i class="bi bi-calendar3"></i></div>
                                </div>
                            </div>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-1 mt-0 mb-0 input-group-sm input_field " >
                            <label class="m-0  mb-1"> <span class="text-dark add-song-label-text"  ><b>Time:</b></span></label>
                            <div class="input-group date" id="timepicker" data-target-input="nearest">
                                <input type="time" class="form-control input-sm border-radius8 datetimepicker-input" placeholder="Select publish time" style="font-size: 12.5px;height:31px;" data-target="#timepicker" id="orderAlbumTime" name="orderAlbumTime" required/>
                            </div>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-3 mt-3 mb-0 input-group-sm input_field " >
                            <button type="button" data-toggle="modal" data-target="#addSongPreorder"  class=" btn ml-0 pink-button btn-xs border-radius8 pt-1 pb-1 pl-2 pr-2"><i class="fas fa-plus"></i> Add Songs Detail</button>
                        </div>
                        <div class="row" >
                            <div class="col-2">
                                    <img src="{{asset('image/blog/blog3.jpg') }}" style="" class="border-radius5 song-image" alt="thumbnail" width="45px" height="45px">
                            </div>
                            <div class="col-7 p-2">
                                <small>Life on Mars</small>
                            </div>
                            <div class="col 2 p-2 pr-3">
                                <i class="float-right red-text bi bi-pencil-fill"></i>
                            </div>
                            <div class="col-1 p-2 ">
                                <i class="float-right bi bi-x-lg"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer p-2 justify-content-between " style="margin-left: 4px">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">Canel</button>
                <input type="submit" class="btn red-button btn-xs mr-2 pt-1 pb-1 pl-3 pr-3 border-radius8 account-Create-button" value="Create">
            </div>
            </form>
        </div>
    </div>
</div>




{{-- Edit Album pre Order Modal --}}
<div class="modal fade" id="editAlbumPreorder">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle" >
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-2 pb-2 pr-2" >
                <p class="modal-title add-bank-text" ><b class="font-size12">Edit Pre Order</b></p>
                <button type="button" class="close"  data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="addSongPreorderForm" class="needs-validation" novalidate>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="mb-2 mt-0 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Order Type ?</span></label>
                            <select id="orderTypeAlbum" name="editOrderTypeAlbum" class="form-control border-radius8 add-song-input-text"  onchange="setStatusValue();">
                                <option class="add-song-input-text">Song</option>
                                <option class="add-song-input-text" selected>Album</option>
                            </select>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-0 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Title</span></label>
                            <input type="text" class="form-control input-sm border-radius8 add-song-input-text"   placeholder="Enter Album title" value="Life is fire" style="font-size: 12.5px" id="editOrderAlbumName" name="editOrderAlbumName" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-0"> <span class="text-dark add-song-label-text" >Tumbnail </span></label><br>
                            <input type="file" style="font-size: 12.5px;margin: .4rem 0;color:gray"      id="editOrderAlbumThumbnail" name="editOrderAlbumThumbnail" required><br>
                            <span class="add-song-span-text "> Max. size 1MB </span>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class=" mt-0 mb-1 input-group-sm input_field " >
                            <label class="m-0  mb-1"> <span class="text-dark add-song-label-text"  ><b>Date:</b></span></label>
                            <div class="input-group date" id="reservationdate" data-target-input="nearest">
                                <input type="text" class="form-control input-sm border-left datetimepicker-input" placeholder="Select publish date" style="font-size: 12.5px;border-right: none;height:31px; " data-target="#reservationdate" id="editOrderAlbumDate" name="editOrderAlbumDate" required/>
                                <div class="input-group-append border-right" data-target="#reservationdate" data-toggle="datetimepicker">
                                    <div class="input-group-text red-text border-right" style="background-color: white;border-left:none;height:31px;"><i class="bi bi-calendar3"></i></div>
                                </div>
                            </div>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mt-0 mb-1 input-group-sm input_field " >
                            <label class="m-0  mb-1"> <span class="text-dark add-song-label-text"  ><b>Time:</b></span></label>
                            <div class="input-group date" id="timepicker" data-target-input="nearest">
                                <input type="text" class="form-control input-sm border-left datetimepicker-input" placeholder="Select publish time" style="font-size: 12.5px;border-right: none;height:31px; " data-target="#timepicker" id="editOrderAlbumTime" name="editOrderAlbumTime" required/>
                                <div class="input-group-append border-right" data-target="#timepicker" data-toggle="datetimepicker">
                                    <div class="input-group-text border-right red-text" style="background-color: white;border-left:none;height:31px;"><i class="far fa-clock"></i></div>
                                </div>
                            </div>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-3 mt-3 mb-0 input-group-sm input_field " >
                            <button type="button" data-toggle="modal" data-target="#addSongPreorder"  class=" btn ml-0 pink-button btn-xs border-radius8 pt-1 pb-1 pl-2 pr-2"><i class="fas fa-plus"></i> Add Songs Detail</button>
                        </div>
                        <div class="row" >
                            <div class="col-2">
                                    <img src="{{asset('image/blog/blog3.jpg') }}" style="" class="border-radius5 song-image" alt="thumbnail" width="45px" height="45px">
                            </div>
                            <div class="col-7 p-2">
                                <small>Life on Mars</small>
                            </div>
                            <div class="col 2 p-2 pr-3">
                                <i class="float-right red-text bi bi-pencil-fill"></i>
                            </div>
                            <div class="col-1 p-2 ">
                                <i class="float-right bi bi-x-lg"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer p-2 justify-content-between " style="margin-left: 4px">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">Canel</button>
                <input type="submit" class="btn red-button btn-xs mr-2 pt-1 pb-1 pl-3 pr-3 border-radius8 account-Create-button" value="Update">
            </div>
            </form>
        </div>
    </div>
</div>

{{-- Delete Pre order Song/Album Modal --}}
<div class="modal fade" id="deletePreorder">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle">
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-3 pb-2" >
                <p class="modal-title notification-detail-text"><b>Confirmation</b></p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col notification-detail-textt">
                        <small>Are You Sure You Want To Delete this Pre Order</small>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between ">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">No</button>
                <form>
                    <input type="submit" class="btn red-button btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8" value="Confirm">
                </form>
            </div>
        </div>
    </div>
</div>

{{-- Delete Song from album Modal --}}
<div class="modal fade" id="deleteSongAlbum">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle">
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-3 pb-2" >
                <p class="modal-title notification-detail-text"><b>Confirmation</b></p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col notification-detail-textt">
                        <small>Are You Sure You Want To Delete this Pre Order Song</small>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between ">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">No</button>
                <form>
                    <input type="submit" class="btn red-button btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8" value="Confirm">
                </form>
            </div>
        </div>
    </div>
</div>