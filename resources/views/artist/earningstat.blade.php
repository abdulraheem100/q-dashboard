 {{-- Follower Table --}}
    <table id="earningStatTable" class="table display "  style="font-size: 12px;">
        <thead class="border-none">
            <tr class="border-none">
                <th class="border-none pt-0 pl-3">#No.</th>
                <th class="border-none pt-0 pl-3">User Name</th>
                <th class="border-none pt-0 pl-3">Email</th>
                <th class="border-none pt-0 pl-3">Song/Album</th>
                <th class="border-none pt-0 pl-3">Spent Amount</th>
                <th class="border-none pt-0 pl-3">Purchased at</th>
                <th class="border-none pt-0 pl-3"></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="border-none p-3 pt-4">00001</td>
                <td class="border-none p-3">
                    <div class="d-flex flex-row">
                        <div id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">
                        </div>
                        <div class="ml-2 padding-top5">Anna Nestron</div>
                    </div>
                    </td>
                <td class="border-none p-3 pt-4">samiiolle@gmail.com</td>
                <td class="border-none p-3 pt-4">Song</td>
                <td class="border-none p-3 pt-4">$15</td>
                <td class="border-none p-3 pt-4">25/01/2021.5:00AM</td>
                <td class="border-none p-3 pt-4"><button type="button" style="cursor:pointer" onclick="openNav()" id="viewEarn" data-toggle="modal" data-target="#" class="btn ml-2 p-1 pl-2 pr-2 red-button btn-xs border-radius8 font-size10"><i class="bi bi-folder-fill "></i> View</button></td>
            </tr>
            <tr>
                <td class="border-none p-3 pt-4">00001</td>
                <td class="border-none p-3">
                    <div class="d-flex flex-row">
                        <div id="follower-image" class ="roundedImage rounded-circle mr-0 mt-0">
                        </div>
                        <div class="ml-2 padding-top5">Anna Nestron</div>
                    </div>
                    </td>
                <td class="border-none p-3 pt-4">samiiolle@gmail.com</td>
                <td class="border-none p-3 pt-4">Song</td>
                <td class="border-none p-3 pt-4">$15</td>
                <td class="border-none p-3 pt-4">25/01/2021.5:00AM</td>
                <td class="border-none p-3 pt-4"><button type="button" data-toggle="modal" data-target="#" class="btn ml-2 p-1 pl-2 pr-2 red-button btn-xs border-radius8 font-size10"><i class="bi bi-folder-fill "></i> View</button></td>
            </tr>
        </tbody>
    </table>

<div id="mySidenav" class="sidenav shadow">
    
  <div class="row m-0 mb-1 mt-1">
    <div class="col ">
        <small class="float-left ml-0 pt-1"  style="font-weight:500">User Detail</small> 
  
    </div>
    <ul class="products-list product-list-in-card pl-2 pr-1">
        <li class="item">
            <div class="product-img pt-1">
                <img src="{{asset('image/blog/blog3.jpg') }}" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
            </div>
            <div class="product-info ml-5">
                <small  class="product-title font-szie10">Khawar Hussain</small>
                <small class="product-description font-szie10 light-grey-text">khawar.hussain@gmail.com</small>
            </div>
        </li>
    </ul>
    <small class="float-left ml-0 pt-1"  style="font-weight:500">Purchased items [Preorder]</small> 
    <ul class="products-list product-list-in-card pl-2 pr-1">
        <li class="item">
            <div class="product-img pt-1">
                <img src="{{asset('image/blog/blog3.jpg') }}" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
            </div>
            <div class="product-info ml-5">
                <small  class="product-title font-szie10 ">Life on Mars<span class="float-right pr-3">$2</span></small>
                <small class="product-description font-szie10 light-grey-text">3:56</small>
            </div>
        </li>
    </ul>
    <div  class="card m-2 pl-0 mr-5 border-radius8 off-white " style="width: 94%">
        <div class="card-body m-2 ml-0 p-0 border-radius8">
            <div class="row">
                <div class="col-lg-12">
                    <div class="d-flex align-items-start">
                        <small class="float-left ml-0 pt-1"  style="font-weight:500">Address</small>
                    </div>
                    <div class="d-flex align-items-end">
                         <p class="pb-2  userAccountNumber">885B Faisal Town Lahore,Pakistan</p>
                    </div>
               </div>
            </div>
        </div>
      </div>
  </div>
</div>    

    

<div id="mySideClose" class="shadow">
  <div class="row m-0 ">
    <div class="col " style="padding-left:7px">
         <a href="javascript:void(0)"  onclick="closeNav()"><i class="bi bi-x"></i></a>
    </div>
 </div>
</div>    
