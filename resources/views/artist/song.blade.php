 {{-- Song Table --}}
                                <table id="songTable" class="table display " >
                                    <thead id="song-thead">
                                        <tr>
                                          <th></th>
                                            <th></th>
                                            <th ></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr style="background-color: white !important;">
                                            <td  class="p-0 pl-2">
                                                <ul class="products-list product-list-in-card pl-2 pr-1">
                                                    <li class="item">
                                                        <div class="product-img pt-1">
                                                            <img src="{{asset('image/blog/blog3.jpg') }}" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
                                                        </div>
                                                        <div class="product-info ml-5">
                                                            <small  class="product-title font-szie10">Life on Mars <span class="light-grey-text"> (3:56)</span>
                                                            <span class=" dot-play"><i class="bi bi-pause-fill" style="color: white;margin-left:4px !important"></i></span>
                                                            </small>
                                                            <small class="product-description font-szie10 light-grey-text">Free song</small>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </td>
                                            <td class="pl-0 pr-0"> 
                                                <span class="ml-5 p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Audio</span>
                                                <span class="p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Video</span>
                                            </td>
                                            <td ></td>
                                            <td class="pb-2" > 
                                                <button class="btn text-left btn-xs orange-button border-radius8 s-button" data-toggle="modal" data-target="#deleteSonginAlbum"><span class="bi bi-trash3"  id="trash-icon"></span></button>
                                                <button class="btn text-left btn-xs red-button border-radius8 s-button" style="margin-right: 7px"  data-toggle="modal" data-target="#editSongAlbum"><span class="bi bi-pencil-square fa-md" id="pencil-icon"></span></button>
                                            </td>
                                        </tr>
                                        <tr style="background-color: white !important;">
                                            <td  class="p-0 pl-2">
                                                <ul class="products-list product-list-in-card pl-2 pr-1">
                                                    <li class="item">
                                                        <div class="product-img pt-1">
                                                            <img src="{{asset('image/blog/blog3.jpg') }}" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
                                                        </div>
                                                        <div class="product-info ml-5">
                                                            <small  class="product-title font-szie10">Life on Mars <span class="light-grey-text"> (3:56)</span>
                                                            <span class=" dot-play"><i class="bi bi-pause-fill" style="color: white;margin-left:4px !important"></i></span>
                                                            </small>
                                                            <small class="product-description  font-szie10 light-grey-text">Free song</small>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </td>
                                            <td class="pl-0 pr-0"> 
                                                <span class="ml-5 p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Audio</span>
                                                <span class="p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Video</span>
                                            </td>
                                            <td></td>
                                            <td class="pb-2" > 
                                                <button class=" btn text-left btn-xs orange-button border-radius8 s-button" data-toggle="modal" data-target="#deleteSonginAlbum"><span class="bi bi-trash3"  id="trash-icon"></span></button>
                                                <button class="btn text-left btn-xs red-button border-radius8 s-button" style="margin-right: 7px"  data-toggle="modal" data-target="#editSongAlbum"><span class="bi bi-pencil-square fa-md" id="pencil-icon"></span></button>
                                            </td>
                                        </tr>
                                        {{-- <tr style="background-color: white !important;">
                                            <td  class="p-2 d-flex  justify-content-start" style="width: 170px">
                                                <img src="{{asset('image/blog/blog3.jpg') }}" style="" class="border-radius5 song-image" alt="thumbnail" width="37.5px" height="37.5px">
                                                <div class="ml-2 song-info" style="display: inline-grid;">
                                                    <small class="d-flex align-items-start font-size10">Life on Mars <span class="ml-1 light-grey-text font-size10 ">(3:56)</span></small>
                                                    <small class="d-flex align-items-end light-grey-text font-size10 ">Free Song</small>
                                                </div>
                                                <div class="ml-2" style="display: inline-grid;">
                                                    <small class="d-flex align-items-start"> </small>
                                                    <small class="d-flex align-items-center dot-play"><i class="bi bi-pause-fill" style="color: white;margin-left:4px !important"></i></small>
                                                    <small class="d-flex align-items-end "></small>
                                                </div>
                                                <div class="ml-2" style="display: inline-grid;">
                                                    <small class="d-flex align-items-start"> </small>
                                                    <small class="d-flex align-items-center dot-play"><i class="bi bi-play-fill" style="color: white;margin-left:4px !important"></i></small>
                                                    <small class="d-flex align-items-end "></small>
                                                </div>
                                            </td>
                                            <td  class="p-3 ">
                                                <span class="p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Audio</span>
                                                <span class="p-1 pl-2 pr-2  text-Audio border-radius8 font-size12">Video</span>
                                            </td>
                                            <td  class="p-2 pb-3 pr-0 d-flex float-right justify-content-start helo"  style="padding-left: 123px">
                                                <button class="btn text-left btn-xs red-button border-radius8 s-button" style="margin-right: 7px"  data-toggle="modal" data-target="#deleteSong"><span class="bi bi-trash3"  id="trash-icon"></span></button>
                                                <button class="btn text-left btn-xs orange-button border-radius8 s-button" data-toggle="modal" data-target="#editSong"><span class="bi bi-pencil-square fa-md" id="pencil-icon"></span></button>
                                            </td>
                                        </tr>                                        --}}
                                    </tbody>
                                </table>


{{-- Add Song Modal --}}
<div class="modal fade" id="addSong">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle" >
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-3 pb-2 pr-2" >
                <p class="modal-title add-bank-text" ><b class="font-size12">Add Song</b></p>
                <button type="button" class="close"  data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="addSongForm" class="needs-validation" novalidate>
            <div class="modal-body">
                <div class="row">
                   
                    <div class="col-12">
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Title</span></label>
                            <input type="text" class="form-control input-sm border-radius8 add-song-input-text"   placeholder="Enter song title" style="font-size: 12.5px" id="songName" name="songName" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Is Song ?</span></label>
                            <select id="setSongStatus" name="setSongStatus" class="form-control border-radius8 add-song-input-text"  onchange="setStatusValue();">
                                <option class="add-song-input-text">Paid Song</option>
                                <option class="add-song-input-text">Free Song</option>
                            </select>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div id="price-div" class="mb-2 mt-1 input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Price</span></label>
                            <input type="text" class="form-control input-sm border-radius8 add-song-input-text"   placeholder="Enter price" style="font-size: 12.5px" id="setPrice" name="setPrice" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Category</span></label>
                            <select class="form-control font-size12 border-radius8 add-song-input-text" id="songCategory" name="songCategory">
                                <option>Select Category</option>
                            </select>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Language</span></label>
                            <select class="form-control font-size12 border-radius8 add-song-input-text" id="songLanguage" name="songLanguage">
                                <option>Select Language</option>
                            </select>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Tumbnail <span class="add-song-span-text ">( Max 1MB )</span></span></label>
                            <input type="file" class="form-control input-sm border-radius8 "   placeholder="Choose Tumbnail (Max 1MB)" style="font-size: 12.5px" id="thumbnail" name="thumbnail" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Audio File <span class="add-song-span-text "> ( Max 5MB )</span></span></label>
                            <input type="file" class="form-control input-sm border-radius8"   placeholder="Choose file" style="font-size: 12.5px" id="audioFile" name="audioFile" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Video File <span class="add-song-span-text "> ( Max 5MB Only Mp4 ) ( Optional )</span></span></label>
                            <input type="file" class="form-control input-sm border-radius8"   placeholder="Choose file" style="font-size: 12.5px" id="videoFile" name="videoFile" >
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-3 mt-2 input-group-sm input_field " >
                            <label class="m-0  mb-1"> <span class="text-dark add-song-label-text"  ><b>Lyrics <span class="add-song-span-text "> ( Optional )</span></b></span></label>
                            <textarea rows="3" cols="50" name="songLyrics"  id="songLyrics"   class="form-control add-song-input-text input-xs rounded border-radius8"  placeholder="Enter Lyrics.." form="addSongForm" ></textarea>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer p-2 justify-content-between " style="margin-left: 4px">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">Canel</button>
                <input type="submit" class="btn red-button btn-xs mr-2 pt-1 pb-1 pl-3 pr-3 border-radius8 account-Create-button" value="Create">
            </div>
            </form>
        </div>
    </div>
</div>

{{-- Edit Song Modal --}}
<div class="modal fade" id="editSong">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle" >
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-3 pb-2 pr-2" >
                <p class="modal-title add-bank-text" ><b class="font-size12">Edit Song</b></p>
                <button type="button" class="close"  data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="addSongForm" class="needs-validation" novalidate>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Title</span></label>
                            <input type="text" class="form-control input-sm border-radius8 add-song-input-text"   value="Life is War" placeholder="Enter song title" style="font-size: 12.5px" id="editSongName" name="editSongName" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Is Song ?</span></label>
                            <select id="editSongStatus" name="editSongStatus" class="form-control border-radius8 add-song-input-text" value=""   onchange="editStatusValue();">
                                <option class="add-song-input-text">Paid Song</option>
                                <option class="add-song-input-text">Free Song</option>
                            </select>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div id="edit-price-div" class="mb-2 mt-1 input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Price</span></label>
                            <input type="text" class="form-control input-sm border-radius8 add-song-input-text"  value="4.0"  placeholder="Enter price" id="editPrice" name="editPrice" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Category</span></label>
                            <select class="form-control font-size12 border-radius8 add-song-input-text" id="editSongCategory" value="" name="editSongCategory">
                                <option>Select Category</option>
                            </select>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Language</span></label>
                            <select class="form-control font-size12 border-radius8 add-song-input-text" id="editSongLanguage" value="" name="editSongLanguage">
                                <option>Select Language</option>
                            </select>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Tumbnail <span class="add-song-span-text ">( Max 1MB )</span></span></label>
                            <input type="file" class="form-control input-sm border-radius8 " value=""   placeholder="Choose Tumbnail (Max 1MB)" style="font-size: 12.5px" id="editThumbnail" name="editThumbnail" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Audio File <span class="add-song-span-text "> ( Max 5MB )</span></span></label>
                            <input type="file" class="form-control input-sm border-radius8"  value=""  placeholder="Choose file" style="font-size: 12.5px" id="editAudioFile" name="editAudioFile" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Video File <span class="add-song-span-text "> ( Max 5MB Only Mp4 ) ( Optional )</span></span></label>
                            <input type="file" class="form-control input-sm border-radius8" value=""  placeholder="Choose file" style="font-size: 12.5px" id="editVideoFile" name="editVideoFile" >
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-3 mt-2 input-group-sm input_field " >
                            <label class="m-0  mb-1"> <span class="text-dark add-song-label-text"  ><b>Lyrics <span class="add-song-span-text "> ( Optional )</span></b></span></label>
                            <textarea rows="3" cols="50" name="editSongLyrics"  id="editSongLyrics"  value="" class="form-control add-song-input-text input-xs rounded border-radius8"  placeholder="Enter Lyrics.." form="addSongForm" ></textarea>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer p-2 justify-content-between " style="margin-left: 4px">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">Canel</button>
                <input type="submit" class="btn red-button btn-xs mr-2 pt-1 pb-1 pl-3 pr-3 border-radius8 account-Create-button" value="Update">
            </div>
            </form>
        </div>
    </div>
</div>

{{-- Delete Song Modal --}}
<div class="modal fade" id="deleteSong">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle">
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-3 pb-2" >
                <p class="modal-title notification-detail-text"><b>Delete Notification</b></p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col notification-detail-textt">
                        <small>Are You Sure You Want To Delete</small>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between ">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">No</button>
                <form>
                    <input type="submit" class="btn red-button btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8" value="Confirm">
                </form>
            </div>
        </div>
    </div>
</div>