<div class="row m-0 mb-1 mt-2">
    <div class="col ">
        <small class="float-left ml-2 pt-1"  style="font-weight:500">Albums List</small> 
        <button type="button" data-toggle="modal" data-target="#addAlbum" class="btn ml-2 red-button btn-xs border-radius8 add-album-button"><i class="fas fa-plus"></i> Create Album</button>
        <input type= "search" class="form-control ml-3 mr-3  float-right font-size12" id="albumFilterSearch"  placeholder="Search">
</div>
</div>

<div class="row m-0 mt-4" >
    <div class=" box ml-2" style="width: 88px">
        <center class=" float-left">
            <a href="#" data-toggle="modal" data-target="#albumDetail"><img src="{{asset('image/blog/blog1.jpg') }}"  class="border-radius5" width="80px" height="80px" alt="">
            <small  class="name" style="font-size: 10px;color:black">Welcome to </small></a>
        </center>
    </div>
    <div class=" box ml-2" style="width: 88px">
        <center class=" float-left">
            <img src="{{asset('image/blog/blog3.jpg') }}" class="border-radius5" width="80px" height="80px" alt="">
            <small class="name" style="font-size: 10px">70s Rock </small>
        </center>
    </div>
    <div class=" box ml-2" style="width: 88px">
        <center class=" float-left">
            <img src="{{asset('image/blog/blog1.jpg') }}"  class="border-radius5" width="80px" height="80px" alt="">
            <small  class="name" style="font-size: 10px">World </small>
        </center>
    </div>
    <div class=" box ml-2" style="width: 88px">
        <center class=" float-left">
            <img src="{{asset('image/blog/blog3.jpg') }}" class="border-radius5" width="80px" height="80px" alt="">
            <small class="name" style="font-size: 10px">80s Rock </small>
        </center>
    </div>
</div>





{{-- Album detail Modal --}}
<div class="modal fade" id="albumDetail">
    <div class="modal-dialog modal-dialog-centered modal-lg card-detail">
        <div class="modal-content border-radius12">
            <div class="modal-header border-radius-top pl-3 pt-4 pb-3 pr-2" style="background-color: #242424;color:white">
                <div class="col-1">
                    <div>
                        <img src="{{asset('image/blog/blog3.jpg') }}" style="" class="border-radius5 song-image" alt="thumbnail" width="45px" height="45px">
                    </div>
                </div>
                <div class="col-8 pl-3">
                    <div class="p-1 pl-0" id="song-title-text">
                        <small class="d-flex align-items-start" style="font-weight: 500">Life on Mars
                        <span class="dot-edit-delete" data-toggle="modal" data-target="#editAlbum"><i class="ml-1 mt-1 bi bi-pencil-fill pencil"></i></span>
                        <span class="dot-edit-delete" data-toggle="modal" data-target="#deleteAlbum"><i class="ml-1 mt-1 bi bi-trash3-fill pencil"></i></span>
                        </small>
                        <small class="d-flex align-items-end " style="font-size:9px;">05 Songs</small>
                    </div>
                </div>
                <div class="col-3  ">
                    <div class="float-right">
                        <button type="button" class="close pt-0" style="color: #fff"  data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="modal-body p-0 ">
                <div class="row">
                    <div class="col-12 table-wrapper-scroll-y my-custom-scrollbar">
                        <table class="table responsive ">
                            <tbody>
                                <tr>
                                    <td  class="p-0 pl-2">
                                        <ul class="products-list product-list-in-card pl-2 pr-1">
                                            <li class="item">
                                                <div class="product-img pt-1">
                                                    <img src="{{asset('image/blog/blog3.jpg') }}" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
                                                </div>
                                                <div class="product-info ml-5">
                                                    <small  class="product-title font-szie10">Life on Mars <span class="light-grey-text"> (3:56)</span>
                                                    <span class=" dot-play"><i class="bi bi-pause-fill" style="color: white;margin-left:4px !important"></i></span>
                                                    </small>
                                                    <small class="product-description font-szie10 light-grey-text">Free song</small>
                                                </div>
                                            </li>
                                        </ul>
                                    </td>
                                    <td class="pl-0 pr-0"> 
                                        <span class="ml-5 p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Audio</span>
                                        <span class="p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Video</span>
                                    </td>
                                    <td></td>
                                    <td class="pb-2" > 
                                        <button class="btn text-left btn-xs orange-button border-radius8 s-button" data-toggle="modal" data-target="#deleteSonginAlbum"><span class="bi bi-trash3"  id="trash-icon"></span></button>
                                        <button class="btn text-left btn-xs red-button border-radius8 s-button" style="margin-right: 7px"  data-toggle="modal" data-target="#editSongAlbum"><span class="bi bi-pencil-square fa-md" id="pencil-icon"></span></button>
                                    </td>
                                </tr>
                                
                            </tbody>
                        </table>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



{{-- Add Album  Modal --}}
<div class="modal fade" id="addAlbum">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle" >
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-2 pb-2 pr-2" >
                <p class="modal-title add-bank-text" ><b class="font-size12">Create Album</b></p>
                <button type="button" class="close"  data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="addAlbumForm" class="needs-validation" novalidate>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="mb-2 mt-0 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Title</span></label>
                            <input type="text" class="form-control input-sm border-radius8 add-song-input-text"   placeholder="Enter Album title" style="font-size: 12.5px" id="albumName" name="albumName" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-0"> <span class="text-dark add-song-label-text" >Tumbnail </span></label><br>
                            <input type="file" style="font-size: 12.5px;margin: .4rem 0;color:gray"      id="albumThumbnail" name="albumThumbnail" required><br>
                            <span class="add-song-span-text "> Max. size 1MB </span>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div id="edit-price-div" class="mb-2 mt-1 input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Add Song</span></label>
                            <input type="text" class="form-control input-sm border-radius8 add-song-input-text" data-toggle="modal" data-target="#selectingSong"   placeholder="Select Songs" id="selectSongName" name="selectSongName" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Add Songs</span></label>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="row mt-1" >
                            <div class="col-2 mt-1">
                                   <img src="{{asset('image/blog/blog3.jpg') }}" style="" class="border-radius5 song-image " alt="thumbnail" width="35px" height="35px">
                            </div>
                            <div class="col-8 ">
                                <small class="d-flex align-items-start">Life on Mars <span class="ml-1 light-grey-text font-size10 pt-1">(3:56)</span></small>
                                <small class="d-flex align-items-end light-grey-text font-size10" >Paid. $4</small>
                            </div>
                            <div class="col-2 p-2 ">
                                <i class="float-right bi bi-x-lg"></i>
                            </div>
                        </div>
                        <div class="row mt-1">
                             <div class="col-2 mt-1">
                                    <img src="{{asset('image/blog/blog3.jpg') }}" style="" class="border-radius5 song-image " alt="thumbnail" width="35px" height="35px">
                            </div>
                            <div class="col-8 ">
                                <small class="d-flex align-items-start">Life on Mars <span class="ml-1 light-grey-text font-size10 pt-1">(3:56)</span></small>
                                <small class="d-flex align-items-end light-grey-text font-size10">Free Song</small>
                            </div>
                            <div class="col-2 p-2 ">
                                <i class="float-right bi bi-x-lg"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer p-2 justify-content-between " style="margin-left: 4px">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">Canel</button>
                <input type="submit" class="btn red-button btn-xs mr-2 pt-1 pb-1 pl-3 pr-3 border-radius8 account-Create-button" value="Create">
            </div>
            </form>
        </div>
    </div>
</div>


{{-- Edit Album  Modal --}}
<div class="modal fade" id="editAlbum">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle" >
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-2 pb-2 pr-2" >
                <p class="modal-title add-bank-text" ><b class="font-size12">Edit Album</b></p>
                <button type="button" class="close"  data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="addAlbumForm" class="needs-validation" novalidate>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="mb-2 mt-0 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Title</span></label>
                            <input type="text" class="form-control input-sm border-radius8 add-song-input-text"   placeholder="Enter Album title" style="font-size: 12.5px" id="editAlbumName" name="editAlbumName" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-0"> <span class="text-dark add-song-label-text" >Tumbnail </span></label><br>
                            <input type="file" style="font-size: 12.5px;margin: .4rem 0;color:gray"      id="editAlbumThumbnail" name="editAlbumThumbnail" required><br>
                            <span class="add-song-span-text "> Max. size 1MB </span>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div id="edit-price-div" class="mb-2 mt-1 input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Add Song</span></label>
                            <input type="text" class="form-control input-sm border-radius8 add-song-input-text" data-toggle="modal" data-target="#selectingSong"   placeholder="Select Songs" id="editSelectSongName" name="editSelectSongName" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Add Songs</span></label>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="row mt-1" >
                            <div class="col-2 mt-1">
                                   <img src="{{asset('image/blog/blog3.jpg') }}" style="" class="border-radius5 song-image " alt="thumbnail" width="35px" height="35px">
                            </div>
                            <div class="col-8 ">
                                <small class="d-flex align-items-start">Life on Mars <span class="ml-1 light-grey-text font-size10 pt-1">(3:56)</span></small>
                                <small class="d-flex align-items-end light-grey-text font-size10" >Paid. $4</small>
                            </div>
                            <div class="col-2 p-2 ">
                                <i class="float-right bi bi-x-lg"></i>
                            </div>
                        </div>
                        <div class="row mt-1">
                             <div class="col-2 mt-1">
                                    <img src="{{asset('image/blog/blog3.jpg') }}" style="" class="border-radius5 song-image " alt="thumbnail" width="35px" height="35px">
                            </div>
                            <div class="col-8 ">
                                <small class="d-flex align-items-start">Life on Mars <span class="ml-1 light-grey-text font-size10 pt-1">(3:56)</span></small>
                                <small class="d-flex align-items-end light-grey-text font-size10">Free Song</small>
                            </div>
                            <div class="col-2 p-2 ">
                                <i class="float-right bi bi-x-lg"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer p-2 justify-content-between " style="margin-left: 4px">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">Canel</button>
                <input type="submit" class="btn red-button btn-xs mr-2 pt-1 pb-1 pl-3 pr-3 border-radius8 account-Create-button" value="Create">
            </div>
            </form>
        </div>
    </div>
</div>



{{-- Select Song  Modal --}}
<div class="modal fade" id="selectingSong">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle" >
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-2 pb-2 pr-2" >
                <p class="modal-title add-bank-text" ><b class="font-size12">Select Songs</b></p>
                <button type="button" class="close"  data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="addSelectingSongForm" class="needs-validation" novalidate>
            <div class="modal-body pl-1">
                        <div class="row" >
                            <div class="col-2 p-1 pr-0 mt-1 mr-0 float-right">
                                <label class="container float-right " id="selectAllCheck" >
                                    <input type="checkbox" class="float-right" onclick="validate()">
                                <span class="checkmark" style="margin-left: 19px"></span>
                                </label>
                            </div>
                            <div class="col-10 p-2">
                                <p class="modal-title add-bank-text" ><b class="font-size12">Select All</b></p>
                            </div>
                        </div>
                        <div class="row mt-1 mb-3" >
                            <div class="col-2 p-1 pr-0 mt-1 mr-0 float-right">
                                <label class="container float-right">
                                    <input type="checkbox" class="float-right">
                                <span class="checkmark" style="margin-left: 19px"></span>
                                </label>
                            </div>
                            <div class="col-2 mt-1 ml-0">
                                    <img src="{{asset('image/blog/blog3.jpg') }}" style="" class="border-radius5 song-image " alt="thumbnail" width="35px" height="35px">
                            </div>
                            <div class="col-8 ">
                                <small class="d-flex align-items-start">Life on Mars <span class="ml-1 light-grey-text font-size10 pt-1">(3:56)</span></small>
                                <small class="d-flex align-items-end light-grey-text font-size10" >Paid. $4</small>
                            </div>
                        </div>
                        <div class="row mt-1 mb-3" >
                            <div class="col-2 p-1 pr-0 mt-1 mr-0 float-right">
                                <label class="container float-right">
                                    <input type="checkbox" class="float-right">
                                <span class="checkmark" style="margin-left: 19px"></span>
                                </label>
                            </div>
                            <div class="col-2 mt-1 ml-0">
                                    <img src="{{asset('image/blog/blog3.jpg') }}" style="" class="border-radius5 song-image " alt="thumbnail" width="35px" height="35px">
                            </div>
                            <div class="col-8 ">
                                <small class="d-flex align-items-start">Life on Mars <span class="ml-1 light-grey-text font-size10 pt-1">(3:56)</span></small>
                                <small class="d-flex align-items-end light-grey-text font-size10" >Paid. $4</small>
                            </div>
                        </div>
                        <div class="row mt-1 mb-3" >
                            <div class="col-2 p-1 pr-0 mt-1 mr-0 float-right">
                                <label class="container float-right">
                                    <input type="checkbox" class="float-right">
                                <span class="checkmark" style="margin-left: 19px"></span>
                                </label>
                            </div>
                            <div class="col-2 mt-1 ml-0">
                                    <img src="{{asset('image/blog/blog3.jpg') }}" style="" class="border-radius5 song-image " alt="thumbnail" width="35px" height="35px">
                            </div>
                            <div class="col-8 ">
                                <small class="d-flex align-items-start">Life on Mars <span class="ml-1 light-grey-text font-size10 pt-1">(3:56)</span></small>
                                <small class="d-flex align-items-end light-grey-text font-size10" >Paid. $4</small>
                            </div>
                        </div>
                        <div class="row mt-1">
                            <div class="col-2 p-1 pr-0 mt-1 mr-0">
                                <label class="container">
                                    <input type="checkbox">
                                <span class="checkmark" style="margin-left: 19px"></span>
                                </label>
                            </div>
                             <div class="col-2 mt-1">
                                    <img src="{{asset('image/blog/blog3.jpg') }}" style="" class="border-radius5 song-image " alt="thumbnail" width="35px" height="35px">
                            </div>
                            <div class="col-8 ">
                                <small class="d-flex align-items-start">Life on Mars <span class="ml-1 light-grey-text font-size10 pt-1">(3:56)</span></small>
                                <small class="d-flex align-items-end light-grey-text font-size10">Free Song</small>
                            </div>
                        </div>
                    
            </div>
            <div class="modal-footer p-2 justify-content-between " style="margin-left: 4px">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">Canel</button>
                <input type="submit" class="btn red-button btn-xs mr-2 pt-1 pb-1 pl-3 pr-3 border-radius8 account-Create-button" value="Create">
            </div>
            </form>
        </div>
    </div>
</div>


{{-- Edit Song Modal --}}
<div class="modal fade" id="editSongAlbum">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle" >
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-3 pb-2 pr-2" >
                <p class="modal-title add-bank-text" ><b class="font-size12">Edit Song</b></p>
                <button type="button" class="close"  data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="addSongForm" class="needs-validation" novalidate>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Title</span></label>
                            <input type="text" class="form-control input-sm border-radius8 add-song-input-text"   value="Life is War" placeholder="Enter song title" style="font-size: 12.5px" id="editSongName" name="editSongName" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Is Song ?</span></label>
                            <select id="editSongStatus" name="editSongStatus" class="form-control border-radius8 add-song-input-text" value=""   onchange="editStatusValue();">
                                <option class="add-song-input-text">Paid Song</option>
                                <option class="add-song-input-text">Free Song</option>
                            </select>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div id="edit-price-div" class="mb-2 mt-1 input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Price</span></label>
                            <input type="text" class="form-control input-sm border-radius8 add-song-input-text"  value="4.0"  placeholder="Enter price" id="editPrice" name="editPrice" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text">Category</span></label>
                            <select class="form-control font-size12 border-radius8 add-song-input-text" id="editSongCategory" value="" name="editSongCategory">
                                <option>Select Category</option>
                            </select>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Language</span></label>
                            <select class="form-control font-size12 border-radius8 add-song-input-text" id="editSongLanguage" value="" name="editSongLanguage">
                                <option>Select Language</option>
                            </select>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Tumbnail <span class="add-song-span-text ">( Max 1MB )</span></span></label>
                            <input type="file" class="form-control input-sm border-radius8 " value=""   placeholder="Choose Tumbnail (Max 1MB)" style="font-size: 12.5px" id="editThumbnail" name="editThumbnail" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Audio File <span class="add-song-span-text "> ( Max 5MB )</span></span></label>
                            <input type="file" class="form-control input-sm border-radius8"  value=""  placeholder="Choose file" style="font-size: 12.5px" id="editAudioFile" name="editAudioFile" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-2 mt-1 form-group input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark add-song-label-text" >Video File <span class="add-song-span-text "> ( Max 5MB Only Mp4 ) ( Optional )</span></span></label>
                            <input type="file" class="form-control input-sm border-radius8" value=""  placeholder="Choose file" style="font-size: 12.5px" id="editVideoFile" name="editVideoFile" >
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-3 mt-2 input-group-sm input_field " >
                            <label class="m-0  mb-1"> <span class="text-dark add-song-label-text"  ><b>Lyrics <span class="add-song-span-text "> ( Optional )</span></b></span></label>
                            <textarea rows="3" cols="50" name="editSongLyrics"  id="editSongLyrics"  value="" class="form-control add-song-input-text input-xs rounded border-radius8"  placeholder="Enter Lyrics.." form="addSongForm" ></textarea>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer p-2 justify-content-between " style="margin-left: 4px">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">Canel</button>
                <input type="submit" class="btn red-button btn-xs mr-2 pt-1 pb-1 pl-3 pr-3 border-radius8 account-Create-button" value="Update">
            </div>
            </form>
        </div>
    </div>
</div>



{{-- Delete Song from album Modal --}}
<div class="modal fade" id="deleteSonginAlbum">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle">
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-3 pb-2" >
                <p class="modal-title notification-detail-text"><b>Confirmation</b></p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col notification-detail-textt">
                        <small>Are You Sure You Want To Delete this Song</small>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between ">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">No</button>
                <form>
                    <input type="submit" class="btn red-button btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8" value="Confirm">
                </form>
            </div>
        </div>
    </div>
</div>


{{-- Delete album Modal --}}
<div class="modal fade" id="deleteAlbum">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle">
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-3 pb-2" >
                <p class="modal-title notification-detail-text"><b>Confirmation</b></p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col notification-detail-textt">
                        <small>Are You Sure You Want To Delete this Album</small>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between ">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">No</button>
                <form>
                    <input type="submit" class="btn red-button btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8" value="Confirm">
                </form>
            </div>
        </div>
    </div>
</div>