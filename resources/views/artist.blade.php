@extends('layouts.master')

@section('title')
Q Music
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('css/customnotification.css')}}">
<style>
/* The container */
.container {
  display: block;
  position: relative;
  padding-left: 35px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 22px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default checkbox */
.container input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}

/* Create a custom checkbox */
.checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 25px;
  width: 25px;
  background-color: #eee;
}

/* On mouse-over, add a grey background color */
.container:hover input ~ .checkmark {
  background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.container input:checked ~ .checkmark {
  background-color: #ba011a;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the checkmark when checked */
.container input:checked ~ .checkmark:after {
  display: block;
}

/* Style the checkmark/indicator */
.container .checkmark:after {
  left: 9px;
  top: 5px;
  width: 7px;
  height: 14px;
  border: solid white;
  border-width: 0 3px 3px 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}

.sidenav {
  height: 100%;
  width: 0;
  position: fixed;
  z-index: 1;
  top: 0;
  right: 0;
  background-color: #fff;
  overflow-x: hidden;
  transition: 0.5s;
  padding-top: 60px;
  
}
#mySideClose{
  border-radius: 32%;
  height: 37px;
  width: 0;
  position: absolute;
  z-index: 1;
  top: 0px;
  right:255px;
  background-color: #fff;
  overflow-x: hidden;
  transition: 0.5s;
  padding-top: 0px;
margin-top: -46px;

}
#mySideClose a {
    
  /* padding: 8px 8px 8px 32px; */
  text-decoration: none;
  font-size: 25px;
  color: #ba011a;
  display: block;
  transition: 0.3s;
}
.sidenav a {
  padding: 8px 8px 8px 32px;
  text-decoration: none;
  font-size: 25px;
  color: #818181;
  display: block;
  transition: 0.3s;
}
#mySideClose a:hover{
  color: #f1f1f1;  
}
.sidenav a:hover {
  color: #f1f1f1;
}
#mySideClose .closebtn{
  position: absolute;
  top: 0;
  right: 25px;
  font-size: 36px;
  margin-left: 50px;  
}
.sidenav .closebtn {
  position: absolute;
  top: 0;
  right: 25px;
  font-size: 36px;
  margin-left: 50px;
}

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}

</style>
@endsection

@section('content')
<div class="row  pt-1" style="padding-bottom: 13px !important">
    <div class="col">
        <span class="withdrawal-text">Artist Profile</span>
    </div>
    
   
</div>

<div class="row  pt-1 pb-3">
    <div class="col-lg-3 col-md-12">
        <div class="card border-radius8">
            <div class="card-body border-radius8 pt-3 pl-4 pr-4 pb-3">
                <div class="row">
                    <div class="d-flex justify-content-center mb-2">
                        <div id="image-show" class ="roundedImage rounded-circle mt-2">
                        </div>
                    </div>
                    <small class="text-center" style="font-weight:400;color:#212529">Anna Nestrom</small>
                    <small class="text-center light-grey-text ">sami.ullah@techswivel.com</small>
                    <div class="dropdown-divider"></div>
                    <div class="pl-0 pr-0">
                        <i class="bi bi-calendar4-event icon-blue"></i><small class="light-grey-text pl-2">Join Date</small><small class="float-right font-weight500" style="font-size:10px;padding-top:8px">03-01-2021</small>
                    </div>     
                    <div class="dropdown-divider"></div>
                    <div class="pl-0 pr-0">
                        <i class="bi bi-tag icon-blue"></i><small class="light-grey-text pl-2">User ID</small><small class="float-right font-weight500" style="font-weight:bold;font-size:10px;padding-top:6px">00001</small>    
                    </div>
                    <div class="dropdown-divider"></div>
                    <div class="small-space pl-0 pr-0">
                        <small class=" light-grey-text">A Good Place To Start Is To Tell Your Audience About Your Specific Are of Expertise. Do You
                            Have A Formal Education In The Arts Or Art History? Great! Tell Your Audience Where And When You Studied, Perhaps
                            Where And When You Studied, Perhaps 
                            </small>
                    <div class="mt-3">
                        <a href="#" id="editProfile"  class="btn btn-block rounded-lg red-button font-weight-lighter text-white border-radius8 edit-profile" data-toggle="modal" data-target="#edit-Profile" >Edit Profile</a>
                    </div>
                </div>
                </div>
            </div>
        </div>
        
    </div>
    <div class="col-lg-9 col-md-12" >
                <div class="card">
                    <div class="card-header pb-0" >
                        <div class="row pb-1">
                            <div class="col-lg-2  pt-2">
                                <div class="card earn-box text-center pb-3">
                                        <span class="info-box-text font-weight-lighter earn-box-title">No. Of Songs</span>
                                        <span class="info-box-number font-weight-lighter earn-box-info" >30</span>
                                </div>
                             </div>
                            <div class="col-lg-2 pt-2">
                                <div class="card earn-box text-center pb-3">
                                        <span class="info-box-text font-weight-lighter earn-box-title">Album</span>
                                        <span class="info-box-number font-weight-lighter earn-box-info" >5</span>
                                </div>
                            </div>
                            <div class="col-lg-2 pt-2">
                                <div class="card earn-box text-center pb-3">
                                        <span class="info-box-text font-weight-lighter earn-box-title" >Paid Songs</span>
                                        <span class="info-box-number font-weight-lighter earn-box-info" >10</span>
                                </div>
                            </div>
                            <div class="col-lg-2 pt-2">
                                <div class="card earn-box text-center pb-3">
                                        <span class="info-box-text font-weight-lighter earn-box-title" >Paid Album</span>
                                        <span class="info-box-number font-weight-lighter earn-box-info" >3</span>
                                </div>
                            </div>
                            <div class="col-lg-2 pt-2">
                                <div class="card earn-box text-center pb-3">
                                        <span class="info-box-text font-weight-lighter earn-box-title" >Earnings</span>
                                        <span class="info-box-number font-weight-lighter earn-box-info" >$300</span>
                                </div>
                            </div>
                            <div class="col-lg-2 pt-2">
                                <div class="card earn-box text-center pb-3">
                                        <span class="info-box-text font-weight-lighter earn-box-total-title" >Pre Booking Earnings</span>
                                        <span class="info-box-number font-weight-lighter earn-box-info" >$204</span>
                                </div>
                            </div>
                        </div>
                        <div class="row ">
                            <ul class="nav nav-pills ml-auto " style="margin-left: -12px !important;margin-bottom: -1px !important;font-size: 11px;">
                                <li class="nav-item"><span class="nav-link active" href="#tab_1" data-toggle="tab" style="">All Songs</span></li>
                                <li class="nav-item"><span class="nav-link" href="#tab_2" data-toggle="tab">All Albums</span></li>
                                <li class="nav-item"><span class="nav-link" href="#tab_3" data-toggle="tab">Earning Stats</span></li>
                                <li class="nav-item"><span class="nav-link" href="#tab_4" data-toggle="tab">Pre Orders</span></li>
                                <li class="nav-item"><span class="nav-link" href="#tab_5" data-toggle="tab">Followers</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body pt-2 pl-0 pr-0">
                        <div class="tab-content">
                            <div class="tab-pane active table-responsive" id="tab_1">
                                @include('artist.song')
                            </div>
                            <div class="tab-pane " id="tab_2">
                                @include('artist.album')
                            </div>
                            <div class="tab-pane table-responsive" id="tab_3">
                                @include('artist.earningstat')
                            </div>
                            <div class="tab-pane mt-1 ml-1" id="tab_4">
                                @include('artist.preorder')
                            </div>
                            <div class="tab-pane table-responsive" id="tab_5">
                                @include('artist.followers')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>  


{{-- Edit Profile Modal --}}
<div class="modal fade" id="edit-Profile">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle" >
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-3 pb-2 pr-2" >
                <p class="modal-title add-bank-text" ><b>Edit Artist Profile</b></p>
                <button type="button" class="close"  data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="editProfileForm" class="needs-validation" novalidate>
            <div class="modal-body">
                <div class="row">
                    <div class="d-flex justify-content-center mb-4">
                    <div class=" mb-1  mt-3 input-group-sm input_field" >
                        <div class="d-flex flex-row">
                            <div id="image-show-detail" class ="roundedImage rounded-circle mr-1 mt-2">
                            </div>
                            <div class="d-flex align-items-end image-div">
                                <label>
                                    <span class="dot d-flex justify-content-center">
                                        <b class="plus">+</b>
                                    </span>
                                    <input type="file" id="fileUpload" style="display: none;" required>
                                </label>
                            </div>
                            <div class="invalid-feedback">Please fill out this field.</div>
                            <div class="valid-feedback">Valid!</div>
                        </div>
                    </div>
                </div>
                    <div class="col-12">
                        <div class="mb-2 mt-1 input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark " style="font-size: 13.5px;font-weight:500;color:black">Name</span></label>
                            <input type="text" class="form-control input-sm border-radius8" value="Sami Ullah"  placeholder="Enter name" style="font-size: 12.5px" id="editName" name="editName" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-1 mt-1 input-group-sm input_field " >
                            <label class="m-0 mb-1"> <span class="text-dark " style="font-size: 13.5px;font-weight:500">Email</span></label>
                            <input type="email" class="form-control input-sm border-radius8 add-bank-feild" value="sami.ullah@techswivel.com" placeholder="Enter email" style="font-size: 12.5px" id="editEmail" name="editEmail" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="mb-3 mt-2 input-group-sm input_field " >
                            <label class="m-0  mb-1"> <span class="text-dark "  style="font-size: 13.5px;font-weight:500"><b>About Artist</b></span></label>
                            <textarea rows="3" cols="50" name="editBio"  id="editBio"   class="form-control input-xs rounded border-radius8"  placeholder="Enter Bio" form="editProfileForm" required>A Good Place To Start Is To Tell Your Audience</textarea>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer p-2 justify-content-between " style="margin-left: 4px">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">Canel</button>
                <input type="submit" class="btn red-button btn-xs mr-2 pt-1 pb-1 pl-3 pr-3 border-radius8 account-Create-button" value="Update">
            </div>
            </form>
        </div>
    </div>
</div>



@endsection



@section('script')

<script>
    $(function()
    {
        $('#fileUpload').on('change',function ()
        {
            var tmppath = URL.createObjectURL(event.target.files[0]);
            console.log(tmppath);
            document.getElementById("image-show-detail").style.backgroundImage = "url("+tmppath+")";
        });
    });
 </script>
<script>
    // Disable form submissions if there are invalid fields
    (function() {
      'use strict';
      window.addEventListener('load', function() {
        // Get the forms we want to add validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
          form.addEventListener('submit', function(event) {
            if (form.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
            }
            form.classList.add('was-validated');
          }, false);
        });
      }, false);
    })();
    </script> 
 <script>
  $(document).ready(function() {
        $("#editName").on('click', function(event) {
            event.preventDefault();
            $('#editName').addClass( "border-danger" );        
        });
        $( "#editName" ).mouseleave(function() {
            $('#editName').removeClass( "border-danger" );
        });
        $("#editEmail").on('click', function(event) {
            event.preventDefault();
        $('#editEmail').addClass( "border-danger" );        
        });
        $("#editEmail" ).mouseleave(function() {
            $('#editEmail').removeClass( "border-danger" );
        });
        $("#editBio").on('click', function(event) {
            event.preventDefault();
            $('#editBio').addClass( "border-danger" );        
        });
        $( "#editBio" ).mouseleave(function() {
            $('#editBio').removeClass( "border-danger" );
       }); 
  });
  </script>

  <script>
    $(document).ready(function() {
        table = $('#songTable').DataTable({
            "ordering": false,
            "scrollY": true,
            "dom":"<'row  p-2 pt-2 padding-top0'<'col-lg-4 '<'song-div'>><'col-lg-6 pr-3'l<'song-status-div'>><'col-lg-2 pr-3'<'song-search'>>><'bottom'rtp>",
            // "dom":"<<'song-div'><'song-search'>l<'song-status-div'><'bottom'rtp>>",
            "initComplete": function(settings, json) {
                $('.dataTables_length').addClass('dataTables_length');
       }
        });
         $("div.song-status-div").html('<select class="float-right p-1 mr-1  month-select font-size12" id="song-status" name="song-status" onchange="getStatusValue();"><option>All Song</option><option>Free Song</option><option>Paid Song</option></select>');
        $("div.song-div").html('<small class="float-left ml-2 pt-1"  style="font-weight:500">Songs List</small> <button type="button" data-toggle="modal" data-target="#addSong" class="btn ml-2 red-button btn-xs border-radius8 add-song-button"><i class="fas fa-plus"></i> Add Song</button>');
        $("div.song-search").html('<input type= "search" class="form-control ml-3 mr-0 float-right font-size12" id="songfilterSearch"  placeholder="Search">');
        //  $("div.allnoti").html('<span class="pr-2 all-noti-text"style="">All Notifications</span>');
     $('#songfilterSearch').keyup(function(){
       table.search(this.value).draw();
       });
         
    });
    function getStatusValue(){
        var conceptName = $('#song-status').find(":selected").text();
        if(conceptName == 'All Song')
        {
            table.search('').draw();
             
        }else{
            table.search(conceptName).draw();
        }
        
    } 
    </script>
  <script>
    $(document).ready(function() {
        followerTable = $('#followerTable').DataTable({
            "dom":"<'row  p-2 pt-2 padding-top0'<'col-lg-4 pt-1'<'follower-div'>><'col-lg-6 pr-3'l><'col-lg-2 pr-3'<'follower-search'>>><'bottom'rtp>",
            "initComplete": function(settings, json) {
                    $('.dataTables_paginate').addClass('dataTables_paginate');
                $('.dataTables_length').addClass('dataTables_length');
        }
        });
        $("div.follower-div").html('<small class="float-left ml-2"  style="font-weight:500">All Followers</small>');
        $("div.follower-search").html('<input type= "search" class="form-control  float-right font-size12" id="followfilterSearch"  placeholder="Search">');
        $('#followfilterSearch').keyup(function(){
       followerTable.search(this.value).draw();
       });
    });

    </script>

<script>
    $(document).ready(function() {
        statTable = $('#earningStatTable').DataTable({
            "dom":"<'row  p-2 pt-2 padding-top0'<'col-lg-4 pt-1'<'earningstat-div'>><'col-lg-6 pr-3'l><'col-lg-2 pr-3'<'earningstat-search'>>><'bottom'rtp>",
            "initComplete": function(settings, json) {
                    $('.dataTables_paginate').addClass('dataTables_paginate');
                // $('.dataTables_length').addClass('dataTables_length');
        }
        });
        $("div.earningstat-div").html('<small class="float-left ml-2"  style="font-weight:500">Earning History</small>');
        $("div.earningstat-search").html('<input type= "search" class="form-control  float-right font-size12" id="statfilterSearch"  placeholder="Search">');
        $('#statfilterSearch').keyup(function(){
       statTable.search(this.value).draw();
       });
    });

    </script>

<script>
    function setStatusValue(){
        var conceptName = $('#setSongStatus').find(":selected").text();
        if(conceptName == 'Free Song')
        {
            $("#price-div").hide(300);
        }else if(conceptName == 'Paid Song'){
            $("#price-div").show(300);
        }
        
    }
    function editStatusValue(){
        var conceptName = $('#editSongStatus').find(":selected").text();
        if(conceptName == 'Free Song')
        {
            $("#edit-price-div").hide(300);
        }else if(conceptName == 'Paid Song'){
            $("#edit-price-div").show(300);
        }
        
    }
</script>

 <script>
     $(document).ready(function(){
    
    var $search = $("#albumFilterSearch").on('input',function(){
        var matcher = new RegExp($(this).val(), 'gi');
        $('.box').show().not(function(){
            return matcher.test($(this).find('.name').text())
        }).hide();
    });
});
 </script>
 <script type="text/javascript">
    function validate() {
        if (document.getElementById('selectAllCheck').checked) {
            alert("checked");
        } else {
            alert("You didn't check it! Let me check it for you.");
        }
    }
</script>
<script>
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
  document.getElementById("mySideClose").style.width = "37px";
}


function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("mySideClose").style.width = "0";
}
</script>
@endsection

