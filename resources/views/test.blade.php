<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="bootstrap.min.css">
</head>

<body>
    <h4>jQuery Filter by category and search</h1>
        <br>
        <hr>

    <div class="row">
        <div class="col-md-4">
            <input type="text" id="search" class="form-control" placeholder="Search product by name or sku">
        </div>
    </div>
    <hr>
    <div class="row" id="parent">
        <div class="col-md-2 box">
            <center>
                <img src="http://via.placeholder.com/80x80" class="" alt="">
                <p class="name">Pepsi </p>
            </center>
        </div>
        <div class="col-md-2 box">
            <center>
                <img src="http://via.placeholder.com/80x80" class="" alt="">
                <p class="name">Cocacola </p>
            </center>
        </div>

        <div class="col-md-2 box">
            <center>
                <img src="http://via.placeholder.com/80x80" class="" alt="">
                <p class="name">Mountien Dwe </p>
            </center>
        </div>


        <div class="col-md-2 box">
            <center>
                <img src="http://via.placeholder.com/80x80" class="" alt="">
                <p class="name">Burger </p>
        \    </center>
        </div>

        <div class="col-md-2 box">
                <center>
                    <img src="http://via.placeholder.com/80x80" class="" alt="">
                    <p class="name">Hot Dog </p>
                    <p>$ 2,410</p>
                </center>
            </div>



    </div>


    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script>
      $(document).ready(function(){
    
    var $search = $("#search").on('input',function(){
        var matcher = new RegExp($(this).val(), 'gi');
        $('.box').show().not(function(){
            return matcher.test($(this).find('.name').text())
        }).hide();
    })
})
    </script>
</body>

</html>