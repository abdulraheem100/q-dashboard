@extends('auth.layouts.login-master')


@section('title')
    Sign In
@endsection

@section('style')
@endsection

@section('content')
<div class="card p-2 mt-4 login_card" id="signin-card">
    <div class="card-body login-card-body" id="login_card_body">
      <div class="data-content mt-1">
        <div>
          <h6 class="font-weight-bold font-szie1 pl-1" id="signin-text">Sign In</h6>
        </div>
        <p class="m-0 text-secondary small pl-1" style="margin-top: -9px !important;font-size:13.5px ">Welcome to Q the Music Artist Dashboard.</p>
      </div>
      <div class="container p-1 pt-3">  
        <form class="needs-validation" novalidate>
          <label class="m-0"><small class="text-dark mb-0 font-wegiht600">Email Address</small></label>
            <div class="mb-1 mt-1 input-group-sm input_field " >
              <input type="email" class="form-control input-sm border-radius8 " style="font-size: 12px"  placeholder="Email" id="email" name="email" required>
                <div class="invalid-feedback">Please fill out this field.</div>
                <div class="valid-feedback">Valid!</div>
            </div>
          <label class="m-0 mt-1"><small class="text-dark m-0 font-wegiht600">Password</small></label>
          <div class="input-group mt-1 mb-3 input-group-sm round-7" id="show_hide_password">
            <input type="password" class="form-control input-sm round-7 password-radius" style="font-size: 12px" placeholder="Password" id="password" name="password" required>
              <div class="input-group-append rounded">
                <div class="input-group-text eye-icon" id="password-eye" style=""><a href="#">
                  <span class="fas fa-eye-slash eye-grey" id="eye-icon"></span></a>
                </div>
              </div>
              <div class="invalid-feedback">Please fill out this field.</div>
              <div class="valid-feedback">Valid!</div>
          </div>
          <input type="submit" class="btn btn-block btn-danger btn-sm rounded-lg small font-weight-lighter red-button" value="LOGIN">
        </form>
        <div class="pb-3">
          <a  href="/forget-password" ><small class="float-right text-xs pt-2 red-text font-szie1">Forget Password?</small></a>
        </div>
        <div class="mt-5 mb-1 rounded-lg">
          <a  href="/register"  class="btn btn-block btn-danger btn-sm rounded-lg light-red-button">Sign Up as Artist</a>
        </div>
      </div>
    </div>
  </div>
@endsection



@section('script')
<script>
  $(document).ready(function() {
      $("#email").on('click', function(event) {
        event.preventDefault();
        $('#email').addClass( "border-danger" );        
      });
      $("#email" ).mouseleave(function() {
        $('#email').removeClass( "border-danger" );
      });     
      $("#password").on('click', function(event) {
        event.preventDefault();
        $('#password').addClass( "border-danger" );
        $('#password-eye').addClass( "border-danger" );        
      });
      $("#password" ).mouseleave(function() {
        $('#password').removeClass( "border-danger" );
        $('#password-eye').removeClass( "border-danger" );
      });   
  });
</script>
<script>
  $(document).ready(function() {
    $("#show_hide_password a").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_password input').attr("type") == "text"){
            $('#show_hide_password input').attr('type', 'password');
            $('#show_hide_password span').addClass( "fa-eye-slash" );
            $('#show_hide_password span').removeClass( "fa-eye" );
        }else if($('#show_hide_password input').attr("type") == "password"){
            $('#show_hide_password input').attr('type', 'text');
            $('#show_hide_password span').removeClass( "fa-eye-slash" );
            $('#show_hide_password span').addClass( "fa-eye" );
        }
    });
});
</script>
<script>
  // Disable form submissions if there are invalid fields
  (function() {'use strict';
    window.addEventListener('load', function() {
      // Get the forms we want to add validation styles to
      var forms = document.getElementsByClassName('needs-validation');
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
          if (form.checkValidity() === false) {
            document.getElementById("signin-card").setAttribute("style", "height:27rem;");
            event.preventDefault();
            event.stopPropagation();
          }else{
            document.getElementById("signin-card").setAttribute("style", "height:27rem;");
          }
          form.classList.add('was-validated');
        }, false);
      });
    }, false);
  })();
  </script>
@endsection
