@extends('auth.layouts.admin-login-master')


@section('title')
    New Password
@endsection

@section('style')
<style>
  body {
    -ms-flex-align: center;
    align-items: center;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
    height: 100vh;
    -ms-flex-pack: center;
    justify-content: center;
    background-image: url("../image/b3.jpg");
    background-repeat: no-repeat;
    background-attachment: fixed;  
    background-size: cover;
  }
  .login_card{
    height: 20rem ;
   margin-top: 35px !important;
   border-radius:7% !important; 
  }
  .login-logo{
    margin-top: -105.5px 
  }
    #signin-text{
      color: black;
      font-family: 'IBM Plex Sans Thai Looped', sans-serif;
    }
    .font-szie1{
      font-size: 19px;
    }
    .font-wegiht600{
      font-weight: 600;
    }
    #welcome-text{
     margin-top: -8px !important; 
    }
    </style>
@endsection

@section('content')
<div class="card p-2 mt-4 login_card" id="signin-card">
    <div class="card-body login-card-body" id="login_card_body">
      <div class="data-content mt-2">
        <div>
          <h6 class="font-weight-bold font-szie1 pb-1 mb-1 pl-1" id="signin-text">New Password</h6>
        </div>
        <p class="m-0 text-secondary small pl-1" style="margin-top: -9px !important;font-size:13.5px ">Set New password to your account.</p>
      </div>
      <div class="container p-1 pt-2">  
        <form class="needs-validation" novalidate>
          <label class="m-0 mt-1"><small class="text-dark m-0 font-wegiht600">Password</small></label>
          <div class="input-group mt-1  mb-1 input-group-sm round-7" id="show_hide_password">
            <input type="password" class="form-control input-sm round-7 password-radius" style="font-size: 12px" placeholder="New Password" id="password" name="password" required>
              <div class="input-group-append rounded">
                <div class="input-group-text eye-icon" id="password-eye" style=""><a href="#">
                  <span class="fas fa-eye-slash eye-grey" id="eye-icon"></span></a>
                </div>
              </div>
              <div class="invalid-feedback">Please fill out this field.</div>
              <div class="valid-feedback">Valid!</div>
          </div>
           <label class="m-0 mt-1"><small class="text-dark m-0 font-wegiht600">Confirm Password</small></label>
          <div class="input-group mt-1 mb-3 input-group-sm round-7" id="show_hide_confirmPassword">
            <input type="password" class="form-control input-sm round-7 password-radius" style="font-size: 12px" placeholder="Confirm Password" id="confirmPassword" name="confirmPassword" required>
              <div class="input-group-append rounded">
                <div class="input-group-text eye-icon" id="confirmPassword-eye" style=""><a href="#">
                  <span class="fas fa-eye-slash eye-grey" id="eye-icon"></span></a>
                </div>
              </div>
              <div class="invalid-feedback">Please fill out this field.</div>
              <div class="valid-feedback">Valid!</div>
          </div>
          <input type="submit" class="btn btn-block btn-danger btn-sm rounded-lg small font-weight-lighter red-button" value="Done">
        </form>
      </div>
    </div>
  </div>
@endsection



@section('script')
<script>
  $(document).ready(function() {
      $("#password").on('click', function(event) {
        event.preventDefault();
        $('#password').addClass( "border-danger" );
        $('#password-eye').addClass( "border-danger" );        
      });
      $("#password" ).mouseleave(function() {
        $('#password').removeClass( "border-danger" );
        $('#password-eye').removeClass( "border-danger" );
      });
      $("#confirmPassword").on('click', function(event) {
        event.preventDefault();
        $('#confirmPassword').addClass( "border-danger" );
        $('#confirmPassword-eye').addClass( "border-danger" );        
      });
      $("#confirmPassword" ).mouseleave(function() {
        $('#confirmPassword').removeClass( "border-danger" );
        $('#confirmPassword-eye').removeClass( "border-danger" );
      });   
  });
</script>
<script>
  $(document).ready(function() {
    $("#show_hide_password a").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_password input').attr("type") == "text"){
            $('#show_hide_password input').attr('type', 'password');
            $('#show_hide_password span').addClass( "fa-eye-slash" );
            $('#show_hide_password span').removeClass( "fa-eye" );
        }else if($('#show_hide_password input').attr("type") == "password"){
            $('#show_hide_password input').attr('type', 'text');
            $('#show_hide_password span').removeClass( "fa-eye-slash" );
            $('#show_hide_password span').addClass( "fa-eye" );
        }
    });

    $("#show_hide_confirmPassword a").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_confirmPassword input').attr("type") == "text"){
            $('#show_hide_confirmPassword input').attr('type', 'password');
            $('#show_hide_confirmPassword span').addClass( "fa-eye-slash" );
            $('#show_hide_confirmPassword span').removeClass( "fa-eye" );
        }else if($('#show_hide_confirmPassword input').attr("type") == "password"){
            $('#show_hide_confirmPassword input').attr('type', 'text');
            $('#show_hide_confirmPassword span').removeClass( "fa-eye-slash" );
            $('#show_hide_confirmPassword span').addClass( "fa-eye" );
        }
    });
});
</script>
<script>
  // Disable form submissions if there are invalid fields
  (function() {'use strict';
    window.addEventListener('load', function() {
      // Get the forms we want to add validation styles to
      var forms = document.getElementsByClassName('needs-validation');
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
          if (form.checkValidity() === false) {
            document.getElementById("signin-card").setAttribute("style", "height:22rem;");
            event.preventDefault();
            event.stopPropagation();
          }else{
            document.getElementById("signin-card").setAttribute("style", "height:22rem;");
          }
          form.classList.add('was-validated');
        }, false);
      });
    }, false);
  })();
  </script>
@endsection
