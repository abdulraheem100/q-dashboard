@extends('auth.layouts.admin-login-master')


@section('title')
    Forgot Password
@endsection

@section('style')
<style>
  body {
    -ms-flex-align: center;
    align-items: center;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
    height: 100vh;
    -ms-flex-pack: center;
    justify-content: center;
    background-image: url("../image/b3.jpg");
    background-repeat: no-repeat;
    background-attachment: fixed;  
    background-size: cover;
  }
  .login_card{
    height: 20rem ;
   margin-top: 35px !important;
   border-radius:7% !important; 
  }
  .login-logo{
    margin-top: -105.5px 
  }
    #signin-text{
      color: black;
      font-family: 'IBM Plex Sans Thai Looped', sans-serif;
    }
    .font-szie1{
      font-size: 19px;
    }
    .font-wegiht600{
      font-weight: 600;
    }
    #welcome-text{
     margin-top: -8px !important; 
    }
    </style>
@endsection

@section('content')
<div class="card p-2 mt-4 login_card" id="signin-card">
    <div class="card-body login-card-body" id="login_card_body">
      <div class="data-content mt-2">
        <div>
          <h6 class="font-weight-bold font-szie1 pb-1 mb-1 pl-1" id="signin-text">Forgot Password</h6>
        </div>
        <p class="m-0 text-secondary small pl-1" style="margin-top: -9px !important;font-size:13.5px ">Verify email to reset password.</p>
      </div>
      <div class="container p-1 pt-4" style="margin-top: 7.5px">  
        <form  class="needs-validation pb-1" novalidate>
          <label class="m-0"><small class="text-dark mb-0 font-wegiht600">Email Address</small></label>
            <div class="mb-1 mt-1 mb-3 input-group-sm input_field " >
              <input type="email" class="form-control input-sm border-radius8 " style="font-size: 12px"  placeholder="Email" id="verifyEmail" name="verifyEmail" required>
                <div class="invalid-feedback">Please fill out this field.</div>
                <div class="valid-feedback">Valid!</div>
            </div>
          <input type="submit" class="btn btn-block btn-danger btn-sm rounded-lg small font-weight-lighter red-button" value="Send Link">
        </form>
        <div class="pt-2 pb-0 text-center">
          <a  href="{{ route('admin.login')}}" ><small class=" text-xs pt-2 red-text font-szie1 font-wegiht600" >Back to login</small></a>
        </div>
        
      </div>
    </div>
  </div>
@endsection



@section('script')
<script>
  $(document).ready(function() {
      $("#verifyEmail").on('click', function(event) {
        event.preventDefault();
        $('#verifyEmail').addClass( "border-danger" );        
      });
      $("#verifyEmail" ).mouseleave(function() {
        $('#verifyEmail').removeClass( "border-danger" );
      });     
         
  });
</script>
<script>
  // Disable form submissions if there are invalid fields
  (function() {'use strict';
    window.addEventListener('load', function() {
      // Get the forms we want to add validation styles to
      var forms = document.getElementsByClassName('needs-validation');
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
          if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
          }else{
          }
          form.classList.add('was-validated');
        }, false);
      });
    }, false);
  })();
  </script>
@endsection
