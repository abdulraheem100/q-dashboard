<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>@yield('title')</title>
   <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('css/app.css')}}">
  <link rel="stylesheet" href="{{ asset('dist/css/adminlte.css')}}">
 <link rel="stylesheet" href="{{ asset('css/custom.css')}}">
  <!--style sheet -->
  <style>
  body {
    -ms-flex-align: center;
    align-items: center;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
    height: 100vh;
    -ms-flex-pack: center;
    justify-content: center;
    background-image: url("../image/background.jpg");
    background-repeat: no-repeat;
    background-attachment: fixed;  
    background-size: cover;
  }
    #signin-text{
      color: black;
      font-family: 'IBM Plex Sans Thai Looped', sans-serif;
    }
    .font-szie1{
  font-size: 19px;
    }
    .font-wegiht600{
      font-weight: 600;
    }
    #welcome-text{
     margin-top: -8px !important; 
    }
    </style>
  @yield('style')
</head>
<body>
  <div class="login-box">
    <div class="login-logo"> 
      <img src="{{ asset('image/logo1.png')}}" alt="logo" width="65" height="65">
    </div>
    <div class="contrainer ">
      @yield('content')
    </div>
  </div>

<!-- jQuery -->
<script src="{{ asset('js/jquery.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('js/app.js')}}"></script>

@yield('script')

</body>
</html>
