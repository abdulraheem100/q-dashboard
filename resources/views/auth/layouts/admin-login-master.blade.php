<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>@yield('title')</title>
   <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('css/app.css')}}">
  <link rel="stylesheet" href="{{ asset('dist/css/adminlte.css')}}">
 <link rel="stylesheet" href="{{ asset('css/custom.css')}}">
  <!--style sheet -->
  @yield('style')
</head>
<body>
  <div class="login-box">
    <div class="login-logo"> 
      <img src="{{ asset('image/logo1.png')}}" alt="logo" width="65" height="65">
    </div>
    <div class="contrainer ">
      @yield('content')
    </div>
  </div>

<!-- jQuery -->
<script src="{{ asset('js/jquery.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('js/app.js')}}"></script>

@yield('script')

</body>
</html>
