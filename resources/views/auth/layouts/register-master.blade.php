<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>@yield('title')</title>
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('css/app.css')}}">
  <link rel="stylesheet" href="{{ asset('dist/css/adminlte.css')}}">
  <link rel="stylesheet" href="{{ asset('css/custom.css')}}">
  <!--style sheet -->
  @yield('style')

</head>
<body>
  @yield('content')


<!-- jQuery -->
<script src="{{ asset('js/jquery.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('js/app.js')}}"></script>

@yield('script')

</body>
</html> 
