@extends('auth.layouts.login-master')


@section('title')
    Forget Password
@endsection

@section('style')
<style>
    .login_card{
    height: 19rem ;
   }
    
</style>
@endsection

@section('content')
<div class="card p-2 mt-4 login_card" id="signin-card">
    <div class="card-body login-card-body" id="login_card_body">
      <div class="data-content mt-1">
        <div>
          <h6 class="font-weight-bold font-szie1 pl-1" id="signin-text">Forget Password</h6>
        </div>
        <p class="m-0 text-secondary small pl-1" style="margin-top: -9px !important;font-size:13.5px ">Verify email to reset password.</p>
      </div>
      <div class="container p-1 pt-4">  
        <form class="needs-validation" novalidate>
          <label class="m-0"><small class="text-dark mb-0 font-wegiht600">Email Address</small></label>
            <div class="mb-3 mt-1 input-group-sm input_field " >
              <input type="email" class="form-control input-sm border-radius8 " style="font-size: 12px"  placeholder="Email" id="verifyEmail" name="verifyEmail" required>
                <div class="invalid-feedback">Please fill out this field.</div>
                <div class="valid-feedback">Valid!</div>
            </div>
          <input type="submit" class="btn btn-block btn-danger btn-sm rounded-lg small font-weight-lighter red-button" value="Send Link">
        </form>
        <div class="text-center pt-2">
          <a  href="/login" ><small class=" text-xs pt-2 red-text font-szie1 font-wegiht600">Back to Login</small></a>
        </div>
      </div>
    </div>
  </div>
@endsection



@section('script')
<script>
  $(document).ready(function() {
      $("#verifyEmail").on('click', function(event) {
        event.preventDefault();
        $('#verifyEmail').addClass( "border-danger" );        
      });
      $("#verifyEmail" ).mouseleave(function() {
        $('#verifyEmail').removeClass( "border-danger" );
      });     
   
  });
</script>
<script>
  // Disable form submissions if there are invalid fields
  (function() {'use strict';
    window.addEventListener('load', function() {
      // Get the forms we want to add validation styles to
      var forms = document.getElementsByClassName('needs-validation');
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
          if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
          }else{
          }
          form.classList.add('was-validated');
        }, false);
      });
    }, false);
  })();
  </script>
@endsection
