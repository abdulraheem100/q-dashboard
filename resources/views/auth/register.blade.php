@extends('auth.layouts.register-master')

@section('title')
    Sign Up
@endsection

@section('style')
<style>
.padding-top30{
    padding-top: 20px
}

.size13{
    font-size: 13.5px;
    font-weight: 500 !important;
}
</style>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6" id="back">
            <div class="p-4">
                <img src="{{ asset('image/logo1.png')}}" alt="logo" width="65" height="65">
            </div>
        </div>
        <div class="col-lg-6 pl-5 pr-5 pb-5">
            <div class="text-center">
                <div class="padding-top30">
                    <div class="data-content mt-5">
                        <div>
                            <span class="font-weight-bold h6 signup-text font-weight-bold" style="font-size: 19px;">Sign Up</span>
                        </div>
                        <p class="m-0 text-secondary small pl-1" style="margin-top: -1.5px !important;font-size: 13.5px;">Q The Music Artist Dashboard.</p>
                    </div>
                </div>
            </div>
            <form id="registerForm" class="needs-validation" novalidate>
                <div class="d-flex justify-content-center">
                    <div class=" mb-1  mt-3 input-group-sm input_field" >
                        <div class="d-flex flex-row">
                            <div id="image-show" class ="roundedImage rounded-circle mt-2">
                            </div>
                            <div class="d-flex align-items-end image-div" style="">
                                <label>
                                    <span class="dot d-flex justify-content-center">
                                        <b class="plus">+</b>
                                    </span>
                                    <input type="file" id="fileUpload" style="display: none;" required>
                                </label>
                            </div>
                            <div class="invalid-feedback">Please fill out this field.</div>
                            <div class="valid-feedback">Valid!</div>
                        </div>
                    </div>
                </div>
                <div class="row">
                        <div class="col-md-6 offset-md-3 mt-3">
                            <div class="row">
                                <div class="col-12 col-md-6" >
                                    <label class="m-0"> <span class="text-dark font-weight-lighter size13">First name</span></label>
                                     <div class="mb-1 input-group-sm input_field round-7">
                                        <input type="text" class="form-control input-xs round-7 border-radius8"  placeholder="First name" id="firstName" name="firstName" required>
                                        <div class="invalid-feedback">Please fill out this field.</div>
                                     </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <label class="m-0"> <span class="text-dark font-weight-lighter size13" >Last name</span></label>
                                    <div class="mb-1 input-group-sm input_field rounded" >
                                        <input type="text" class="form-control input-xs border-radius8" placeholder="Last name" id="lastName" name="lastName" required>
                                        <div class="invalid-feedback">Please fill out this field.</div>   
                                    </div>
                                </div>
                            {{-- </div> --}}
                            {{-- <div class="row mt-1"> --}}
                                <div class="col-12">
                                    <label class="m-0"> <span class="text-dark font-weight-lighter size13" >Email address</span></label>
                                    <div class=" mb-1 input-group-sm input_field mt-1" >
                                        <input type="email" class="form-control input-xs border-radius8"  placeholder="Email" id="email" name="email" required>
                                        <div class="invalid-feedback">Please fill out this field.</div>
                                    </div>
                                </div>
                            {{-- </div> --}}
                            {{-- <div class="row"> --}}
                                <div class="col-12">
                                    <label class="m-0"> <span class="text-dark font-weight-lighter size13" ><b>About Artist</b></span></label>
                                    <div class=" mb-1 input-group-sm input_field rounded mt-1" >
                                        <textarea rows="3" cols="50" name="bio"  id="bio"  class="form-control input-xs rounded border-radius8"  placeholder="Enter Bio" form="registerForm" required></textarea>
                                        <div class="invalid-feedback">Please fill out this field.</div>
                                    </div>
                                </div>
                            {{-- </div> --}}
                            {{-- <div class="row mt-1"> --}}
                                <div class="col-12">
                                    <label class="m-0"> <span class="text-dark font-weight-lighter size13">Password</span></label>
                                    <div class="input-group mb-1 input-group-sm round-7 mt-1" id="show_hide_password">
                                        <input type="password" class="form-control input-xs password-radius"  placeholder="Password" id="password" name="password" required>
                                        <div class="input-group-append rounded">
                                            <div class="input-group-text eye-icon" id="password-eye"><a href="#">
                                                <span span class="fas fa-eye-slash eye-grey" ></span></a>
                                            </div>
                                        </div>
                                        <div class="invalid-feedback">Please fill out this field.</div>
                                    </div>
                                </div>
                            {{-- </div> --}}
                            {{-- <div class="row mt-1"> --}}
                                <div class="col-12">
                                    <label class="m-0"> <span class="text-dark font-weight-lighter size13" >Confirm Password</span></label>
                                    <div class="input-group mb-3 input-group-sm round-7 mt-1" id="show_hide_password_confirm">
                                        <input type="password" class="form-control input-xs password-radius"  placeholder="Enter Password" id="confirmPassword" name="confirmPassword" required>
                                        <div class="input-group-append rounded">
                                            <div class="input-group-text eye-icon" id="confirmPassword-eye" ><a href="#">
                                                <span class="fas fa-eye-slash eye-grey" ></span></a>
                                            </div>
                                        </div>
                                        <div class="invalid-feedback">Please fill out this field.</div>
                                    </div>
                                </div>
                            {{-- </div> --}}
                            {{-- <div class="row"> --}}
                                <div class="col-12">
                                    <input type="submit" class="btn btn-block btn-danger btn-sm rounded-lg red-button" value="Sign Up">
                                </div>
                            </div>
                        </form>
                            <div class="row">
                                <div class="col-12">
                                    <div class="mt-3 mb-1 rounded-lg">
                                        <a  href="/register"  class="btn btn-block btn-sm btn-danger rounded-lg light-red-button">Login</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
               
        </div>
    </div>
</div>
@endsection

@section('script')
 <script>
    $(function()
    {
        $('#fileUpload').on('change',function ()
        {
            var tmppath = URL.createObjectURL(event.target.files[0]);
            document.getElementById("image-show").style.backgroundImage = "url("+tmppath+")";
        });
    });
 </script>

<script>
    $(document).ready(function() {
      $("#show_hide_password a").on('click', function(event) {
          event.preventDefault();
          if($('#show_hide_password input').attr("type") == "text"){
              $('#show_hide_password input').attr('type', 'password');
              $('#show_hide_password span').addClass( "fa-eye-slash" );
              $('#show_hide_password span').removeClass( "fa-eye" );
          }else if($('#show_hide_password input').attr("type") == "password"){
              $('#show_hide_password input').attr('type', 'text');
              $('#show_hide_password span').removeClass( "fa-eye-slash" );
              $('#show_hide_password span').addClass( "fa-eye" );
          }
      });
  });

  $(document).ready(function() {
      $("#show_hide_password_confirm a").on('click', function(event) {
          event.preventDefault();
          if($('#show_hide_password_confirm input').attr("type") == "text"){
              $('#show_hide_password_confirm input').attr('type', 'password');
              $('#show_hide_password_confirm span').addClass( "fa-eye-slash" );
              $('#show_hide_password_confirm span').removeClass( "fa-eye" );
          }else if($('#show_hide_password_confirm input').attr("type") == "password"){
              $('#show_hide_password_confirm input').attr('type', 'text');
              $('#show_hide_password_confirm span').removeClass( "fa-eye-slash" );
              $('#show_hide_password_confirm span').addClass( "fa-eye" );
          }
      });
  });
  </script>
  <script>
  $(document).ready(function() {
        $("#firstName").on('click', function(event) {
            event.preventDefault();
            $('#firstName').addClass( "border-danger" );        
        });
        $( "#firstName" ).mouseleave(function() {
            $('#firstName').removeClass( "border-danger" );
        });
        $("#lastName").on('click', function(event) {
            event.preventDefault();
        $('#lastName').addClass( "border-danger" );        
        });
        $("#lastName" ).mouseleave(function() {
            $('#lastName').removeClass( "border-danger" );
        });
        $("#email").on('click', function(event) {
            event.preventDefault();
            $('#email').addClass( "border-danger" );        
        });
        $( "#email" ).mouseleave(function() {
            $('#email').removeClass( "border-danger" );
       });
        $("#password").on('click', function(event) {
            event.preventDefault();
            $('#password').addClass( "border-danger" );
            $('#password-eye').addClass( "border-danger" );        
        });
        $( "#password" ).mouseleave(function() {
            $('#password').removeClass( "border-danger" );
            $('#password-eye').removeClass( "border-danger" );
        }); 
        $("#confirmPassword").on('click', function(event) {
            event.preventDefault();
            $('#confirmPassword').addClass( "border-danger" );
            $('#confirmPassword-eye').addClass( "border-danger" );        
        });
        $( "#confirmPassword" ).mouseleave(function() {
            $('#confirmPassword').removeClass( "border-danger" );
            $('#confirmPassword-eye').removeClass( "border-danger" );
       }); 
       $("#bio").on('click', function(event) {
            event.preventDefault();
            $('#bio').addClass( "border-danger" );        
        });
        $( "#bio" ).mouseleave(function() {
            $('#bio').removeClass( "border-danger" );
       }); 
  });
  </script>
  <script>
    // Disable form submissions if there are invalid fields
    (function() {
      'use strict';
      window.addEventListener('load', function() {
        // Get the forms we want to add validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
          form.addEventListener('submit', function(event) {
            if (form.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
                document.getElementById("back").setAttribute("style", "height: 150vh;"); 
            }
            form.classList.add('was-validated');
          }, false);
        });
      }, false);
    })();
    </script>
@endsection
