@extends('layouts.master')

@section('title')
Withdrawal and Earnings
@endsection

@section('style')

@endsection

@section('content')
    <div class="row pt-0 pl-1">
        <p class="withdrawal-text ">Withdrawal Earnings</p>
    </div>
    <div class="row pt-1" >
        <div class="col-lg-3 col-md-12 in-one-line">
            <div class="balance-card card ml-2  border-radius8 red-background">
                <div class="card-body  border-radius8">
                    <div class="row pl-0">
                        <div class="col">
                            <P class="font-weight-lighter text-white pt-1 balance-text">YOUR BALANCE</P>
                        </div>
                    </div>
                    <div class="row pl-0 value-balance">
                        <div class="col">
                            <p class="text-white pt-1">$400</p>
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col pt-5">
                        </div>
                    </div>
                    <div class="row mt-5 pl-0">
                        <div class="col pt-3">
                            <P id="payoutClick" class="font-weight-lighter text-white font-weight-bold payout-text" >PAYOUT ACCOUNT</P>
                            <small class="font-weight-lighter text-white">No bank account added yet.</small>
                        </div>
                    </div>

                    {{-- if user account exists --}}
                    {{-- <div id="userAccount" class="card mt-2 mb-0 border-radius8 off-white" >
                        <div class="card-body p-0 border-radius8">
                            <div class="row">
                                <div class="col-8 pr-0">
                                    <div class="d-flex align-items-start">
                                        <p  class="pt-2 pl-2 userAccountName" >Muzamli Afridi</p>
                                    </div>
                                    <div class="d-flex align-items-end">
                                        <p class="pb-2 pl-2 userAccountNumber">123 456 789 123 4567</p>
                                    </div>
                                </div> 
                                <div class="col-4 " >
                                    <div class="d-flex align-items-center">
                                        <a class="red-text pt-3 pl-2 changeLink" href="#" data-toggle="modal" data-target="#editAccount"><b>Change</b></a> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    <div class="row pt-2">
                        <div class="col">
                            <a href="#" id="addAccountButton"  class="btn btn-block rounded-lg red-button font-weight-lighter text-white border-radius8 add-card" data-toggle="modal" data-target="#addAccount" >Add Card</a>
                            {{-- <a href="#" id="withdrawalButton"  class="btn btn-block rounded-lg red-button font-weight-lighter text-white border-radius8 add-card" data-toggle="modal" data-target="#withdrawal" >Request for Withdrawal</a> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <div class="col-lg-9 col-md-12" >
                <div class="card">
                    <div class="card-header pb-0" >
                        <div class="row">
                            <div class="earn-col  pt-2">
                                <div class="card earn-box text-center pb-3">
                                        <span class="info-box-text font-weight-lighter earn-box-title">Total Earnings</span>
                                        <span class="info-box-number font-weight-lighter earn-box-info" >$1.4K</span>
                                </div>
                             </div>
                            <div class="earn-col pt-2">
                                <div class="card earn-box text-center pb-3">
                                        <span class="info-box-text font-weight-lighter earn-box-title">Songs Earnings</span>
                                        <span class="info-box-number font-weight-lighter earn-box-info" >$800</span>
                                </div>
                            </div>
                            <div class="earn-col pt-2">
                                <div class="card earn-box text-center pb-3">
                                        <span class="info-box-text font-weight-lighter earn-box-title" >Album Earnings</span>
                                        <span class="info-box-number font-weight-lighter earn-box-info" >$600</span>
                                </div>
                            </div>
                            <div class="earn-col pt-2">
                                <div class="card earn-box text-center pb-3">
                                        <span class="info-box-text font-weight-lighter earn-box-title" >Available Balance</span>
                                        <span class="info-box-number font-weight-lighter earn-box-info" >$400</span>
                                </div>
                            </div>
                            <div class="earn-col pt-2">
                                <div class="card earn-box text-center pb-3">
                                        <span class="info-box-text font-weight-lighter earn-box-total-title" >Total Withdrawal Amount</span>
                                        <span class="info-box-number font-weight-lighter earn-box-info" >$800</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body pl-0 pt-2 pr-2">
                        <div class="row">
                            <div class`="col-md-12 payout-history-text" >
                                <small class="payout-text ml-2" >Payouts History</small>
                                <select class="float-right month-select" id="month" name="month" onchange="getMonthValue();"></select>
                                <select class="float-right year-select" id="year" name="year" onchange="getYearValue();"></select>
                            </div>
                            <div class`="col-md-12 pt-5  pl-0 pr-5">
                                <div class="timeline" style="">
                                    <div class="dot-left" style="margin-bottom: 9px">
                                        <span class="dot-earn"></span>
                                        <small class="payout-time pr-5">26/2/2021 .5:00AM</small><small class="payout-detail pl-lg-5"> Administration Cleared Your Funds</small><small class="payout-price text-danger timeline-right" > $400</small>
                                    </div>
                                    <div class="dot-left">
                                        <span class="dot-earn"></span>
                                        <small class="payout-time pr-5">26/2/2021 .5:00AM</small><small class="payout-detail pl-lg-5"> Withdrawal Request Initiated</small><small class="payout-price text-danger timeline-right" > $400</small>
                                    </div>
                                    <div class="dot-left">
                                        <span class="dot-earn"></span>
                                        <small class="payout-time pr-5">26/2/2021 .5:00AM</small><small class="payout-detail pl-lg-5"> Administration Cleared Your Funds</small><small class="payout-price text-danger timeline-right" > $400</small>
                                    </div>
                                    <div class="dot-left">
                                        <span class="dot-earn"></span>
                                        <small class="payout-time pr-5">26/2/2021 .5:00AM</small><small class="payout-detail pl-lg-5"> Administration Cleared Your Funds</small><small class="payout-price text-danger timeline-right" > $400</small>
                                    </div>
                                    <div class="dot-left">
                                        <span class="dot-earn"></span>
                                        <small class="payout-time pr-5">26/2/2021 .5:00AM</small><small class="payout-detail pl-lg-5"> Withdrawal Request Initiated</small><small class="payout-price text-danger timeline-right" > $400</small>
                                    </div>
                                    <div class="dot-left">
                                        <span class="dot-earn"></span>
                                        <small class="payout-time pr-5">26/2/2021 .5:00AM</small><small class="payout-detail pl-lg-5"> Administration Cleared Your Funds</small><small class="payout-price text-danger timeline-right" > $400</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
       
{{-- Add Account Modal --}}
<div class="modal fade" id="addAccount">
    <div class="modal-dialog modal-dialog-centered modal-sm ">
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-3 pb-2 pr-2" >
                <p class="modal-title add-bank-text" ><b>Add Bank Details </b></p>
                <button type="button" class="close"  data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="needs-validation" novalidate>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="mb-2 mt-1 input-group-sm input_field " >
                            <input type="text" class="form-control input-sm border-radius8 add-bank-feild"  placeholder="Account Name" style="font-size: 12.5px" id="accountName" name="accountName" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                            <div class="valid-feedback">Valid!</div>
                        </div>
                        <div class="mb-1 mt-1 input-group-sm input_field " >
                            <input type="text" class="form-control input-sm border-radius8 add-bank-feild"  placeholder="Account Number" style="font-size: 12.5px" id="accountNumber" name="accountNumber" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                            <div class="valid-feedback">Valid!</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer p-2 justify-content-between " style="margin-left: 4px">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">Canel</button>
                <input type="submit" class="btn red-button btn-xs mr-2 pt-1 pb-1 pl-3 pr-3 border-radius8 account-Create-button" value="Create">
            </div>
            </form>
        </div>
    </div>
</div>

{{-- Edit Account Modal --}}
<div class="modal fade" id="editAccount">
    <div class="modal-dialog modal-dialog-centered modal-sm ">
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pb-2  pt-3" >
                <small class="modal-title edit-bank-text"><b>Add Bank Details</b></small>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="needs-validation" novalidate>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="mb-2 mt-1 input-group-sm input_field " >
                            <input type="text" class="form-control input-sm border-radius8"  placeholder="Account Name" id="editAccountName" name="editAccountName" value="Muzamli Afridi" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                            <div class="valid-feedback">Valid!</div>
                        </div>
                        <div class="mb-1 mt-1 input-group-sm input_field " >
                            <input type="text" class="form-control input-sm border-radius8"  placeholder="Account Number" id="editAccountNumber" name="editAccountNumber" value="1234567891234567" required>
                            <div class="invalid-feedback">Please fill out this field.</div>
                            <div class="valid-feedback">Valid!</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between ">
                  <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">Canel</button>
                <input type="submit" class="btn red-button btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 account-Create-button" value="Create">
            </div>
            </form>
        </div>
    </div>
</div>

{{-- Withdrawal Modal --}}
<div class="modal fade" id="withdrawal">
    <div class="modal-dialog modal-dialog-centered modal-sm ">
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-3 pb-2" >
                <small class="modal-title"><b>Confirmation</b></small>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col">
                        <p class="rwithdrawal-text">You are going to initiate withdrawal request of $400</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between ">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">No</button>
                <form>
                    <input type="submit" class="btn red-button btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8" value="Confirm">
                </form>
            </div>
        </div>
    </div>
</div>
@endsection


@section('script')
<script>
  $(document).ready(function() {
      $("#month").on('click', function(event) {
        event.preventDefault();
        $('#month').addClass( "border-danger" );        
      });
      $("#month" ).mouseleave(function() {
        $('#month').removeClass( "border-danger" );
      });
      $("#accountName").on('click', function(event) {
        event.preventDefault();
        $('#accountName').addClass( "border-danger" );        
      });
      $("#accountName" ).mouseleave(function() {
        $('#accountName').removeClass( "border-danger" );
      });
      $("#accountNumber").on('click', function(event) {
        event.preventDefault();
        $('#accountNumber').addClass( "border-danger" );        
      });
      $("#accountNumber" ).mouseleave(function() {
        $('#accountNumber').removeClass( "border-danger" );
      });
      $("#editAccountName").on('click', function(event) {
        event.preventDefault();
        $('#editAccountName').addClass( "border-danger" );        
      });
      $("#editAccountName" ).mouseleave(function() {
        $('#editAccountName').removeClass( "border-danger" );
      });
      $("#editAccountNumber").on('click', function(event) {
        event.preventDefault();
        $('#editAccountNumber').addClass( "border-danger" );        
      });
      $("#editAccountNumber" ).mouseleave(function() {
        $('#editAccountNumber').removeClass( "border-danger" );
      });      
      $("#year").on('click', function(event) {
        event.preventDefault();
        $('#year').addClass( "border-danger" );
      });
      $("#year" ).mouseleave(function() {
        $('#year').removeClass( "border-danger" );
      });   
  });
</script>
 <script>
(function () {
    let year_satart = 2000;
    let year_end = (new Date).getFullYear(); // current year
    let year_selected = 'Year';

    let option = '';
    option = '<option value="'+00+'">Year</option>'; // first option

    for (let i = year_satart; i <= year_end; i++) {
        let selected = (i === year_selected ? ' selected' : '');
        option += '<option value="' + i + '"' + selected + '>' + i + '</option>';
    }

    document.getElementById("year").innerHTML = option;
})();
</script>
<script>
(function () {
    let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var month_selected = 'Month';     //(new Date).getMonth(); // current month
    var option = '';
    option = '<option value="'+00+'">Month</option>'; // first option

    for (let i = 0; i < months.length; i++) {
        let month_number = (i + 1);

        // value month number with 0. [01 02 03 04..]
        let month = (month_number <= 9) ? '0' + month_number : month_number;


        let selected = (i === month_selected ? ' selected' : '');
        option += '<option value="' + month + '"' + selected + '>' + months[i] + '</option>';
    }
    document.getElementById("month").innerHTML = option;
})();
</script>
<script>
  // Disable form submissions if there are invalid fields
  (function() {'use strict';
    window.addEventListener('load', function() {
      // Get the forms we want to add validation styles to
      var forms = document.getElementsByClassName('needs-validation');
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
          if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
          }
          form.classList.add('was-validated');
        }, false);
      });
    }, false);
  })();
</script>
<script>
   
    function getMonthValue() {
   var conceptName = $('#month').find(":selected").text();
   console.log(conceptName); 
}
function getYearValue() {
   var conceptName = $('#year').find(":selected").text();
   console.log(conceptName); 
}
</script>
<script>
</script>
@endsection


