<div class="row m-2">
    <div class="col-lg-6">
        <div class="card" style="border-radius: 6px;">
            <div class="card-body p-0" style="border-radius: 6px">
                <div class="row">
                <div class="col-9">
                <ul class="products-list product-list-in-card pl-2 pr-1">
                    <li class="item m-0 p-0">
                        <div class="product-img pt-2 pb-2">
                            <img src="{{asset('image/blog/blog3.jpg') }}"  alt="Product Image" data-toggle="modal" data-target="#playlistDetail"  style="border-radius: 6px;width: 47px;height:47px">
                        </div>
                        <div class="product-info pt-2">
                            <small  class="product-title font-szie10">Sleep Time </small>
                            <small class="product-description light-grey-text" style="font-size: 8px">05 Songs</small>
                        </div>
                    </li>
                </ul>
                </div>
                <div class="col-3">
                <div class="float-right mt-3 mr-2">
                    <button type="button"  class="btn grey-button  btn-xs " id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border-radius: 5px" ><i class="bi bi-three-dots"></i></button>
                        <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a href="#" class="text-dark" data-toggle="modal" data-target="#editPlaylist">
                            <div class="dropdown-item m-0" >
                                <div class="container">
                                    <small>Edit</small>
                                </div>
                           </div>
                           </a>
                        <div class="dropdown-divider m-0"></div>
                        <a href="#" class="text-dark" data-toggle="modal" data-target="#deletePlaylist"> 
                        <div class="dropdown-item" >
                            <div class="container">
                                <small>Delete</small>
                            </div>
                        </div>
                       </a>
                 </div>
                </div>
            </div>
        </div>
            </div>
        </div>
    </div>
     <div class="col-lg-6">
        <div class="card" style="border-radius: 6px;">
            <div class="card-body p-0" style="border-radius: 6px">
                <div class="row">
                <div class="col-9">
                <ul class="products-list product-list-in-card pl-2 pr-1">
                    <li class="item m-0 p-0">
                        <div class="product-img pt-2 pb-2">
                            <img src="{{asset('image/blog/blog3.jpg') }}"  alt="Product Image" data-toggle="modal" data-target="#playlistDetail"  style="border-radius: 6px;width: 47px;height:47px">
                        </div>
                        <div class="product-info pt-2">
                            <small  class="product-title font-szie10">Sleep Time </small>
                            <small class="product-description light-grey-text" style="font-size: 8px">05 Songs</small>
                        </div>
                    </li>
                </ul>
                </div>
                <div class="col-3">
                <div class="float-right mt-3 mr-2">
                    <button type="button"  class="btn grey-button  btn-xs " id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border-radius: 5px" ><i class="bi bi-three-dots"></i></button>
                        <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a href="#" class="text-dark" data-toggle="modal" data-target="#editPlaylist">
                            <div class="dropdown-item m-0" >
                                <div class="container">
                                    <small>Edit</small>
                                </div>
                           </div>
                           </a>
                        <div class="dropdown-divider m-0"></div>
                        <a href="#" class="text-dark" data-toggle="modal" data-target="#deletePlaylist"> 
                        <div class="dropdown-item" >
                           <div class="container">
                             <small>Delete</small>
                        </div>
                       </div>
                       </a>
                 </div>
                </div>
            </div>
        </div>
            </div>
        </div>
    </div>
     <div class="col-lg-6">
        <div class="card" style="border-radius: 6px;">
            <div class="card-body p-0" style="border-radius: 6px">
                <div class="row">
                <div class="col-9">
                <ul class="products-list product-list-in-card pl-2 pr-1">
                    <li class="item m-0 p-0">
                        <div class="product-img pt-2 pb-2">
                            <img src="{{asset('image/blog/blog3.jpg') }}"  alt="Product Image" data-toggle="modal" data-target="#playlistDetail"  style="border-radius: 6px;width: 47px;height:47px">
                        </div>
                        <div class="product-info pt-2">
                            <small  class="product-title font-szie10">Sleep Time </small>
                            <small class="product-description light-grey-text" style="font-size: 8px">05 Songs</small>
                        </div>
                    </li>
                </ul>
                </div>
                <div class="col-3">
                <div class="float-right mt-3 mr-2">
                    <button type="button"  class="btn grey-button  btn-xs " id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border-radius: 5px" ><i class="bi bi-three-dots"></i></button>
                        <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a href="#" class="text-dark" data-toggle="modal" data-target="#editPlaylist">
                            <div class="dropdown-item m-0" >
                                <div class="container">
                                    <small>Edit</small>
                                </div>
                           </div>
                           </a>
                        <div class="dropdown-divider m-0"></div>
                        <a href="#" class="text-dark" data-toggle="modal" data-target="#deletePlaylist"> 
                        <div class="dropdown-item" >
                           <div class="container">
                             <small>Delete</small>
                        </div>
                       </div>
                       </a>
                 </div>
                </div>
            </div>
        </div>
            </div>
        </div>
    </div>
</div>


{{-- Delete Playlist Modal --}}
<div class="modal fade" id="deletePlaylist">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle">
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-3 pb-2" >
                <p class="modal-title notification-detail-text"><b>Confirmation</b></p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col notification-detail-textt">
                        <small>Are You Sure You Want To Delete this Playlist?</small>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between ">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">No</button>
                <form>
                    <input type="submit" class="btn red-button btn-xs pt-1 pb-1 pl-4 pr-4 border-radius8" value="Yes">
                </form>
            </div>
        </div>
    </div>
</div>


{{-- Edit Playlist Modal --}}
<div class="modal fade" id="editPlaylist">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle">
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-3 pb-2" >
                <p class="modal-title notification-detail-text"><b>Edit Playlist</b></p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                <div class="row">
                    <div class="mb-1 mt-1 input-group-sm input_field " >
                        <label class="m-0"><small class="text-dark add-song-label-text">Title</small></label>
                        <input type="text" class="form-control input-sm border-radius8 text-dark"  placeholder="Enter title" value="Sleep Time" id="userName" name="userName" required>
                        <div class="invalid-feedback">Please fill out this field.</div>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between ">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">Cancel</button>
                    <input type="submit" class="btn red-button btn-xs pt-1 pb-1 pl-4 pr-4 border-radius8" value="Update">
                </form>
            </div>
        </div>
    </div>
</div>

{{-- Playlist detail Modal --}}
<div class="modal fade" id="playlistDetail">
    <div class="modal-dialog modal-dialog-centered modal-lg card-detail">
        <div class="modal-content border-radius12">
            <div class="modal-header border-radius-top pl-3 pt-4 pb-3 pr-2" style="background-color: #BA011A;color:white">
                <div class="col-1">
                    <div>
                        <img src="{{asset('image/blog/blog3.jpg') }}" style="" class="border-radius5 song-image" alt="thumbnail" width="45px" height="45px">
                    </div>
                </div>
                <div class="col-8 pl-3">
                    <div class="p-1 pl-0" id="song-title-text">
                        <small class="d-flex align-items-start" style="font-weight: 500">For Sleep Time
                        </small>
                        <small class="d-flex align-items-end " style="font-size:9px;">05 Songs</small>
                    </div>
                </div>
                <div class="col-3  ">
                    <div class="float-right">
                        <button type="button" class="close pt-0" style="color: #fff"  data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="modal-body p-0 ">
                <div class="row">
                    <div class="col-12 table-wrapper-scroll-y my-custom-scrollbar">
                        <table class="table responsive ">
                            <tbody>
                                <tr>
                                    <td  class="p-0 pl-2">
                                        <ul class="products-list product-list-in-card pl-2 pr-1">
                                            <li class="item">
                                                <div class="product-img pt-1">
                                                    <img src="{{asset('image/blog/blog3.jpg') }}" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
                                                </div>
                                                <div class="product-info ml-5">
                                                    <small  class="product-title font-szie10">Life on Mars
                                                    </small>
                                                    <small class="product-description font-szie10 light-grey-text">(3:56)</small>
                                                </div>
                                            </li>
                                        </ul>
                                    </td>
                                    <td class="pl-0 pr-0"> 
                                        <span class="ml-5 p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Audio</span>
                                        <span class="p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Video</span>
                                    </td>
                                    <td></td>
                                    <td class="pb-2" > 
                                        <button class="btn text-left float-right btn-xs orange-button border-radius8 s-button" data-toggle="modal" data-target="#deleteSongFromPlaylist"><span class="bi bi-trash3"  id="trash-icon"></span></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td  class="p-0 pl-2">
                                        <ul class="products-list product-list-in-card pl-2 pr-1">
                                            <li class="item">
                                                <div class="product-img pt-1">
                                                    <img src="{{asset('image/blog/blog3.jpg') }}" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
                                                </div>
                                                <div class="product-info ml-5">
                                                    <small  class="product-title font-szie10">Life on Mars
                                                    </small>
                                                    <small class="product-description font-szie10 light-grey-text">(3:56)</small>
                                                </div>
                                            </li>
                                        </ul>
                                    </td>
                                    <td class="pl-0 pr-0"> 
                                        <span class="ml-5 p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Audio</span>
                                        <span class="p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Video</span>
                                    </td>
                                    <td></td>
                                    <td class="pb-2" > 
                                        <button class="btn text-left float-right btn-xs orange-button border-radius8 s-button" data-toggle="modal" data-target="#deleteSongFromPlaylist"><span class="bi bi-trash3"  id="trash-icon"></span></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td  class="p-0 pl-2">
                                        <ul class="products-list product-list-in-card pl-2 pr-1">
                                            <li class="item">
                                                <div class="product-img pt-1">
                                                    <img src="{{asset('image/blog/blog3.jpg') }}" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
                                                </div>
                                                <div class="product-info ml-5">
                                                    <small  class="product-title font-szie10">Life on Mars
                                                    </small>
                                                    <small class="product-description font-szie10 light-grey-text">(3:56)</small>
                                                </div>
                                            </li>
                                        </ul>
                                    </td>
                                    <td class="pl-0 pr-0"> 
                                        <span class="ml-5 p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Audio</span>
                                        <span class="p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Video</span>
                                    </td>
                                    <td></td>
                                    <td class="pb-2" > 
                                        <button class="btn text-left float-right btn-xs orange-button border-radius8 s-button" data-toggle="modal" data-target="#deleteSongFromPlaylist"><span class="bi bi-trash3"  id="trash-icon"></span></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td  class="p-0 pl-2">
                                        <ul class="products-list product-list-in-card pl-2 pr-1">
                                            <li class="item">
                                                <div class="product-img pt-1">
                                                    <img src="{{asset('image/blog/blog3.jpg') }}" class="border-radius8" alt="Product Image"  style="width: 35.5px;height:35.5px">
                                                </div>
                                                <div class="product-info ml-5">
                                                    <small  class="product-title font-szie10">Life on Mars
                                                    </small>
                                                    <small class="product-description font-szie10 light-grey-text">(3:56)</small>
                                                </div>
                                            </li>
                                        </ul>
                                    </td>
                                    <td class="pl-0 pr-0"> 
                                        <span class="ml-5 p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Audio</span>
                                        <span class="p-1 pl-2 pr-2 text-Audio border-radius8 font-size12">Video</span>
                                    </td>
                                    <td></td>
                                    <td class="pb-2" > 
                                        <button class="btn text-left float-right btn-xs orange-button border-radius8 s-button" data-toggle="modal" data-target="#deleteSongFromPlaylist"><span class="bi bi-trash3"  id="trash-icon"></span></button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Delete Song Playlist Modal --}}
<div class="modal fade" id="deleteSongFromPlaylist">
    <div class="modal-dialog modal-dialog-centered modal-md card-middle">
        <div class="modal-content border-radius12">
            <div class="modal-header pl-3 pt-3 pb-2" >
                <p class="modal-title notification-detail-text"><b>Confirmation</b></p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col notification-detail-textt">
                        <small>Are You Sure You Want To Delete Song from Playlist?</small>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between ">
                <button type="button" class="btn btn-xs pt-1 pb-1 pl-3 pr-3 border-radius8 light-grey" data-dismiss="modal">No</button>
                <form>
                    <input type="submit" class="btn red-button btn-xs pt-1 pb-1 pl-4 pr-4 border-radius8" value="Yes">
                </form>
            </div>
        </div>
    </div>
</div>
