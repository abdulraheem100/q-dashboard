<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::redirect('/','/login');
Route::view('/login','auth.login');
Route::view('/forget-password','auth.forget-password');
Route::view('/new-password','auth.new-password');

Route::view('/register','auth.register');
Route::view('/home','home');
Route::view('/earning','earning');
Route::view('/notification','notification');
Route::view('/artist','artist');
Route::view('/dashboard','dashboard');
Route::view('/terms','terms');
Route::view('/policy','policy');
Route::view('/test','test');

Route::view('/admin/login','auth.admin-login')->name('admin.login');
Route::view('/admin/forgot-password','auth.admin-forgot-password')->name('admin.forgotPassword');
Route::view('/admin/new-password','auth.admin-new-password')->name('admin.newPassword');
Route::view('/admin/dashboard','admin-dashboard')->name('admin.dashboard');
Route::view('/admin/app-users','app-users')->name('admin.appUsers');
Route::view('/admin/app-users/detail','app-detail')->name('admin.detail');
Route::view('/admin/music-category','music-category')->name('admin.musicCategory');

